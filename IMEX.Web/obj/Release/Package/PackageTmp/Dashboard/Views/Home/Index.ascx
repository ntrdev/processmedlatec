﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%= ShowMessage()%>



<div class="page-content">
    <div class="breadcrumbs">
        <ol class="breadcrumb">
            <li>
                <a href="/{CPPath}/">Home</a>
            </li>
            <li class="active">Quản trị hệ thống</li>
        </ol>
    </div>

    <div class="row">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-bubbles font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Trang chủ quản trị</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#portlet_product_1" data-toggle="tab" aria-expanded="true">Trang chủ</a>
                            </li>

                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="portlet_product_1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.document.title = 'Trang chủ quản trị Medo | Power by IMEXsoft®';
</script>
