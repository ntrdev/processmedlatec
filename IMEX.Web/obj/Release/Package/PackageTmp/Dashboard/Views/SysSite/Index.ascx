﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysSiteModel;
    var listItem = ViewBag.Data as List<SysSiteEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý ngôn ngữ</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Site</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetSortListCommand()%>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Site</span>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center ">STT</th>
                                        <th class="sorting_disabled text-center">
                                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting text-center"><%= GetSortLink("Tiêu đề", "Name")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Mã", "Code")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngôn ngữ", "LangID")%></th>
                                        <th class="text-center">Trạng thái</th>
                                        <th style="width: 110px">Sắp xếp vị trí</th>
                                        <th class="text-center">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td class="text-center">
                                            <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                        </td>
                                        <td class="text-center"><%=listItem[i].Code %></td>
                                        <td class="text-center"><%= GetName(SysLangService.Instance.GetByID(listItem[i].LangID)) %></td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%= GetDefault(listItem[i].ID, listItem[i].Default)%>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%= GetOrder(listItem[i].ID, listItem[i].Order)%>
                                        </td>
                                        <td class="text-center">
                                            <div class="control-button">
                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        var REDDEVILController = "SysSite";

        var REDDEVILArrVar = [
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

</form>
