﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using IMEX.Core.Global;
using IMEX.Core.Web;
using IMEX.Lib.Global;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;
using Newtonsoft.Json.Linq;
using System.Net.Mime;
using System.Net.Mail;
using System.Web;

namespace IMEX.Lib.Controllers
{
    [ModuleInfo(Name = "API: API", Code = "MApi", Order = 99, Crawl = false, HideOrShow = true)]

    public class MApiController : Controller
    {

        public void ActionIndex() { }
        public void ActionSecurity(SecurityModel model)
        {
            var json = new Json();

            Captcha.CaseSensitive = false;
            var ci = new Captcha(175, 35);

            ViewPage.Response.Clear();
            ViewPage.Response.ContentType = "image/jpeg";

            ci.Image.Save(ViewPage.Response.OutputStream, ImageFormat.Jpeg);
            ci.Dispose();

            ViewBag.Model = model;

            ViewPage.Response.End();
        }


        public void ActionGetProcess(Subcribe model)
        {
            var json = new Json();

            var _process = ModProcessService.Instance.CreateQuery().Select(o => o.ProcessID)
                                                    .Where(o => o.MaLichHen == model.email)
                                                    .ToSingle();
            if (_process != null)
                json.Instance.Html = HttpUtility.UrlEncode(Core.Global.Security.Encrypt("MrReddevil_" + _process.ProcessID.ToString()));
            else
                json.Instance.Message = "Lịch hẹn không tồn tại.";

            json.Create();

        }
        private static string[] image = { "ic_calendar.png", "ic_bike.png", "ic_athome.png", "ic_sample.png", "ic_result.png", "ic_talkbox.png", "ic_hospital.png" };
        private static string[] imageDisable = { "ic_calendar_grey.png", "ic_bike_grey.png", "ic_athome_grey.png", "ic_sample_grey.png", "ic_result_grey.png", "ic_talkbox_grey.png", "ic_hospital_grey.png" };
        public void ActionStepProcess(GetSearchModel model)
        {
            var json = new Json();

            if (string.IsNullOrEmpty(model.token)) json.Instance.Message = "không có quyền truy cập";
            else
            {

                int _id = Core.Global.Convert.ToInt(Core.Global.Security.Decrypt(HttpUtility.UrlDecode(model.token)).Replace("MrReddevil_", string.Empty));

                var _process = ModProcessService.Instance.CreateQuery()
                                                        .Where(o => o.ProcessID == _id)
                                                        .ToSingle();
                if (_process != null)
                {
                    for (int i = 0; i < 7; i++)
                    {

                        if (i == 0)
                        {
                            string text = "Đã xác nhận đặt lịch."; string text2 = "";
                            if (_process.TGXacNhan > DateTime.MinValue) text = "Đã xác nhận đặt lịch.";
                            else if (!string.IsNullOrEmpty(_process.SID)) text = "Đã xác nhận đặt lịch.";
                            else text = "Lịch hẹn của quý khách đang được chờ xác nhận.";
                            if (_process.TGXacNhan > DateTime.MinValue)
                                text2 = "<div class=\"alert alert-process\">Nhân viên lấy mẫu dự kiến sẽ đến lúc " + _process.KhungGio + " " + Global.Utils.FormatDate(_process.TGDenDiaChi) + "</div>";

                            json.Instance.Html += @"<li class=""item"">
                                                    <div class=""process_img "">
                                                        <i class=""icon-reversed"">
                                                           <img src=""/interface/img/" + (_process.TGXacNhan > DateTime.MinValue || !string.IsNullOrEmpty(_process.SID) ? image[i] : imageDisable[i]) + @""" alt=""""></i>
                                                    </div>
                                                    <div class=""process_content"">
                                                        <p><b>" + text + @"</b></p>
                                                           " + text2 + @"
                                                    </div>
                                                </li>";
                        }

                        if (i == 1)
                        {

                            string text = "NV lấy mẫu đang trên đường tới điểm hẹn.";
                            string text2 = "";
                            if (_process.TGXacNhan <= DateTime.MinValue)
                                text2 = "";
                            else
                            {

                                var nhanvien = ModNhanVienService.Instance.CreateQuery()
                                                                          .Select(o => new { o.maTN, o.soDT })
                                                                          .Where(o => o.maNV == _process.UserNhanLich).ToSingle_Cache() ?? new ModNhanVienEntity();

                                var sysUser = ModSysNhanVienService.Instance.CreateQuery()
                                                                            .Select(o => o.UserCode)
                                                                            .Where(o => o.UserID == nhanvien.maTN).ToSingle_Cache() ?? new ModSysNhanVienEntity();
                                
                                var location = ModNhanVienLocationService.Instance.CreateQuery()
                                                                      .Where(o => o.MaTN == sysUser.UserCode)
                                                                      .OrderByDesc(o => o.ID)
                                                                      .ToSingle_Cache();

                                text = "Nhân viên lấy mẫu đang trên đường tới điểm hẹn.";
                                text2 = "<div class=\"alert alert-process\">Nhân viên lấy mẫu đang trên đường tới vị trí của bạn. Thời gian dự kiến "
                                  + _process.KhungGio + " " + Global.Utils.FormatDate(_process.TGDenDiaChi) + (location != null ? "<p><a href=\"https://maps.google.com/?q=" + location.Lat + "," + location.Lon + "\" taget=\"_blank\"><i class=\"fa fa-map-marker\"></i>Xem vị trí</a></p>" : "") + "</div>";
                            }
                            json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + (_process.TGXacNhan > DateTime.MinValue ? image[i] : imageDisable[i]) + @"""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                            <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";

                        }
                        if (i == 2)
                        {
                            if (!string.IsNullOrEmpty(_process.SID))
                            {
                                string text = "Hoàn thành khám/lấy mẫu.";
                                string text2 = "<div class=\"alert alert-process\">Nhân viên đã hoàn thành lấy mẫu bệnh phẩm lúc "
                                    + Global.Utils.FormatDateTime(_process.TGVaoSo) + " <p>"
                                    + (_process.DiemDanhGia < 1 ? "<a data-toggle=\"modal\" data-target=\"#register-form\" title=\"Đánh giá nhân viên\"  class=\"rating-process\"><i class=\"fa fa-thumbs-o-up\"></i>Đánh giá</a>" : "<a class=\"rating-process\"><i class=\"fa fa-star" + (_process.DiemDanhGia >= 1 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 2 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 3 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 4 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 5 ? "" : "-o") + @"""></i></a>") + "</p></div>";
                                json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + image[i] + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                            <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";
                            }
                            else
                            {
                                json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + imageDisable[i] + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                            <p><b>Hoàn thành khám/lấy mẫu?</b></p>
                                                        </div>
                                                    </li>";
                            }

                        }
                        if (i == 3)
                        {
                            string text = "Trạng thái mẫu bệnh phẩm.";
                            string text2 = "";
                            if (_process.TGNhanMau > DateTime.MinValue)
                                text2 = "<div class=\"alert alert-process\">Mẫu bệnh phẩm đã được bàn giao cho phòng xét nghiệm và đang phân tích. Dự kiến kết quả sẽ có lúc " + Global.Utils.FormatDateTime(_process.TGNhanMau.AddMinutes(90)) + "</div>";

                            json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + (_process.TGNhanMau > DateTime.MinValue ? image[i] : imageDisable[i]) + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                             <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";
                        }
                        if (i == 4)
                        {
                            string text = "Kết quả xét nghiệm.";
                            string text2 = "";
                            if (_process.FullResult)
                            {

                                if (!string.IsNullOrEmpty(_process.SID))
                                {
                                    string _sid = AES.EncryptString(_process.SID.Trim(), "").Trim();
                                    string tokenkey = SaltedHash.GetSHA512("m$dl4tec" + _process.SID.Trim());
                                    string _url = "https://medlatec.vn/tra-cuu-ket-qua/sid?sid=" + _sid + "&place=HN&La=VN&token=" + tokenkey;


                                    text2 = "<div class=\"alert alert-process\">Kết quản xét nghiệm của quý khách đã có lúc " + Global.Utils.FormatDateTime(_process.ValidTime) +
                                        ". Để xem kết quả quý khách vui lòng click vào mã tra cứu màu xanh dưới đây (hoặc tải ứng dụng MedOn - <a href=\"" + Config.GetValue("Mod.LinkApp").ToString() + "\" target=\"_blank\">link tải app</a>) Mã tra cứu: <a href=\"" + _url + "\" target=\"_blank\">" + _process.SID + @"</a></div>";
                                }
                            }
                            json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + (_process.FullResult ? image[i] : imageDisable[i]) + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                             <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";
                        }
                        if (i == 5)
                        {

                            string text = "Tư vấn kết quả.";
                            string text2 = "";
                            if (_process.FullResult)
                            {

                                var _rate = ModRateServiceService.Instance.CreateQuery().Where(o => o.ProcessID == _process.ProcessID && o.ScheduleCode == _process.MaLichHen).ToSingle_Cache();
                                _process.DiemDanhGia = _rate != null ? (_rate.DoctorLevel + _rate.DoctorPoint + _rate.TimeAdvisoryPoint + _rate.PointService) / 4 : 0;
                                text2 = "<div class=\"alert alert-process\">" +
                                    "Quý khách vui lòng gặp bác sĩ chỉ định để khám hoặc liên hệ tổng đài <a href=\"tel:1900565656\">1900 565656</a> " +
                                    "hoặc tải ứng dụng MedOn (<a href=\"" + Config.GetValue("Mod.LinkApp").ToString() + "\" target=\"_blank\">link tải app</a>) " +
                                    "để đặt lịch hẹn với bác sĩ và được tư vấn kết quả xét nghiệm. "
                                    + (_process.TuVan && _rate == null ? "<br/><a data-toggle=\"modal\" data-target=\"#rate-form\" class=\"rating-process tuvan-star\"><i class=\"fa fa-thumbs-o-up\"></i>Đánh giá</a>" : _rate != null ? "<a class=\"rating-process\"><i class=\"fa fa-star" + (_process.DiemDanhGia >= 1 ? "" : "-o") + "\"></i><i class=\"fa fa-star" + (_process.DiemDanhGia >= 2 ? "" : "-o") + "\"></i><i class=\"fa fa-star" + (_process.DiemDanhGia >= 3 ? "" : "-o") + "\"></i><i class=\"fa fa-star" + (_process.DiemDanhGia >= 4 ? "" : "-o") + "\"></i><i class=\"fa fa-star" + (_process.DiemDanhGia >= 5 ? "" : "-o") + "\"></i></a><br/><a style=\"float:right\" data-toggle=\"modal\" data-target=\"#rate-form\" class=\"tuvan-star\">Xem lại đánh giá</a>" : "") + "</div>";
                            }


                            json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + (_process.FullResult ? image[i] : imageDisable[i]) + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                             <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";
                        }
                        if (i == 6)
                        {
                            string text = "Lịch hẹn tái khám.";
                            string text2 = "";
                            if (_process.TGHenTaiKham > DateTime.MinValue)
                                text2 = "<div class=\"alert alert-process\">Thời gian khuyến nghị sử dụng dịch vụ lần tiếp theo của Quý khách là vào <span style=\"color:#1d93e3\">" + Global.Utils.FormatDate(_process.TGHenTaiKham) + "</span> MEDLATEC xin cảm ơn!</div>";

                            json.Instance.Html += @"<li class=""item"">
                                                        <div class=""process_img "">
                                                            <i class=""icon-reversed"">
                                                               <img src=""/interface/img/" + (_process.TGHenTaiKham > DateTime.MinValue ? image[i] : imageDisable[i]) + @""" alt =""""></i>
                                                        </div>
                                                        <div class=""process_content"">
                                                             <p><b>" + text + @"</b></p>
                                                               " + text2 + @"
                                                        </div>
                                                    </li>";
                        }
                        #region old code

                        //if (model.step == 0)
                        //{
                        //    json.Instance.Html = @"<div class=""content_body"">";
                        //    if (_process.TGXacNhan > DateTime.MinValue) json.Instance.Html += "✔️ Đã xác nhận đặt lịch. <br/>";
                        //    else if (!string.IsNullOrEmpty(_process.SID)) json.Instance.Html += "✔️ Đã xác nhận đặt lịch.";
                        //    else json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //    if (_process.TGXacNhan > DateTime.MinValue && _process.DaNhanLich && _process.TGDenDiaChi > DateTime.MinValue && !string.IsNullOrEmpty(_process.KhungGio) && string.IsNullOrEmpty(_process.SID))
                        //    {
                        //        json.Instance.Html += "🏍 Nhân viên lấy mẫu dự kiến sẽ đến lúc " + _process.KhungGio + " " + Global.Utils.FormatDate(_process.TGDenDiaChi);
                        //    }


                        //    json.Instance.Html += "</div>";
                        //}
                        //if (model.step == 1)
                        //{
                        //    if (!string.IsNullOrEmpty(_process.SID))
                        //    {
                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        json.Instance.Html += "✔️ NV lấy mẫu đã hoàn thành lấy mẫu bệnh phẩm lúc:<br/>";
                        //        json.Instance.Html += Global.Utils.FormatDateTime(_process.TGVaoSo);
                        //        if (_process.DiemDanhGia < 1)
                        //        {
                        //            json.Instance.Html += @"<a data-toggle=""modal"" data-target=""#register-form"" class=""rating-process""><i class=""fa fa-thumbs-o-up""></i> Đánh giá</a>";
                        //        }
                        //        else
                        //        {
                        //            json.Instance.Html += @"<a class=""rating-process""><i class=""fa fa-star" + (_process.DiemDanhGia >= 1 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 2 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 3 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 4 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 5 ? "" : "-o") + @"""></i></a>";
                        //        }
                        //        json.Instance.Html += "</div>";
                        //    }
                        //    else
                        //    {
                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        if (_process.TGXacNhan <= DateTime.MinValue)
                        //            json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //        else
                        //            json.Instance.Html += "👩‍💻 Đang chờ lấy mẫu bệnh phẩm.";

                        //        json.Instance.Html += "</div>";
                        //    }
                        //}
                        //if (model.step == 2)
                        //{
                        //    if (_process.FullResult)
                        //    {
                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        json.Instance.Html += "✔️ Mã tra cứu: " + _process.MaLichHen + @" đã đủ kết quả";
                        //        json.Instance.Html += "</div>";
                        //    }
                        //    else
                        //    {
                        //        if (_process.TGNhanMau > DateTime.MinValue)
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            json.Instance.Html += "👨‍🔬 Mã tra cứu: " + _process.MaLichHen + @" đã nhận đủ mẫu. Và đang chạy xét nghiệm.";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //        else
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            if (_process.TGXacNhan <= DateTime.MinValue)
                        //                json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //            else
                        //                json.Instance.Html += "👩‍💻 Mã tra cứu: " + _process.MaLichHen + @" đang chờ nhận mẫu.";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //    }

                        //}
                        //if (model.step == 3)
                        //{
                        //    if (_process.TGXacNhan <= DateTime.MinValue)
                        //    {

                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //        json.Instance.Html += "</div>";
                        //    }
                        //    else
                        //    {
                        //        if (!_process.FullResult)
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            json.Instance.Html += "😔 Mã tra cứu: " + _process.MaLichHen + @" chưa có kết quả.";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //        else
                        //        {
                        //            if (!string.IsNullOrEmpty(_process.SID))
                        //            {
                        //                string _sid = AES.EncryptString(_process.SID.Trim(), "").Trim();
                        //                string tokenkey = SaltedHash.GetSHA512("m$dl4tec" + _process.SID.Trim());
                        //                string _url = "https://medlatec.vn/tra-cuu-ket-qua/sid?sid=" + _sid + "&place=HN&La=VN&token=" + tokenkey;

                        //                json.Instance.Html = @"<div class=""content_body"">";
                        //                json.Instance.Html += "✔️ Kết quả xét nghiệm của quý khách đã có lúc: " + Global.Utils.FormatDateTime(_process.ValidTime) + @". Để xem kết quả quý khách vui lòng click vào mã tra cứu màu xanh dưới đây (hoặc tải app iCNM):<br/>";
                        //                json.Instance.Html += @"Mã tra cứu: <a href=""" + _url + @""" target=""_blank"">" + _process.SID + @"</a>";
                        //                json.Instance.Html += "</div>";
                        //            }
                        //            else
                        //            {
                        //                json.Instance.Html = @"<div class=""content_body"">";
                        //                json.Instance.Html += "😔 Mã tra cứu: " + _process.MaLichHen + @" chưa có kết quả.";
                        //                json.Instance.Html += "</div>";
                        //            }
                        //        }
                        //    }
                        //}
                        //if (model.step == 4)
                        //{
                        //    if (_process.TGXacNhan <= DateTime.MinValue)
                        //    {

                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //        json.Instance.Html += "</div>";
                        //    }
                        //    else
                        //    {
                        //        if (!_process.FullResult)
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            json.Instance.Html += "😔 Mã tra cứu: " + _process.MaLichHen + @" chưa có kết quả.";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //        else
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            json.Instance.Html += @"Quý khách vui lòng gặp bác sĩ chỉ định để khám hoặc liên hệ tổng đài: <br/> <a href=""tel:1900565656"" target=""_blank"">☎️ 1900 565656</a> để được tư vấn kết quả xét nghiệm";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //    }
                        //}
                        //if (model.step == 5)
                        //{
                        //    if (_process.TGXacNhan <= DateTime.MinValue)
                        //    {

                        //        json.Instance.Html = @"<div class=""content_body"">";
                        //        json.Instance.Html += "😌 Lịch hẹn của quý khách đang được chờ xác nhận.";
                        //        json.Instance.Html += "</div>";
                        //    }
                        //    else
                        //    {
                        //        if (!_process.FullResult)
                        //        {
                        //            json.Instance.Html = @"<div class=""content_body"">";
                        //            json.Instance.Html += "😔 Mã tra cứu: " + _process.MaLichHen + @" chưa có kết quả.";
                        //            json.Instance.Html += "</div>";
                        //        }
                        //        else
                        //        {
                        //            if (_process.TGHenTaiKham > DateTime.MinValue)
                        //            {
                        //                json.Instance.Html = @"<div class=""content_body"">";
                        //                json.Instance.Html += @"Với kết quả xét nghiệm như hiện tại, quý khách hàng nên tái khám lại vào ngày " + Global.Utils.FormatDate(_process.TGHenTaiKham) + @" để đảm bảo tình trạng sức khỏe tốt nhất. Xin cảm ơn! <br/>";
                        //                json.Instance.Html += @"<a href=""javascript:void(0)"" >Đã hẹn tái khám</a>";
                        //                json.Instance.Html += "</div>";
                        //            }
                        //            else
                        //            {
                        //                json.Instance.Html = @"<div class=""content_body"">";
                        //                json.Instance.Html += @"Chưa hẹn tái khám.";
                        //                json.Instance.Html += "</div>";
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                }
                else { json.Instance.Message = "Lịch hẹn không tồn tại"; }
                json.Create();

            }

        }

        public void ActionRate(GetSearchModel model)
        {
            var json = new Json();

            if (string.IsNullOrEmpty(model.token)) json.Instance.Message = "Lịch hẹn không tồn tại.";
            else
            {
                int _id = Core.Global.Convert.ToInt(Core.Global.Security.Decrypt(HttpUtility.UrlDecode(model.token)).Replace("MrReddevil_", string.Empty));

                var _process = ModProcessService.Instance.CreateQuery()
                                                        .Where(o => o.ProcessID == _id)
                                                        .ToSingle();
                if (_process != null)
                {
                    _process.DiemDanhGia = model.step / 4;
                    _process.NoiDungDanhGia = Regex.Replace(model.content, @"<script[^>]*>[\s\S]*?</script>", "");
                    _process.ID = _process.ProcessID;

                    ModProcessService.Instance.Save(_process, o => new { o.DiemDanhGia, o.NoiDungDanhGia });
                    json.Instance.Html += @"<i class=""fa fa-star" + (_process.DiemDanhGia >= 1 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 2 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 3 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 4 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 5 ? "" : "-o") + @"""></i>";
                    json.Instance.Message = "Cảm ơn Quý khách đã đánh giá dịch vụ của MEDLATEC";
                }
            }
            json.Create();
        }

        public void ActionRateAdvisory(GetSearchModel model)
        {
            var json = new Json();

            if (string.IsNullOrEmpty(model.token)) json.Instance.Message = "Lịch hẹn không tồn tại.";
            else
            {
                try
                {
                    int _id = Core.Global.Convert.ToInt(Core.Global.Security.Decrypt(HttpUtility.UrlDecode(model.token)).Replace("MrReddevil_", string.Empty));

                    var _process = ModProcessService.Instance.CreateQuery()
                                                           .Where(o => o.ProcessID == _id)
                                                           .ToSingle();
                    if (_process != null)
                    {
                        var _rate = ModRateServiceService.Instance.CreateQuery().Where(o => o.ProcessID == _process.ProcessID && o.ScheduleCode == _process.MaLichHen).ToSingle_Cache();
                        if (_rate == null)
                        {
                            _rate = new ModRateServiceEntity();
                            _rate.ProcessID = _process.ProcessID;
                            _rate.Comment = Regex.Replace(model.content, @"<script[^>]*>[\s\S]*?</script>", "");
                            _rate.DoctorID = _process.UserTuVan.Trim();
                            _rate.DateCreated = DateTime.Now;
                            _rate.DoctorLevel = model.doctocLevel;
                            _rate.DoctorPoint = model.doctorPoint;
                            _rate.TimeAdvisoryPoint = model.timeAdvisoryPoint;
                            _rate.ScheduleCode = _process.MaLichHen;
                            _rate.PointService = model.pointSevice;
                            ModRateServiceService.Instance.Save(_rate);

                            _process.DiemDanhGia = (_rate.DoctorLevel + _rate.DoctorPoint + _rate.TimeAdvisoryPoint + _rate.PointService) / 4;
                            json.Instance.Html += @"<i class=""fa fa-star" + (_process.DiemDanhGia >= 1 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 2 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 3 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 4 ? "" : "-o") + @"""></i><i class=""fa fa-star" + (_process.DiemDanhGia >= 5 ? "" : "-o") + @"""></i>";

                            json.Instance.Message = "Cảm ơn quý khách đã dành thời gian đánh giá và góp ý cho dịch vụ của MEDLATEC.";
                            json.Instance.Js = _rate.ID.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.Instance.Message = "Có lỗi xảy ra khi đánh giá. Vui lòng đợi và thử lại.";
                }

            }
            json.Create();
        }
    }
    public class GetSearchModel
    {
        public string token { get; set; }
        public string content { get; set; }
        public int step { get; set; }
        public int doctocLevel { get; set; }
        public int doctorPoint { get; set; }
        public int timeAdvisoryPoint { get; set; }
        public int pointSevice { get; set; }


    }
    public class SecurityModel
    {
        public string Code { get; set; }
    }


    public class Subcribe
    {
        public int email { get; set; }
    }

}
