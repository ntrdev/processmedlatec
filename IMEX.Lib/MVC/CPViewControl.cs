﻿using System.Collections.Generic;
using IMEX.Core.Models;
using IMEX.Lib.Global;
using IMEX.Lib.Models;

namespace IMEX.Lib.MVC
{
    public class CPViewControl : Core.MVC.ViewControl
    {
        private readonly string[] _arrColor = { "blue", "yellow", "red", "green", "dark" };
        private readonly string[] _arrCommand = "import,export,new,edit,publish,unpublish,delete,copy,config,apply,save,save-new,upload,cancel".Split(',');
        private readonly string[] _arrClass = "fa-files-o,fa-import,fa-plus-circle,fa-pencil-square-o,fa-check-circle,fa-ban,fa-ban,fa-files-o,fa-undo,fa-check,fa-save,fa-plus,fa-plus,fa-ban".Split(',');

        public CPViewPage CPViewPage => Page as CPViewPage;




        protected string GetName(EntityBase entityBase)
        {
            return entityBase == null ? string.Empty : entityBase.Name;
        }

        protected string GetOrder(int id, int order)
        {

            return @"<div class=""input-group input-group-sm input-group-button orderBy"">
                                        <input type=""number"" class=""form-control"" placeholder=""vị trí"" id=""order[" + id + @"]"" value=""" + order + @""" size=""10"" >
                                        <div class=""input-group-append"">
                                            <button class=""btn btn-info"" onclick=""reddevil_exec_cmd('saveorder')"" type=""button""  data-toggle=""tooltip"" data-original-title=""Lưu sắp xếp"">
                                                <svg aria-hidden=""true"" focusable=""false"" data-prefix=""fal"" data-icon=""save"" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512"" class=""svg-inline--fa fa-save fa-w-14 fa-2x"">
                                                    <path fill=""currentColor"" d=""M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM288 64v96H96V64h192zm128 368c0 8.822-7.178 16-16 16H48c-8.822 0-16-7.178-16-16V80c0-8.822 7.178-16 16-16h16v104c0 13.255 10.745 24 24 24h208c13.255 0 24-10.745 24-24V64.491a15.888 15.888 0 0 1 7.432 4.195l83.882 83.882A15.895 15.895 0 0 1 416 163.882V432zM224 232c-48.523 0-88 39.477-88 88s39.477 88 88 88 88-39.477 88-88-39.477-88-88-88zm0 144c-30.879 0-56-25.121-56-56s25.121-56 56-56 56 25.121 56 56-25.121 56-56 56z"" class=""""></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>";
        }

        protected string GetCheckbox(int id, int index)
        {
            return @"<div class=""md-checkbox""   data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chọn để chỉnh sửa hoặc xóa"" >
                                  <input type=""checkbox"" id=""cb" + index + @""" name=""cid"" value=""" + id + @""" onclick=""isChecked(this.checked)"" class=""md-check check-select"" />
                                        <label for=""cb" + index + @""">
                                            <span></span>
                                            <span class=""check""></span>
                                            <span class=""box""></span>
                                        </label>
                            </div>";
        }

        protected string GetDefault(int id, bool defaut)
        {
            return @" <div class=""switchToggle"">
                                        <div class=""anil_nepal"">
                                            <label class=""switch switch-left-right""  data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để chọn mặc định hoặc bỏ mặc định"">
                                                <input class=""switch-input"" type=""checkbox"" " + (defaut ? "checked" : "") + @"  onclick=""reddevil_exec_cmd('[defaultgx][" + id + @"]'); return false"" >
                                                <span class=""switch-label"" data-on=""Bật"" data-off=""Tắt""></span><span class=""switch-handle""></span>
                                            </label>
                                        </div>
                                    </div>";
        }
        protected string GetOnDetail(int id, bool activity)
        {
            return @"<a href=""javascript:void(0)"" " + (activity ? "style=\"color: #559d01\"" : "style=\"color: red\"") + @"  onclick=""reddevil_exec_cmd('[onoffadv][" + id + "," + !activity + @"]'); return false"" data-toggle=""tooltip"" data-original-title=""Click để hiển thị ở trang chi tiết hoặc ẩn"">
                        <span class=""fa " + (activity ? "fa-check-circle publish" : " fa-times-circle-o unpublish") + @"""></span>
                    </a>";
        }
        protected string GetHideOrShow(int id, bool activity)
        {
            return @" <div class=""switchToggle"">
                                        <div class=""anil_nepal"">
                                            <label class=""switch switch-left-right"" data-toggle=""tooltip""  data-original-title=""Click để hiển thị hoặc ẩn"">
                                                <input class=""switch-input"" type=""checkbox"" " + (activity ? "checked" : "") + @" onclick=""reddevil_exec_cmd('[publishgx][" + id + "," + !activity + @"]'); return false"" >
                                                <span class=""switch-label"" data-on=""Hiện"" data-off=""Ẩn"" ></span><span class=""switch-handle""></span>
                                            </label>
                                        </div>
                                    </div>";
        }
        protected string GetPublish(int id, bool activity)
        {
            return @" <div class=""switchToggle"">
                                        <div class=""anil_nepal"">
                                            <label class=""switch switch-left-right"" data-toggle=""tooltip""  data-original-title=""Click để duyệt hoặc bỏ duyệt"">
                                                <input class=""switch-input"" type=""checkbox"" " + (activity ? "checked" : "") + @" onclick=""reddevil_exec_cmd('[publishgx][" + id + "," + !activity + @"]'); return false"" >
                                                <span class=""switch-label"" data-on=""Đã duyệt"" data-off=""Chờ duyệt"" ></span><span class=""switch-handle""></span>
                                            </label>
                                        </div>
                                    </div>";
        }
        protected string GetHideOrShowTop(int id, bool activity)
        {
            return @" <div class=""switchToggle"">
                                        <div class=""anil_nepal"">
                                            <label class=""switch switch-left-right"" data-toggle=""tooltip""  data-original-title=""Click để hiển thị hoặc ẩn"">
                                                <input class=""switch-input"" type=""checkbox"" " + (activity ? "checked" : "") + @" onclick=""reddevil_exec_cmd('[hide][" + id + "," + !activity + @"]'); return false"" >
                                                <span class=""switch-label"" data-on=""Hiển thị"" data-off=""Ẩn"" ></span><span class=""switch-handle""></span>
                                            </label>
                                        </div>
                                    </div>";
        }
        protected string GetHide(int id, bool hide)
        {
            return @"<a href=""javascript:void(0)"" " + (hide ? "style=\"color: #559d01\"" : "style=\"color: red\"") + @" onclick=""reddevil_exec_cmd('[hide][" + id + "," + !hide + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để ẩn hoặc hiển thị"">
                        <span class=""fa " + (hide ? "fa-check-circle publish" : " fa-times-circle-o unpublish") + @"""></span>
                    </a>";
        }

        protected string GetLock(int id, bool _lock)
        {
            return @"<a href=""javascript:void(0)"" onclick=""reddevil_exec_cmd('[lockgx][" + id + "," + !_lock + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để khóa hoặc hủy khóa"">
                        <span class=""fa " + (_lock ? "fa-check-circle publish" : "fa-dot-circle-o unpublish") + @"""></span>
                    </a>";
        }
        protected string GetMall(int id, bool _lock)
        {
            return @"<a href=""javascript:void(0)"" onclick=""reddevil_exec_cmd('[mallgx][" + id + "," + !_lock + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để hiển thị hoặc ẩn"">
                        <span class=""fa " + (_lock ? "fa-check-circle publish" : "fa-dot-circle-o unpublish") + @"""></span>
                    </a>";
        }
        protected string GetDisplayHome(int id, bool displayHome)
        {
            return @"<a href=""javascript:void(0)"" onclick=""reddevil_exec_cmd('[displayhome][" + id + "," + !displayHome + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để hiển thị và hủy"">
                        <span class=""fa " + (displayHome ? "fa-check-circle publish" : "fa-dot-circle-o unpublish") + @"""></span>
                    </a>";
        }

        protected string GetDisplayCat(int id, bool displayCat)
        {
            return @"<a href=""javascript:void(0)"" onclick=""reddevil_exec_cmd('[displaycat][" + id + "," + !displayCat + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để hiển thị và hủy"">
                        <span class=""fa " + (displayCat ? "fa-check-circle publish" : "fa-dot-circle-o unpublish") + @"""></span>
                    </a>";
        }

        protected string GetShowMenuTop(int id, bool showMenuTop)
        {
            return @"<a href=""javascript:void(0)"" onclick=""reddevil_exec_cmd('[showmenutop][" + id + "," + !showMenuTop + @"]'); return false"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để h.thị và không h.thị"">
                        <span class=""fa " + (showMenuTop ? "fa-check-circle publish" : "fa-dot-circle-o unpublish") + @"""></span>
                    </a>";
        }

        protected string GetMultiple(bool multiple)
        {
            return @"<a href=""javascript:void(0)"" " + (multiple ? "style=\"color: #559d01\"" : "style=\"color: red\"") + @"  data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""" + (multiple ? "Chọn nhiều" : "Chọn 1") + @""">
                        <span class=""fa " + (multiple ? "fa-check-circle publish" : " fa-times-circle-o unpublish") + @"""></span>
                    </a>";
        }

        protected string GetSortUnLink(string name, string key)
        {
            return $@"<a href=""javascript:void(0)"">{name} {GetImgSortTypeDesc(key)}</a>";
        }

        protected string GetSortLink(string name, string key)
        {
            return $@"<a href=""javascript:REDDEVILRedirect('Index', '{key}-{GetSortTypeDesc(key)}', 'Sort')"">{name} {GetImgSortTypeDesc(key)}</a>";
        }

        protected string GetTinyAddCommand()
        {
            return GetListCommand("cancel|Đóng");
        }

        protected string GetSortAddCommand()
        {
            return GetListCommand("apply|Lưu,save|Lưu  &amp; đóng,cancel|Đóng");
        }

        protected string GetDefaultAddCommand()
        {
            return GetListCommand("apply|Lưu,save|Lưu  &amp; đóng,save-new|Lưu &amp; thêm,cancel|Đóng");
        }

        protected string GetDefaultAddCommand(string extension)
        {
            return GetListCommand(extension + ",apply|Lưu,save|Lưu  &amp; đóng,save-new|Lưu &amp; thêm,cancel|Đóng");
        }

        protected string GetTinyListCommand()
        {
            return GetListCommand("delete|Xóa,config|Xóa cache");
        }
        protected string GetTinyConfigCommand()
        {
            return GetListCommand("config|Xóa cache");
        }
        protected string GetSortListCommand()
        {
            return GetListCommand("new|Thêm,edit|Sửa,delete|Xóa,copy|Sao chép,config|Xóa cache");
        }

        protected string GetDefaultListCommand()
        {
            return GetListCommand("new|Thêm,edit|Sửa,publish|Hiện,unpublish|Ẩn,delete|Xóa,copy|Sao chép,config|Xóa cache");
        }

        protected string GetDefaultNoCopyListCommand()
        {
            return GetListCommand("new|Thêm,edit|Sửa,unpublish|Hùy duyệt,publish|Duyệt,delete|Xóa");
        }

        protected string GetSoftDefautCommand()
        {
            return GetListCommand("new|Thêm,edit|Sửa,delete|Xóa,config|Xóa cache");
        }

        protected string GetEditAndCacheCommand()
        {
            return GetListCommand("edit|Sửa,config|Xóa cache");
        }


        protected string GetDefaultListCommand(string extension)
        {
            return GetListCommand(extension + ",new|Thêm,edit|Sửa,publish|Hiện,unpublish|Ẩn,delete|Xóa,copy|Sao chép,config|Xóa cache");
        }

        protected string GetListCommand(string commands)
        {
            var arrCommand = commands.Split(',');

            var html = @"<button type=""button"" class=""btn default"" onclick=""javascript:back_history()"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Trở lại trang trước""><i class=""fa fa-angle-left""></i>Back</button>";

            for (var i = 0; i < arrCommand.Length; i++)
            {
                var key = arrCommand[i].Split('|')[0];
                var name = arrCommand[i].Split('|')[1];
                var color = _arrColor[i % _arrColor.Length];

                var classValue = _arrClass[System.Array.IndexOf(_arrCommand, key)];

                switch (key)
                {
                    case "new":
                        html += @"<button type=""button"" class=""btn btn-default " + color + @""" onclick=""REDDEVILRedirect('Add')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Thêm mới""><i class=""fa fa-plus""></i><span class=""hidden-xs"">" + name + @"</span></button>";
                        break;

                    case "delete":
                        html += @"<button type=""button"" class=""btn btn-default " + color + @""" onclick=""if(document.IMEXsoftForm.boxchecked.value>0){confirm_delete()}"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chọn để xóa""><i class=""fa fa-times-circle""></i><span class=""hidden-xs"">" + name + @"</span></button>";
                        break;

                    case "publish":
                    case "unpublish":
                    case "edit":
                    case "copy":
                        html += $@"<button type=""button"" class=""btn btn-default {color}"" onclick=""if(document.IMEXsoftForm.boxchecked.value>0){{reddevil_exec_cmd('{key}')}}"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""{name}""><i class=""fa {classValue}""></i><span class=""hidden-xs"">{name}</span></button>";
                        break;
                    case "export":
                        html += $@"<button type=""button"" class=""btn  btn-default {color}"" onclick=""if(document.IMEXsoftForm.boxchecked.value>0 && document.IMEXsoftForm.listshopidcopy.value.length>0){{reddevil_exec_cmd('{key}')}}"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""{name}""><i class=""fa {classValue}""></i><span class=""hidden-xs"">{name}</span></button>";
                        break;

                    default:
                        html += $@"<button type=""button"" class=""btn btn-default {color}"" onclick=""reddevil_exec_cmd('{key}')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""{name}""><i class=""fa {classValue}""></i><span class=""hidden-xs"">{name}</span></button>";
                        break;
                }
            }

            return html;
        }

        protected string GetPagination(int pageIndex, int pageSize, int totalRecord)
        {
            var pager = new Pager
            {
                IsCpLayout = true,
                ActionName = "Index",
                ParamName = "PageIndex",
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalRecord = totalRecord
            };

            pager.Update();

            var html = @"<div class=""dataTables_length"">
                            <label>Hiển thị</label>
                            " + ShowDDLLimit(pager.PageSize) + @"
                        </div>";

            html += @"  <div class=""dataTables_paginate"">
                            <ul class=""pagination pagination-sm"">
                                " + pager.Html + @"
                            </ul>
                        </div>";

            return html;
        }

        protected string ShowDDLLimit(int pageSize)
        {
            return ShowDDLLimit(pageSize, "Index");
        }

        protected string ShowDDLLimit(int pageSize, string key)
        {
            int[] arrSize = { 5, 10, 15, 20, 30, 50, 100 };

            var html = @"<select class=""form-control input-inline input-sm"" name=""limit"" id=""limit"" onchange=""REDDEVILRedirect('" + key + @"')"" size=""1"">";

            foreach (var t in arrSize)
            {
                html += @"<option value=""" + t + @""" " + (t == pageSize ? "selected" : string.Empty) + @">" + t + @"</option>";
            }

            html += @" </select>";

            return html;
        }

        protected string ShowDDLLang(int langID)
        {
            return ShowDDLLang(langID, "Index");
        }

        protected string ShowDDLLang(int langID, string key)
        {
            var list = SysLangService.Instance.CreateQuery().ToList_Cache();

            var html = @"<select class=""form-control input-sm"" name=""filter_lang"" id=""filter_lang"" onchange=""REDDEVILRedirect('" + key + @"','0','parent_id')"" size=""1"">";

            for (var i = 0; list != null && i < list.Count; i++)
            {
                html += @"<option value=""" + list[i].ID + @""" " + (list[i].ID == langID ? "selected" : string.Empty) + @">Ngôn ngữ " + list[i].Name + @"</option>";
            }

            html += @"</select>";

            return html;
        }

        protected string ShowMap(List<EntityBase> listMap)
        {
            var html = @"<li class=""breadcrumb-item"">
                            <i class=""fa fa-home""></i>
                            <a href = ""javascript:REDDEVILRedirect('Index', '0', 'ParentID')"">Root</a>
                        </li>";

            for (var i = 0; listMap != null && i < listMap.Count; i++)
            {
                html += @"<li class=""breadcrumb-item"">
                            <a href=""javascript:REDDEVILRedirect('Index', '" + listMap[i].ID + @"', 'ParentID')"">" + listMap[i].Name + @"</a>
                        </li>";
            }

            return html;
        }

        protected string ShowMessage()
        {
            var html = string.Empty;

            var result = html;
            if (Cookies.GetValue("message") != string.Empty)
            {
                html += @"<div class=""card-block"">
                            <div class=""alert alert-icon alert-white alert-success alert-dismissible fade show"" role=""alert""><i class=""fa fa-check""></i>" + Data.Base64Decode(Cookies.GetValue("message")) + @"</div>
                        </div>";

                Cookies.Remove("message");
            }
            else
            {
                var message = CPViewPage.Message;

                if (message == null || message.ListMessage.Count <= 0) return html;

                string classValue = ""; string icon = "";
                if (message.MessageTypeName == "error")
                {
                    classValue = "alert-danger";
                    icon = "fa-times-circle";
                }
                else if (message.MessageTypeName == "notice")
                {
                    classValue = "alert-info";
                    icon = "fa-exclamation-circle";
                }
                else
                {
                    classValue = "alert-success";
                    icon = "fa-check";
                }

                foreach (var m in message.ListMessage)
                    result = result + "<div class=\"alert alert-icon alert-white " + classValue + " alert-dismissible fade show\" role=\"alert\"><i class=\"fa " + icon + "\"></i><strong>Thông báo!</strong> " + m + ".</div>";

                html += @"  <div class=""card-block"">
                                " + result + @"
                            </div>";
            }

            return html;
        }

        //protected string ShowMessage()
        //{
        //    var html = string.Empty;

        //    var result = html;
        //    if (Cookies.GetValue("message") != string.Empty)
        //    {
        //        html += @"<div class=""note note-info"">
        //                    <p>" + Data.Base64Decode(Cookies.GetValue("message")) + @"</p>
        //                </div>";

        //        Cookies.Remove("message");
        //    }
        //    else
        //    {
        //        var message = CPViewPage.Message;

        //        if (message == null || message.ListMessage.Count <= 0) return html;

        //        var classValue = message.MessageTypeName == "error" ? "note-danger" : "note-info";

        //        foreach (var m in message.ListMessage)
        //            result = result + "<p>" + m + "</p>";

        //        html += @"  <div class=""note " + classValue + @""">
        //                        " + result + @"
        //                    </div>";
        //    }

        //    return html;
        //}

        protected void CreatePathUpload(string pathChild)
        {
            Directory.Create(Server.MapPath("~/Data/upload/" + pathChild));
        }

        #region private

        private string SortType => CPViewPage.PageViewState.GetValue("Sort").ToString().Trim().Split('-')[0]
            .Replace("'", string.Empty)
            .Replace("-", string.Empty)
            .Replace(";", string.Empty);

        private bool SortDesc => string.Equals("desc", CPViewPage.PageViewState.GetValue("Sort").ToString().Trim().Split('-')[1], System.StringComparison.OrdinalIgnoreCase);

        private string GetSortTypeDesc(string type)
        {
            if (type != SortType)
                return "desc";

            return SortDesc ? "asc" : "desc";
        }

        private string GetImgSortTypeDesc(string type)
        {
            if (type != SortType)
                return string.Empty;

            return SortDesc ? @"<i class=""fa fa-angle-down""></i>" : @"<i class=""fa fa-angle-up""></i>";
        }

        #endregion private
    }
}