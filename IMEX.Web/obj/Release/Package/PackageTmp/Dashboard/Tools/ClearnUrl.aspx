﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CPLogin.IsLogin() && CPLogin.CurrentUser.IsAdministrator) return;

        Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
    }
    private static int GetMaxOrder()
    {
        return ModCategoryService.Instance.CreateQuery()
                .Max(o => o.Order)
                .ToValue().ToInt(0) + 1;
    }

    private static int GetMaxOrder2()
    {
        return ModDictionaryService.Instance.CreateQuery()
                .Max(o => o.Order)
                .ToValue().ToInt(0) + 1;
    }
    private ModDictionaryEntity _item = null;
    //protected void btnRun_Click(object sender, EventArgs e)
    //{

    //    int haha = 0;
    //    // Query the data and write out a subset of contacts

    //    var listCode = ModDMCPService.Instance.CreateQuery().Select(o => new { o.TenCP, o.MaCP })
    //                                            .Where(o => o.ManhCP == "XN").ToList_Cache();
    //    for (int i = 0; listCode != null && i < listCode.Count; i++)
    //    {
    //        _item = new ModDictionaryEntity();
    //        _item.TestCode = listCode[i].MaCP;
    //        _item.CodeName = listCode[i].TenCP;
    //        _item.DateCreate = DateTime.Now;
    //        _item.Active = true;
    //        _item.Order = GetMaxOrder2();
    //        _item.UserUpdate = CPLogin.UserID;
    //        ModDictionaryService.Instance.Save(_item);
    //        haha++;
    //    }
    //     Response.Write(haha);

    //}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SQL</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center;">
                <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Run" Width="111px" />
                <br />
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                
            </div>
        </div>
    </form>
</body>
</html>
