﻿using System;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace IMEX.Lib.Global
{
    public class Similarity
    {
        private static int Distance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if (source.Length == 0 || target.Length == 0) return 0;
            if (source == target) return source.Length;

            source = Data.GetCode(source);
            target = Data.GetCode(target);

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

        public static double Calculate(string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            //source = Data.GetCode(source);
            //target = Data.GetCode(target);

            int stepsToSame = Distance(source, target);
            return (1.0 - ((double)stepsToSame / Math.Max(source.Length, target.Length)));
        }
    }
}