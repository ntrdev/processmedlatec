﻿using System;
using IMEX.Core.Interface;
using IMEX.Lib.Global;
using IMEX.Lib.Models;

namespace IMEX.Lib.MVC
{
    public class ViewPage : Core.MVC.ViewPage
    {
        public ViewPage()
        {
            LangService = SysLangService.Instance;
            ModuleService = SysModuleService.Instance;
            SiteService = SysSiteService.Instance;
            TemplateService = SysTemplateService.Instance;
            PageService = SysPageService.Instance;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ResourceService = new IniSqlResourceService(CurrentLang);

            //lượt truy cập
            //Lib.Global.Utils.UpdateOnline();
        }

        protected override IPageInterface PageNotFound()
        {
            Error404();
            return null;
        }

        public void Error404()
        {
            Core.Web.HttpRequest.Error404();
        }

        public new SysSiteEntity CurrentSite => base.CurrentSite as SysSiteEntity;
        public new SysTemplateEntity CurrentTemplate => base.CurrentTemplate as SysTemplateEntity;
        public new SysPageEntity CurrentPage => base.CurrentPage as SysPageEntity;
        public new SysLangEntity CurrentLang => base.CurrentLang as SysLangEntity;

        public ModCleanURLEntity CurrentCleanUrl => ViewBag.CleanURL as ModCleanURLEntity;

        private string _currentUrl;
        public string CurrentURL => _currentUrl ?? (_currentUrl = GetPageURL(CurrentPage));

        //Module
        private string _feedbackUrl;
        public string FeedbackUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_feedbackUrl)) return _feedbackUrl;

                var item = SysPageService.Instance.CreateQuery()
                                    .Select(o => o.Url)
                                    .Where(o => o.Activity == true && o.ModuleCode == "MFeedback" && o.LangID == CurrentLang.ID)
                                    .ToSingle_Cache();

                if (item != null) _feedbackUrl = GetURL(0, item.Url);

                return _feedbackUrl;
            }
        }

        private string _searchUrl;
        public string SearchUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_searchUrl)) return _searchUrl;

                var item = SysPageService.Instance.CreateQuery()
                                    .Select(o => o.Url)
                                    .Where(o => o.Activity == true && o.ModuleCode == "MSearch" && o.LangID == CurrentLang.ID)
                                    .ToSingle_Cache();

                if (item != null) _searchUrl = GetURL(0, item.Url);

                return _searchUrl;
            }
        }


        #region shop

        private string _buninesskUrl;
        public string BuninesskUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninesskUrl)) return _buninesskUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessProduct" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _buninesskUrl = GetURL(0, item.Url);

                return _buninesskUrl;
            }
        }

        private string _ShopMaketingkUrl;
        public string ShopMaketingUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_ShopMaketingkUrl)) return _ShopMaketingkUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MShopMaketing" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _ShopMaketingkUrl = GetURL(0, item.Url);

                return _ShopMaketingkUrl;
            }
        }


        private string _MarketingRegisteredUrl;
        public string MarketingRegisteredUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_MarketingRegisteredUrl)) return _MarketingRegisteredUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MMarketingRegistered" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _MarketingRegisteredUrl = GetURL(0, item.Url);

                return _MarketingRegisteredUrl;
            }
        }


        private string _registerShopUrl;
        public string RegisterShopUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_registerShopUrl)) return _registerShopUrl;

                var item = SysPageService.Instance.CreateQuery()
                                    .Select(o => o.Url)
                                    .Where(o => o.Activity == true && o.ModuleCode == "MShopRegister" && o.LangID == CurrentLang.ID)
                                    .ToSingle_Cache();

                if (item != null) _registerShopUrl = GetURL(0, item.Url);

                return _registerShopUrl;
            }
        }


        private string _buninessSettingCpUrl;
        public string BuninessSettingCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessSettingCpUrl)) return _buninessSettingCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MShopSettingCP" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessSettingCpUrl = GetURL(0, item.Url);

                return _buninessSettingCpUrl;
            }
        }
        private string _buninessOrderCpUrl;
        public string BuninessOrderCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessOrderCpUrl)) return _buninessOrderCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MShopOrder" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessOrderCpUrl = GetURL(0, item.Url);

                return _buninessOrderCpUrl;
            }
        }

        private string _buninessReviewCpUrl;
        public string BuninessReviewCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessReviewCpUrl)) return _buninessReviewCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MShopReview" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessReviewCpUrl = GetURL(0, item.Url);

                return _buninessReviewCpUrl;
            }
        }

        private string _buninessRatingCpUrl;
        public string BuninessRatingCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessRatingCpUrl)) return _buninessRatingCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MShopProductRating" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessRatingCpUrl = GetURL(0, item.Url);

                return _buninessRatingCpUrl;
            }
        }


        private string _buninessNotificationCpUrl;
        public string BuninessNotificationCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessNotificationCpUrl)) return _buninessNotificationCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MUserNotification" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessNotificationCpUrl = GetURL(0, item.Url);

                return _buninessNotificationCpUrl;
            }
        }

        private string _buninessMessengerCpUrl;
        public string BuninessMessengerCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessMessengerCpUrl)) return _buninessMessengerCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MUserMessenger" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessMessengerCpUrl = GetURL(0, item.Url);

                return _buninessMessengerCpUrl;
            }
        }

        private string _ProductUrl;
        public string ProductUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_ProductUrl)) return _ProductUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Url)
                    .Where(o => o.Activity == true && o.ModuleCode == "MProduct" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _ProductUrl = GetURL(0, item.Url);

                return _ProductUrl;
            }
        }

        #endregion

        #region user

        private string _oViewUserURL;
        public string ViewScientistURL
        {
            get
            {
                if (!string.IsNullOrEmpty(_oViewUserURL)) return _oViewUserURL;

                var item = SysPageService.Instance.CreateQuery()
                                    .Select(o => o.Url)
                                    .Where(o => o.Activity == true && o.ModuleCode == "MScientist" && o.LangID == CurrentLang.ID)
                                    .ToSingle_Cache();

                if (item != null) _oViewUserURL = GetURL(0, item.Url);

                return _oViewUserURL;
            }
        }


        #endregion user

        public Message Message { get; } = new Message();

        public string SortMode
        {
            get
            {
                var sort = Core.Web.HttpQueryString.GetValue("sort").ToString().ToLower().Trim();

                if (sort == "new_asc" || sort == "price_asc" || sort == "price_desc" || sort == "view_desc")
                    return sort;

                return "new_asc";
            }
        }

        public string GetURL(string key, string value)
        {
            var url = string.Empty;
            for (var i = 0; i < PageViewState.Count; i++)
            {
                var tempKey = PageViewState.AllKeys[i];
                var tempValue = PageViewState[tempKey].ToString();

                if (string.Equals(tempKey, key, StringComparison.OrdinalIgnoreCase) || string.Equals(tempKey, "rdv", StringComparison.OrdinalIgnoreCase) || tempKey.IndexOf("web.", StringComparison.OrdinalIgnoreCase) >= 0)
                    continue;

                if (url.Length == 0)
                    url = "?" + tempKey + "=" + Server.UrlEncode(tempValue);
                else
                    url += "&" + tempKey + "=" + Server.UrlEncode(tempValue);
            }

            url += (url == string.Empty ? "?" : "&") + key + "=" + value;

            return url;
        }

        public string GetURL(int menuID, string code)
        {
            return GetURL(code);
        }

    
        public string GetPageURL(SysPageEntity page)
        {
            var typeValue = page.Items.GetValue("Type").ToString();

            if (typeValue.Length == 0)
                return GetURL(page.Url);

            if (!typeValue.Equals("http", StringComparison.OrdinalIgnoreCase)) return "javascript:void(0)";

            var target = page.Items.GetValue("Target").ToString();
            var url = page.Items.GetValue("URL").ToString();

            if (url.Length == 0)
                url = page.Url;

            return url.Replace("{URLBase}/", URLBase).Replace("{PageExt}", PageExt) + (target == string.Empty ? string.Empty : "\" target=\"" + target);
        }
      
        public bool IsPageActived(SysPageEntity pageToCheck)
        {
            if (CurrentPage.ID == pageToCheck.ID)
                return true;

            var page = (SysPageEntity)CurrentPage.Clone();
            while (true)
            {
                page = SysPageService.Instance.GetByID_Cache(page.ParentID);

                if (page == null || page.ParentID == 0)
                    return false;

                if (page.ID == pageToCheck.ID)
                    return true;
            }
        }

        public bool IsPageActived(SysPageEntity page, int index)
        {
            return CurrentPage.ID == page.ID || CurrentVQS.Equals(index, page.Url);
        }

        public void Back(int step)
        {
            JavaScript.Back(step, Page);
        }

        public void Navigate(string url)
        {
            JavaScript.Navigate(url, Page);
        }

        public void Close()
        {
            JavaScript.Close(Page);
        }

        public void Script(string key, string script)
        {
            JavaScript.Script(key, script, Page);
        }

        public void RefreshPage()
        {
            Response.Redirect(Request.RawUrl);
        }

        #region zebradialog
        public string ShowMessage()
        {
            var html = string.Empty;

            var result = html;
            if (Cookies.GetValue("message") != string.Empty)
            {
                html += @"<div class=""note note-info"">
                            <p>" + Data.Base64Decode(Cookies.GetValue("message")) + @"</p>
                        </div>";

                Cookies.Remove("message");
            }
            else
            {
                var message = Message;

                if (message == null || message.ListMessage.Count <= 0) return html;

                var classValue = message.MessageTypeName == "error" ? "note-danger" : "note-info";

                foreach (var m in message.ListMessage)
                    result = result + "<p>" + m + "</p>";

                html += @"  <div class=""note " + classValue + @""">
                                " + result + @"
                            </div>";
            }

            return html;
        }

        public void Alert(string title, string content)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content)) return;

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "AlertScript", string.Format("<script type=\"text/javascript\">swee_alert('" + title + "', '" + content + "');</script>"));
        }

        public void Alert(string content)
        {
            Alert("Thông báo !", content);
        }

        public void AlertThenRedirect(string title, string content, string redirect)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content) || string.IsNullOrEmpty(redirect)) return;

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ConfirmScript", string.Format("<script type=\"text/javascript\">alert_redirect('" + title + "','" + content + "','" + redirect + "');</script>"));
        }

        public void AlertThenRedirect(string content, string redirect)
        {
            AlertThenRedirect("Thông báo !", content, redirect);
        }

        public void AlertThenRedirect(string content)
        {
            AlertThenRedirect("Thông báo !", content, "/");
        }

        public void Confirm(string title, string content, string redirect)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content) || string.IsNullOrEmpty(redirect))
            {
                return;
            }

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ConfirmScript", string.Format("<script type=\"text/javascript\">alert_redirect('" + title + "','" + content + "','" + redirect + "');</script>"));
        }

        public void Confirm(string content, string redirect)
        {
            Confirm("Thông báo !", content, redirect);
        }

        public void Confirm(string content)
        {
            Confirm("Thông báo !", content, "/");
        }

        #endregion zebradialog
    }
}