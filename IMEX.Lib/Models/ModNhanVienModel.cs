﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModNhanVienEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public string maNV { get; set; }

        [DataInfo]
        public string maCung { get; set; }

        [DataInfo]
        public string hoTen { get; set; }

        [DataInfo]
        public string soDT { get; set; }

        [DataInfo]
        public string maTN { get; set; }

        [DataInfo]
        public bool rac_NV { get; set; }

        #endregion Autogen by RDV


    }

    public class ModNhanVienService : ServiceBase<ModNhanVienEntity>
    {
        #region Autogen by RDV

        private ModNhanVienService() : base("[NhanVien]", "maNV")
        {
            DBConfigKey = "DBConnectCCOM";

        }

        private static ModNhanVienService _instance;
        public static ModNhanVienService Instance => _instance ?? (_instance = new ModNhanVienService());

        #endregion Autogen by RDV


    }
}