﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as FormLikeModel;
    var listItem = ViewBag.DataLike as List<ModNewsLikeEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetListCommand("config|Xóa cache") %>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Danh sách Thích bài viết</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">STT</th>
                                        <th class="sorting text-left">Khách hàng</th>
                                        <th class="sorting text-center hidden-sm hidden-col">Thời gian Like</th>
                                        <th class="text-center">Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%=i+1 %></td>
                                        <td><%=listItem[i].NameUser%></td>

                                        <td class="text-center hidden-sm hidden-col"><%= Utils.FormatDateTime2(listItem[i].TimeLike) %></td>
                                        <td class="text-center">
                                            <div class="switchToggle">
                                                <div class="anil_nepal">
                                                    <label class="switch switch-left-right" data-toggle="tooltip" data-original-title="Click để duyệt hoặc bỏ duyệt">
                                                        <input class="switch-input publish-like" data-id="<%=listItem[i].UserID%>" data-newsid="<%=listItem[i].NewsID%>" data-type="<%=listItem[i].Active%>" type="checkbox" <%=listItem[i].Active?"checked":"" %>>
                                                        <span class="switch-label" data-on="Đã duyệt" data-off="Chờ duyệt"></span><span class="switch-handle"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var REDDEVILController = "FormLike";

        var REDDEVILArrVar = [

            "filter_state", "State",
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];


    </script>

</form>
