﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">
    private int GetMaxOrder()
    {
        return ModCategoryService.Instance.CreateQuery()
                        .Max(o => o.Order)
                        .ToValue().ToInt(0) + 1;
    }

    int Insert(int parent_id, string name)
    {
        
        int id = ModCategoryService.Instance.CreateQuery()
                        .Where(o => o.Name == name && o.ParentID == parent_id)
                        .ToValue().ToInt(0);

        if (id > 0) return id;

        ModCategoryEntity entity = new ModCategoryEntity();
        entity.LangID = 1;
        entity.Type = "Region";
        entity.ParentID = parent_id;
        entity.Name = name;
        entity.Url = Data.GetCode(entity.Name);
        entity.DateCreate = DateTime.Now;
        entity.Order = GetMaxOrder();
        entity.Active = true;


        ModCategoryService.Instance.Save(entity);
        if (entity.ParentID != 6 && entity.ParentID > 0 && entity.ID > 0)
        {
            entity.Levels = entity.Parent.Levels + "-" + entity.ID;
            ModCategoryService.Instance.Save(entity, o => new { o.Levels });
        }
        return entity.ID;
    }

    void Insert(int parent_id)
    {
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.ReadXml(Server.MapPath("City.xml"));
        System.Data.DataTable dtCity = ds.Tables[0];

        for (int i = 0; i < dtCity.Rows.Count; i++)
        {
            int city_id = Insert(parent_id, dtCity.Rows[i]["city"].ToString());
            int district_id = Insert(city_id, dtCity.Rows[i]["district"].ToString());
            Insert(district_id, dtCity.Rows[i]["ward"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Insert(19);
        Response.Write("OK");
    }
</script>
