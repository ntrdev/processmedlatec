﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModRateServiceEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ScheduleCode { get; set; }//mã lịch hẹn

        [DataInfo]
        public int ProcessID { get; set; }//khách hàng đánh giá

        [DataInfo]
        public string DoctorID { get; set; }//mã nhân viên

        [DataInfo]
        public int PointService { get; set; }//Điểm hài lòng về dịch vụ

        [DataInfo]
        public int DoctorPoint { get; set; }//thái độ của bác sĩ

        [DataInfo]
        public int DoctorLevel { get; set; }//trình độ của bác sĩ

        [DataInfo]
        public int TimeAdvisoryPoint { get; set; }//thời gian tư vấn

        [DataInfo]
        public string Comment { get; set; }//lời nhận xét

        [DataInfo]
        public DateTime DateCreated { get; set; }//ngày đánh giá


        #endregion Autogen by RDV


    }

    public class ModRateServiceService : ServiceBase<ModRateServiceEntity>
    {
        #region Autogen by RDV

        private ModRateServiceService() : base("[Mod_RateService]")
        {

        }

        private static ModRateServiceService _instance;
        public static ModRateServiceService Instance => _instance ?? (_instance = new ModRateServiceService());

        #endregion Autogen by RDV


    }
}