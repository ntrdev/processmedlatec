﻿using IMEX.Lib.MVC;
using Controller = IMEX.Lib.MVC.Controller;

namespace IMEX.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Bài viết", Code = "MContent", Order = 1, Crawl = true)]
    public class MContentController : Controller
    {
        public void ActionIndex()
        {
          
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 600, 315);
        }
    }
}