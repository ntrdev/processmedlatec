﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModNhanVienLocationEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public string Lat { get; set; }

        [DataInfo]
        public string Lon { get; set; }

        [DataInfo]
        public string MaTN { get; set; }

      
        #endregion Autogen by RDV


    }

    public class ModNhanVienLocationService : ServiceBase<ModNhanVienLocationEntity>
    {
        #region Autogen by RDV

        private ModNhanVienLocationService() : base("[TN_ToaDo]")
        {
            DBConfigKey = "DBConnectCCOM";
        }

        private static ModNhanVienLocationService _instance;
        public static ModNhanVienLocationService Instance => _instance ?? (_instance = new ModNhanVienLocationService());

        #endregion Autogen by RDV


    }
}