﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.ViewControl" %>

<%

    var process = ViewBag.Process as ModProcessEntity;
    if (process == null) return;
    var employee = ViewBag.Employee as ModKPIEmployeeEntity;
    var customer = ViewBag.Customer as ModScheduleEntity ?? new ModScheduleEntity();
    var nhanvien = ViewBag.NhanVien as ModNhanVienEntity;
    var _rate = ModRateServiceService.Instance.CreateQuery().Where(o => o.ProcessID == process.ProcessID && o.ScheduleCode == process.MaLichHen).ToSingle_Cache() ?? new ModRateServiceEntity();
%>

<section class="box-process">
    <div class="container">
        <div class="row">
            <article class="thumbnail-process-view">
                <h1 class="title_new_detail d-flex-wrap mb10">Quản lý tiến trình lấy mẫu tại nhà</h1>
                <ul class="content-top-view d-flex-wrap mb10">

                    <li class="item  col-md-auto">
                        <div class="bg-item mb10">
                            <p class="info" style="font-size: 16px;"><b>Lịch hẹn Lấy mẫu xét nghiệm tại nhà</b></p>
                            <div style="margin-top: 8px">
                                <p class="d-flex-wrap info">
                                    <span>
                                        <img src="/interface/img/ic_person.png" alt="">&nbsp;&nbsp;<%=process.HoVaTen %></span>
                                </p>
                                <p class="info">
                                    <img src="/interface/img/ic_location-1.png" alt="">&nbsp;&nbsp;<%=Utils.FormatPhoneNumber(customer.Phone) %>
                                </p>
                                <p class="info">
                                    <img src="/interface/img/ic_clock.png" alt="">&nbsp;&nbsp;NV sẽ đến lúc: <%=process.KhungGio %>&nbsp;<%=Utils.FormatDate(process.TGDenDiaChi) %>
                                </p>
                                <p class="info" style="margin-top:10px!important">
                                    <img src="/interface/img/ic_location.png" alt="">&nbsp;&nbsp;<%=process.DiaChi %>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="item col-md-auto">
                        <div class="bg-item bg-img ">
                            <div class="img">
                                <figure class=" hm-reponsive">
                                    <img src="<%=Utils.ImageToBase64(employee.Anh) %>" alt="Ảnh Nhân Viên <%=employee.Hoten %>">
                                </figure>
                            </div>
                            <div class="txt-bg">
                                <p class="info" style="font-size: 16px;"><b>Thông tin NV lấy mẫu tại nhà</b></p>
                                <div style="margin-top: 8px">
                                    <p class="info">Họ tên: <%=employee.Hoten %></p>
                                    <p class="info">Mã NV: <%=employee.Manhanvien %></p>
                                    <p class="info">Ngày sinh: <%=Utils.FormatDate2(employee.NgaySinh) %></p>
                                    <p class="info">Đánh giá:&nbsp;<i class="fa fa-star" style="color: #ffc107"></i>5.0</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="view-note d-flex-wrap mb10">
                    <p><b>* Quý khách lưu ý chỉ đồng ý sử dụng dịch vụ khi nhận diện đúng NV lấy mẫu tại nhà qua các thông tin phía trên để đề phòng kẻ gian lợi dụng. <a data-toggle="modal" href="" data-target="#employee-img" title="">Xem nhận diện nhân viên lấy mẫu tại nhà của Medlatec </a></b></p>
                </div>
                <div class="process-list">
                    <ul class="process-list-cont">
                    </ul>
                </div>
            </article>
        </div>
    </div>
</section>
<input type="hidden" id="token" value="<%=process.token %>" />

<div id="register-form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close-model" data-dismiss="modal" style="color: #fff">x</a>
                <h4 class="modal-title text-center"><b>Đánh giá dịch vụ tại MEDLATEC</b></h4>
            </div>
            <div class="modal-body">
                <div class="main">
                    <div role="form" id="login_form" class="form-singup">
                        <div class="nx-ofkhachhang all" style="display: block">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">1. Quý khách vui lòng đánh giá về thời gian lấy mẫu của nhân viên</b>

                                                </li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="width: 28%; display: inline-flex; float: right" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-sp-1" type="number" class="stars" value="" />
                                                                <input type="hidden" id="Star_sp-1" class="start-rate" value="0" />
                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="pricingTable" style="margin-top: 4%">

                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">2. Quý khách vui lòng đánh giá về thái độ của nhân viên lấy mẫu</b>
                                                    <input type="hidden" id="Star_sp-2" class="start-rate" value="0" />
                                                </li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="width: 28%; display: inline-flex; float: right" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-sp-2" type="number" class="stars" value="" />

                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">3. Quý khách vui lòng đánh giá về trình độ tư vấn của nhân viên</b>
                                                    <input type="hidden" id="Star_sp-3" class="start-rate" value="0" /></li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="width: 28%; display: inline-flex; float: right" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-sp-3" type="number" class="stars" value="" />

                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">4. Quý khách vui lòng đánh giá về trình độ lấy mẫu của nhân viên</b>
                                                    <input type="hidden" id="Star_sp-4" class="start-rate" value="0" /></li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="width: 28%; display: inline-flex; float: right" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-sp-4" type="number" class="stars" value="" />

                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="form-group">
                                            <label for="dateCustomer">5. Quý khách có góp ý giúp nâng cao chất lượng dịch vụ của MEDLATEC?</label>
                                            <textarea class="form-control " id="comment" name="comment" placeholder="Nhập góp ý của bạn tại đây" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <a href="javascrpit:void(0)" onclick="postRate()" class="guinhanxet-kh" type="button">Gửi</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div id="rate-form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close-model" data-dismiss="modal" style="color: #fff">x</a>
                <h4 class="modal-title text-center"><b>Đánh giá dịch vụ tại MEDLATEC</b></h4>
            </div>
            <div class="modal-body">
                <div class="main">
                    <div role="form" id="rate_form" class="form-singup">
                        <div class="nx-ofkhachhang all" style="display: block">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">1. Quý khách vui lòng đánh giá về thời gian tư vấn của bác sĩ.</b>

                                                </li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="display: inline-flex;" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-rate-1" type="number" class="stars" value="<%=_rate.TimeAdvisoryPoint %>" />
                                                                <input type="hidden" id="TimeAdvisoryPoint" name="TimeAdvisoryPoint" class="start-rate-doctor" value="<%=_rate.TimeAdvisoryPoint %>" />
                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="pricingTable" style="margin-top: 4%">

                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">2. Quý khách vui lòng đánh giá về thái độ của bác sĩ.</b>
                                                    <input type="hidden" id="DoctorPoint" name="DoctorPoint" class="start-rate-doctor" value="<%=_rate.DoctorPoint %>" />
                                                </li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="display: inline-flex;" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-rate-2" type="number" class="stars" value="<%=_rate.DoctorPoint %>" />

                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">3. Quý khách vui lòng đánh giá về trình độ tư vấn của bác sĩ</b>
                                                    <input type="hidden" id="DoctorLevel" class="start-rate-doctor" value="<%=_rate.DoctorLevel %>" /></li>
                                                <li>
                                                    <div class="dg-sao">
                                                        <span>
                                                            <div class="ratingStars" itemprop="reviewRating" style="display: inline-flex;" itemscope itemtype="http://schema.org/Rating">
                                                                <input id="rating-input-rate-3" type="number" class="stars" value="<%=_rate.DoctorLevel %>" />

                                                            </div>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="pricing-content">
                                            <ul class="pricing-content">
                                                <li><b class="dv">4. Hãy cho chúng tôi biết mức độ hài lòng của quý khách về chất lượng dịch vụ của MEDLATEC</b></li>
                                                <li>
                                                    <div class="dg-sao">

                                                        <div class="process_img ">
                                                            <i class="icon-reversed" data-star="1" data-img-grey="/interface/img/ic_sad_grey.png" data-img="/interface/img/ic_sad.png">
                                                                <img src="/interface/img/ic_sad_grey.png"><p><b>Rất không hài lòng</b></p>
                                                            </i>
                                                        </div>
                                                        <div class="process_img ">
                                                            <i class="icon-reversed" data-star="2" data-img-grey="/interface/img/ic_mediumsad_grey.png" data-img="/interface/img/ic_mediumsad.png">
                                                                <img src="/interface/img/ic_mediumsad_grey.png"><p><b>Không hài lòng</b></p>
                                                            </i>
                                                        </div>
                                                        <div class="process_img ">
                                                            <i class="icon-reversed" data-star="3" data-img-grey="/interface/img/ic_unhappy_grey.png" data-img="/interface/img/ic_unhappy.png">
                                                                <img src="/interface/img/ic_unhappy_grey.png" data-img-grey="/interface/img/ic_unhappy_grey.png"><p><b>Trung lập</b></p>
                                                            </i>
                                                        </div>
                                                        <div class="process_img ">
                                                            <i class="icon-reversed" data-star="4" data-img-grey="/interface/img/ic_satisfy_grey.png" data-img="/interface/img/ic_satisfy.png">
                                                                <img src="/interface/img/ic_satisfy_grey.png"><p><b>Hài lòng</b></p>
                                                            </i>
                                                        </div>
                                                        <div class="process_img ">
                                                            <i class="icon-reversed" data-star="5" data-img-grey="/interface/img/ic_love_grey.png" data-img="/interface/img/ic_love.png">
                                                                <img src="/interface/img/ic_love_grey.png"><p><b>Rất hài lòng</b></p>
                                                            </i>
                                                        </div>
                                                        <input type="hidden" name="PointService" id="PointService" value="<%=_rate.PointService %>" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="pricingTable" style="margin-top: 4%">
                                        <div class="form-group">
                                            <label for="dateCustomer"><b class="dv">5. Quý khách có góp ý giúp nâng cao chất lượng dịch vụ của MEDLATEC?</b></label>
                                            <textarea class="form-control " id="comment_bacsi" name="Comment" placeholder="Nhập góp ý của bạn tại đây" rows="3"><%=_rate.Comment %></textarea>
                                        </div>
                                    </div>
                                </div>
                                <%if (_rate.ProcessID < 1)
                                { %>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <a href="javascrpit:void(0)" onclick="postRate_service()" class="guinhanxet-kh" type="button">Gửi</a>
                                    </div>
                                </div>
                                <%} %>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


<div id="employee-img" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="justify-content: center !important; border-bottom: none">
                <h4 class="modal-title " style="text-transform: none !important; color: #108EED"><b>Nhận dạng Nhân viên Lấy mẫu tại nhà của Medlatec</b></h4>
            </div>
            <div class="modal-body">
                <div class="main">
                    <div role="form" class="form-singup">
                        <div class="nx-ofkhachhang all" style="display: block">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="">
                                        <div class="pricing-content">
                                            <ul class="d-flex-wrap" style="justify-content: center !important;">
                                                <li>
                                                    <img src="/interface/img/canbotainha-1.png" alt="">
                                                </li>
                                                <li>
                                                    <img src="/interface/img/can-bo-tai-nha-2.png" alt="">
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="pricing-content">
                                            <ul class="d-flex-wrap" style="justify-content: center !important;">
                                                <li>
                                                    <div id="tab-customer" class="tab-customer turn-up--content">
                                                        <div class="form-group form-group--btn clearfix">
                                                            <a data-dismiss="modal" style="width: 100%" class="btn-form--submit">Đóng</a>
                                                        </div>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


<link rel="stylesheet" href="/interface/pc/css/modules.css" />
<script src="/interface/pc/js/star-rating.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        setTimeout(function () {
            loadprocess2($('#token').val(), '.process-list-cont');
        }, 500);
        setTimeout(function () {
            $('#PointService').val(<%=_rate.PointService%>);
            var _mapImg = $(".icon-reversed").map(function () {
                return this;
            }).get();

            for (var i = 0; i < _mapImg.length; i++) {
                if ($(_mapImg[i]).data('star') == '<%=_rate.PointService%>') {
                    $(_mapImg[i]).children().attr('src', $(_mapImg[i]).data('img'));
                }
                else {
                    $(_mapImg[i]).children().attr('src', $(_mapImg[i]).data('img-grey'));
                }
            }
        }, 1800);

    });

    $("input[id^='rating-input-sp-']").rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'sm',
        showClear: false,
        showCaption: false,
        readonly: false
    });
    $("input[id^='rating-input-rate-']").rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'sm',
        showClear: false,
        showCaption: false,
        readonly: false
    });



    $("input[id^='rating-input-sp-']").rating('update', 0);
    $('#rating-input-sp-1').on('rating.change', function (event, value, caption) {
        $('#Star_sp-1').val(value);
    });

    $('#rating-input-sp-2').on('rating.change', function (event, value, caption) {
        $('#Star_sp-2').val(value);
    });

    $('#rating-input-sp-3').on('rating.change', function (event, value, caption) {
        $('#Star_sp-3').val(value);
    });

    $('#rating-input-sp-4').on('rating.change', function (event, value, caption) {
        $('#Star_sp-4').val(value);
    });


    $('#rating-input-rate-1').on('rating.change', function (event, value, caption) {
        $('#TimeAdvisoryPoint').val(value);
    });

    $('#rating-input-rate-2').on('rating.change', function (event, value, caption) {
        $('#DoctorPoint').val(value);
    });

    $('#rating-input-rate-3').on('rating.change', function (event, value, caption) {
        $('#DoctorLevel').val(value);
    });
    $('.icon-reversed').on('click', function () {
        $('#PointService').val($(this).data('star'));
        var _mapImg = $(".icon-reversed").map(function () {
            return this;
        }).get();

        for (var i = 0; i < _mapImg.length; i++) {
            $(_mapImg[i]).children().attr('src', $(_mapImg[i]).data('img-grey'));
        }
        $(this).children().attr('src', $(this).data('img'));
    });



</script>
