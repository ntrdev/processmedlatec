﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysTemplateModel;
    var item = ViewBag.Data as SysTemplateEntity;

    var path = Server.MapPath("~/Views/Shared");

    string[] arrFiles = null;
    if (System.IO.Directory.Exists(path))
        arrFiles = System.IO.Directory.GetFiles(path);
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%=model.RecordID>0?"Chỉnh sửa mẫu giao diện":"Thêm mới mẫu giao diện" %></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Mẫu giao diện</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pageMauGiaoDien">
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">File hiển thị, tên...</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="form-group form-md-line-input has-warning">
                                <input type="text" class="form-control" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                <label for="form_control_1">Tên mẫu:</label>
                                <span class="help-block">Ký tự tối đa 20</span>
                            </div>
                            <div class="form-group form-md-line-input has-warning">
                                <select class="form-control" name="File">
                                    <%foreach (var file in arrFiles)
                                        {
                                            var fileName = System.IO.Path.GetFileName(file);
                                    %>
                                    <option <%if (item.File == fileName)
                                        {%>selected<%} %>
                                        value="<%=fileName %>"><%=fileName %></option>
                                    <%}%>
                                </select>
                                <label for="form_control_1">File hiển thị</label>
                            </div>

                            <div class="form-group form-md-radios">
                                <label>Thiết bị hiển thị</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio has-warning">
                                        <input type="radio" id="rdo_decktop" name="Device" <%= item.Device == 0 ? "checked": "" %> class="md-radiobtn" value="0">
                                        <label for="rdo_decktop">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>PC / Laptop
                                       
                                        </label>
                                    </div>
                                    <div class="md-radio has-warning">
                                        <input type="radio" id="rdo_mobile" name="Device" <%= item.Device == 1 ? "checked": "" %> value="1" class="md-radiobtn">
                                        <label for="rdo_mobile">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Mobile
                                       
                                        </label>
                                    </div>
                                    <div class="md-radio has-warning">
                                        <input type="radio" id="rdo_tablet" name="Device" <%= item.Device == 2 ? "checked": "" %> value="2" class="md-radiobtn">
                                        <label for="rdo_tablet">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Tablet
                                       
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input ">
                                <span class="btn default btn-block uppercase btnMaThietKe">Mã thiết kế</span>
                                <div class="codeInterface">
                                    <textarea class="form-control custom" name="Custom" id="Custom" rows="15" cols=""><%=item.Custom %></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thiết kế giao diện</span>
                            <span class="caption-helper">đặt vị trí, cách hiển thị...</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row blockGridPage" id="">

                            <ul class="sortMaugiaodien" id="content_design">
                                <div class="lds-ring">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </ul>
                            <iframe id="frame_design" name="frame_design" src="/{CPPath}/Design/EditTemplate.aspx?id=<%= model.RecordID %>" onload="get_content_design()" style="height: 0; width: 0; border-width: 0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        window.document.title = '<%=model.RecordID>0? "Xem, cấu hình mẫu giao diện " +item.Name +" | trình quản trị IMEXsoft.net ":"Thêm mới mẫu giao diện | trình quản trị IMEXsoft.net"%>';

        $(".btnMaThietKe").click(function () {
            $(".codeInterface").toggle();
        });

        var ListOnLoad = new Array();
        var has_update = false;

        function get_content_design() {
            var ReddevilContainer = window.frames["frame_design"].document.getElementById("rdv_container");
            if (ReddevilContainer)
                document.getElementById("content_design").innerHTML = ReddevilContainer.innerHTML;
            else
                document.getElementById("content_design").innerHTML = window.frames["frame_design"].document.childNodes[0].innerHTML;

            if (has_update)
                custom_update();

            for (var i = 0; i < ListOnLoad.length; i++) {
                layout_change(ListOnLoad[i].pid, ListOnLoad[i].list_param, ListOnLoad[i].layout)
            }

            $('[data-toggle="tooltip"]').tooltip();
            $('.chosen-layout').chosen({

                display_selected_options: true,
                placeholder_text_multiple: 'Gõ từ khóa để tìm kiếm',
                no_results_text: 'Không có kết quả',
                enable_split_word_search: true,
                search_contains: true,
                display_disabled_options: true,
                single_backstroke_delete: false,
                inherit_select_classes: true
            });
        }

        function custom_update() {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "";
            $.ajax({
                url: "/{CPPath}/Api/TemplateGetCustom/<%= item.ID %>.aspx?rnd=" + ranNum,
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    if (content !== "") $("#Custom").html(content);
                },
                error: function (status) { }
            });
        }

        function cp_update(value) {

            has_update = true;

            var input = document.createElement("input");
            input.type = "hidden";
            input.name = "rdv_submit";
            input.value = value;

            var cpForm = window.frames["frame_design"].document.forms[0];

            var arrTagInput = window.frames["frame_design"].document.getElementsByTagName("input");
            var i;
            for (i = 0; i < arrTagInput.length; i++) {
                if (document.getElementById(arrTagInput[i].id))
                    arrTagInput[i].value = document.getElementById(arrTagInput[i].id).value;
            }

            var arrTagSelect = window.frames["frame_design"].document.getElementsByTagName("select");
            for (i = 0; i < arrTagSelect.length; i++) {
                if (document.getElementById(arrTagSelect[i].id))
                    arrTagSelect[i].value = document.getElementById(arrTagSelect[i].id).value;
            }

            cpForm.appendChild(input);
            cpForm.submit();

        }

        function dragStart(ev) {
            ev.dataTransfer.effectAllowed = "move";
            ev.dataTransfer.setData("Text", ev.target.getAttribute("id"));
            ev.dataTransfer.setDragImage(ev.target, 0, 0);
            return true;
        }
        function dragEnter(ev) {
            ev.preventDefault();
            return true;
        }
        function dragOver() {
            return false;
        }
        function dragDrop(ev) {
            var src = ev.dataTransfer.getData("Text");

            var id = ev.target.id;

            if (id !== "to_rdvid_" + src)
                cp_update(id + "$" + src + "|move");

            //ev.target.appendChild(document.getElementById(src));

            ev.stopPropagation();
            return false;
        }
        function do_display(idTbl) {
            var o = document.getElementById(idTbl);
            if (o.style.display === "")
                o.style.display = "none";
            else
                o.style.display = "";
        }
    </script>
</form>
