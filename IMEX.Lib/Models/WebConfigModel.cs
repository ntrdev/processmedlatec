﻿using IMT.Core.Models;

namespace IMT.Lib.Models
{
    public class WebConfigEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public int Value { get; set; }

        #endregion Autogen by RDV
    }

    public class WebConfigService : ServiceBase<WebConfigEntity>
    {
        #region Autogen by RDV

        public WebConfigService()
            : base("[Web_Config]")
        {
        }

        private static WebConfigService _instance;

        public static WebConfigService Instance => _instance ?? (_instance = new WebConfigService());

        #endregion Autogen by RDV

        public WebConfigEntity GetByID(int id)
        {
            return CreateQuery()
                        .Where(o => o.ID == id)
                        .ToSingle_Cache();
        }
    }
}