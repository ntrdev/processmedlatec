﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>



<div class=" content">

    <form method="post" class="login-form" id="loginForm" name="loginForm">
        <div class="auth-box card">
            <div class="card-block">
                <div class="row m-b-20">
                    <div class="col-md-12">
                        <div class="text-center">
                            <img src="/{CPPath}/interface/images/logo.png" style="width:50%" alt="Medo" />
                        </div>
                    </div>
                </div>
                <p class="text-muted text-center p-b-5">{RS:Login_LoginTitle}</p>
                <%= ShowMessage()%>
                <div class="form-group">
                    <i class="fa fa-user"></i>
                    <input type="text" name="UserName" class="form-control" required="" autocomplete="off" placeholder="{RS:Login_UserName}">
                </div>
                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" autocomplete="off" name="Password" class="form-control" required="required" placeholder=" {RS:Login_Password}">
                </div>
                <div class="row m-t-30">
                    <div class="col-md-12">
                        <input type="hidden" id="_imt_action" name="_imt_action" value="Login" />
                        <button type="button" class="btn btn-info btn-md btn-block waves-effect text-center m-b-20" style="background: #fae89f; color: #0066b3; font-weight: 700; border: 1px solid #fae89f;"
                            onclick="loginForm.submit();">
                            {RS:Login_LoginSubmit}</button>
                    </div>
                </div>
                <div class="copyright">
                    Copyright © <%=DateTime.Now.Year %> <a href="MEDO.vn" style="color: aliceblue" target="_blank">MEDO</a> Inc. All rights reserved.
                </div>
            </div>
        </div>

    </form>

</div>
<script type="text/javascript">
    window.document.title = 'Đăng nhập quản trị MEDO | Power by IMEXsoft®';
</script>
