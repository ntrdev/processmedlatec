﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModFeedbackModel;
    var item = ViewBag.Data as ModFeedbackEntity;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Xem phản hồi" : "Thêm mới phả hồi"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phải hồi</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetTinyAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-12">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin bài viết</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tiêu đề:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Phone" name="Phone" value="<%=item.Phone %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Số điện thoại:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Email" name="Email" value="<%=item.Email %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Email:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Address" name="Address" value="<%=item.Address %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Địa chỉ:</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" id="Website" name="Website" value="<%=item.Website %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Website:</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" id="Company" name="Company" value="<%=item.Company %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Tên công ty:</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" name="Content" placeholder="Nội dung yêu cầu"><%=item.Content %></textarea>
                                        <label for="form_control_1">Nội dung phản hồi:</label>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</form>
