﻿using IMEX.Core.Interface;
using IMEX.Core.Models;

namespace IMEX.Lib.Models
{
    public class SysSiteEntity : EntityBase, ISiteInterface
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int PageID { get; set; }

        [DataInfo]
        public int LangID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public bool Default { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public int Order { get; set; }

        #endregion Autogen by RDV
    }

    public class SysSiteService : ServiceBase<SysSiteEntity>, ISiteServiceInterface
    {
        #region Autogen by RDV

        public SysSiteService() : base("[Sys_Site]")
        {
        }

        private static SysSiteService _instance;

        public static SysSiteService Instance => _instance ?? (_instance = new SysSiteService());

        #endregion Autogen by RDV

        public SysSiteEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        #region ISiteServiceInterface Members

        public ISiteInterface CoreMr_Reddevil_GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public ISiteInterface CoreMr_Reddevil_GetByCode(string code)
        {
            return CreateQuery()
               .Where(o => o.Code == code)
               .ToSingle_Cache();
        }

        public ISiteInterface CoreMr_Reddevil_GetDefault()
        {
            return CreateQuery()
               .Where(o => o.Default == true)
               .ToSingle_Cache();
        }

        #endregion ISiteServiceInterface Members
    }
}