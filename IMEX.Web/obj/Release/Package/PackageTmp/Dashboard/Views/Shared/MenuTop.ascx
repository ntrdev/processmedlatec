﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<% 
    //var listModule = IMEX.Lib.Web.Application.CPModules.Where(o => o.ShowInMenu == true).OrderBy(o => o.Order).ToList();
    //var listModule2 = IMEX.Lib.Web.Application.CPModules.Where(o => o.ShowInMenu == false).OrderBy(o => o.Order).ToList();
    var listManager = IMEX.Lib.Web.Application.CPModules.Where(o => o.Partitioning == 1).OrderBy(o => o.Order).ToList();
    var listAdv = IMEX.Lib.Web.Application.CPModules.Where(o => o.Partitioning == 2).OrderBy(o => o.Order).ToList();

%>

<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">

    <ul class="nav navbar-nav">
        <li class="dropdown active <%=CPViewPage.CurrentModule.Code=="Home"?"open":""%> selected "><a href="/{CPPath}/"><i class="icon-home"></i>Home</a></li>

        <li class="dropdown dropdown-fw dropdown-fw-disabled ">
            <a href="javascript:;" class="">
                <i class="fa fa-cubes"></i>{RS:MenuTop_Management}</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <%for (int i = 0; i < listManager.Count; i++)
                    {

                %>
                <li class="dropdown more-dropdown-sub <%=CPViewPage.CurrentModule.Code==listManager[i].Code?"active select-parent":"" %>">
                    <a href="/{CPPath}/<%=listManager[i].Code%>/Index.aspx"><i class="fa fa-<%=listManager[i].CssClass %>"></i>&nbsp;<%=listManager[i].Name %></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/{CPPath}/<%=listManager[i].Code%>/Index.aspx"><i class="fa fa-server"></i>&nbsp;Danh sách <%=listManager[i].Name %> </a>
                        </li>
                        <%if (listManager[i].ShowInMenu)
                            { %>
                        <li>
                            <a href="/{CPPath}/<%=listManager[i].Code%>/Add.aspx"><i class="fa fa-plus-circle"></i>&nbsp;Thêm <%=listManager[i].Name %> mới </a>
                        </li>
                        <%} %>
                    </ul>
                </li>
                <%} %>
            </ul>
        </li>
     <%--   <li class="dropdown dropdown-fw dropdown-fw-disabled ">
            <a href="javascript:;" class="">
                <i class="fa fa-windows"></i>Bảng điều khiển</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <%for (int i = 0; i < listAdv.Count; i++)
                    {
                %>
                <li class="dropdown more-dropdown-sub <%=CPViewPage.CurrentModule.Code==listAdv[i].Code?"active select-parent":"" %>">
                    <a href="/{CPPath}/<%=listAdv[i].Code%>/Index.aspx"><i class="fa fa-<%=listAdv[i].CssClass %>"></i>&nbsp;<%=listAdv[i].Name %></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/{CPPath}/<%=listAdv[i].Code%>/Index.aspx"><i class="fa fa-server"></i>&nbsp;Danh sách <%=listAdv[i].Name %> </a>
                        </li>
                        <%if (listAdv[i].ShowInMenu)
                            {%>
                        <li>
                            <a href="/{CPPath}/<%=listAdv[i].Code%>/Add.aspx"><i class="fa fa-plus-circle"></i>&nbsp;Thêm <%=listAdv[i].Name %> mới </a>
                        </li>
                        <%} %>
                    </ul>
                </li>
                <%} %>
            </ul>
        </li>--%>
        <li class="dropdown dropdown-fw dropdown-fw-disabled ">
            <a href="javascript:;" class=""><i class="fa fa-windows"></i>Bảng điều khiển </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="<%=CPViewPage.CurrentModule.Code=="SysPage"?"active select-parent":"" %>">
                    <a href="/{CPPath}/SysPage/Index.aspx">
                        <i class="fa fa-th-list"></i>{RS:MenuTop_Page}</a>
                </li>
                <li class="<%=CPViewPage.CurrentModule.Code=="SysTemplate"?"active select-parent":"" %>">
                    <a href="/{CPPath}/SysTemplate/Index.aspx">
                        <i class="fa fa-th"></i>{RS:MenuTop_Template}</a></li>
                <li class="<%=CPViewPage.CurrentModule.Code=="SysSite"?"active select-parent":"" %>">
                    <a href="/{CPPath}/SysSite/Index.aspx">
                        <i class="fa fa-sitemap"></i>Site</a>
                </li>
            </ul>
        </li>

        <li class="dropdown dropdown-fw dropdown-fw-disabled ">
            <a href="javascript:;" class="">
                <i class="fa fa-codepen"></i>{RS:MenuTop_System}  </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="SysMenu"?"active select-parent":"" %>">
                    <a href="/{CPPath}/SysMenu/Index.aspx"><i class="fa fa-folder"></i>Danh mục</a>
                </li>



            </ul>
        </li>

        <li class="dropdown more-dropdown ">
            <a href="javascript:;" class="text-uppercase"><i class="fa fa-lightbulb-o"></i>Tiện ích</a>
            <ul class="dropdown-menu">
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="SysUser"?"active":"" %>">
                    <a href="/{CPPath}/SysUser/Index.aspx">
                        <i class="fa fa-user"></i>Tài khoản truy cập </a>
                </li>
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="SysRole"?"active ":"" %>">
                    <a href="/{CPPath}/SysRole/Index.aspx">
                        <i class="fa fa-users"></i>Phân quyền truy cập </a>
                </li>
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="SysUserLog"?"active":"" %>">
                    <a href="/{CPPath}/SysUserLog/Index.aspx"><i class="fa fa-history"></i>Lịch sử truy cập quản trị </a>
                </li>
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="UserChangeInfo"?"active ":"" %>">
                    <a href="/{CPPath}/UserChangeInfo.aspx"><i class="fa fa-user"></i>Thay đổi thông tin </a>
                </li>
                <li class="dropdown <%=CPViewPage.CurrentModule.Code=="UserChangePass"?"active ":"" %>">
                    <a href="/{CPPath}/UserChangePass.aspx"><i class="fa fa-magic"></i>Thay đổi mật khẩu </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
