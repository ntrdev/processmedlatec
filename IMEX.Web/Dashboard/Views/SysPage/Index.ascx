﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<script runat="server">
    private List<EntityBase> AutoGetMap(SysPageModel model)
    {
        List<EntityBase> list = new List<EntityBase>();

        int _id = model.ParentID;
        while (_id > 0)
        {
            var _item = SysPageService.Instance.GetByID(_id);

            if (_item == null)
                break;

            _id = _item.ParentID;

            list.Insert(0, _item);
        }

        return list;
    }
</script>

<%
    var model = ViewBag.Model as SysPageModel;
    var listItem = ViewBag.Data as List<SysPageEntity>;

    var parent = listItem != null ? SysPageService.Instance.GetByID_Cache(listItem[0].ParentID) : null;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />




    <div class="page-content">

        <div class="breadcrumbs">
            <h1>Quản lý trang</h1>
            <ol class="breadcrumb">
                <%= ShowMap(AutoGetMap(model))%>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultListCommand("upload|Thêm nhiều")%>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12"></div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-right pull-right">
                                <div class="table-group-actions d-inline-block" style="margin-top: 10px">
                                    <%= ShowDDLLang(model.LangID)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="dt-responsive table-responsive">
                    <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th>
                                    <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                        <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                        <label for="checks">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </div>
                                </th>
                                <th class="text-center"><%= GetSortLink("Tên trang", "Name")%></th>
                                <%if (model.ParentID > 0)
                                    {%>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Url", "Url")%></th>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Chức năng", "ModuleCode")%></th>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Mẫu giao diện", "TemplateID")%></th>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Giao diện Mobile", "TemplateMobileID")%></th>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Chuyên mục", "MenuID")%></th>
                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Hiển thị", "ShowTop")%></th>
                                <%} %>
                                <th class="text-center">Trạng thái</th>
                                <th style="width: 110px">Sắp xếp vị trí</th>
                                <th class="text-center">Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                {
                                    var moduleInfo = SysModuleService.Instance.CoreMr_Reddevil_GetByCode(listItem[i].ModuleCode) as IMEX.Lib.MVC.ModuleInfo;
                            %>
                            <tr>

                                <td class="text-center"><%=i+1 %></td>
                                <td><%= GetCheckbox(listItem[i].ID, i)%></td>
                                <td class="text-center"><a href="javascript:REDDEVILRedirect('Index', <%= listItem[i].ID %>, 'ParentID')"><i class="fa <%=listItem[i].Faicon %>"></i>&nbsp;<%= listItem[i].Name%></a></td>
                                <%if (model.ParentID > 0)
                                    {%>
                                <td class="text-center hidden-sm hidden-col">
                                    <%= listItem[i].Url %>
                                </td>
                                <td class="text-center hidden-sm hidden-col">
                                    <%= moduleInfo == null ? string.Empty : moduleInfo.Name%>
                                </td>
                                <td class="text-center hidden-sm hidden-col">
                                    <%= GetName(SysTemplateService.Instance.GetByID(listItem[i].TemplateID))%>
                                </td>
                                <td class="text-center hidden-sm hidden-col">
                                    <%= GetName(SysTemplateService.Instance.GetByID(listItem[i].TemplateMobileID))%>
                                </td>
                                <td class="text-center hidden-sm hidden-col">
                                    <%= listItem[i].Menu.Name%>
                                </td>
                                <td class="text-center hidden-sm hidden-col">
                                    <%=GetHideOrShowTop(listItem[i].ID,listItem[i].ShowMenuTop)%>
                                </td>
                                <%} %>
                                <td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Activity)%></td>
                                <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                <td class="text-center">
                                    <div class="control-button">
                                        <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                        <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <%} %>
                        </tbody>
                    </table>
                </div>
                <div class="row center">
                    <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                        <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var REDDEVILController = "SysPage";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.ParentID %>", "ParentID",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>

</form>
