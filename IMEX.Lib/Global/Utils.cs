﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using IMEX.Core.Global;
using IMEX.Core.OptimalImage;
using IMEX.Core.Web;
using IMEX.Lib.Global.ListItem;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;
using HttpRequest = IMEX.Core.Web.HttpRequest;

namespace IMEX.Lib.Global
{
    public class Utils : Core.Web.Utils
    {
        //public static string GetTracking(int device)
        //{
        //    string tracking = string.Empty;

        //    string rawUrl = HttpRequest.RawUrl;
        //    var listItem = ModTrackingService.Instance.CreateQuery()
        //                                .Where(o => o.Activity == true)
        //                                .Where(o => (o.State & device) == device)
        //                                .ToList_Cache();

        //    for (int i = 0; listItem != null && i < listItem.Count; i++)
        //    {
        //        if (rawUrl.Contains(listItem[i].Url))
        //            tracking += listItem[i].Content;
        //    }

        //    return tracking;
        //}
        public static string GetResponseJson(string url)
        {
            Uri uri = new Uri(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string output = reader.ReadToEnd();
            response.Close();

            return output;
        }

        public static string ConvertTime(int totalseconds)
        {
            int time = totalseconds, h = 0, m = 0;
            while (time > 60)
            {
                if (time > 3600)
                {
                    h = time / 3600;
                    time = time % 3600;
                }
                else if (time > 60)
                {
                    m = time / 60;
                    time = time % 60;
                }
            }

            return h + ":" + m + ":" + time;
        }

        public static void GooglePing(string sitemap)
        {
            sitemap = "/ping?sitemap=" + sitemap;

            //GOOGLE
            try
            {
                var request = WebRequest.Create("http://www.google.com/webmasters/tools" + sitemap);
                request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new Exception("Ping sitemap to google had error - " + ex.Message);
            }
        }


        public static string GetHtmlForSeo(string content)
        {
            if (string.IsNullOrEmpty(content))
                return string.Empty;


            return HttpUtility.HtmlDecode(content);



        }

        public static string DayOfWeekVn(DateTime dt)
        {
            var arrDayOfWeek = "Chủ nhật,Thứ hai,Thứ ba,Thứ tư,Thứ năm,Thứ sáu,Thứ bảy".Split(',');

            return arrDayOfWeek[(int)dt.DayOfWeek];
        }

        public static string GetNameOfConfig(string configKey, int value)
        {
            var list = List.GetListByConfigkey(configKey);

            var item = list.Find(o => o.Value == value.ToString());

            return item == null ? string.Empty : item.Name;
        }



        #region charater

        public static string GetFirstLetterOfString(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;

            while (s.Contains("  "))
            {
                s = s.Replace("  ", " ");
            }

            var result = string.Empty;

            for (var i = 1; i < s.Length; i++)
                if (s[i - 1] == ' ') result += s[i];

            return result;
        }

        public static string GetFirstChar(string title)
        {
            return !string.IsNullOrEmpty(title) ? title[0].ToString() : string.Empty;
        }

        public static string GetCharWithoutFirst(string title)
        {
            return !string.IsNullOrEmpty(title) ? title.Substring(1, title.Length - 1) : string.Empty;
        }

        #endregion charater

        #region validate

        public static bool IsEmailAddress(string email)
        {
            return Regex.IsMatch(email.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static bool IsLoginName(string s)
        {
            if (s.Length < 6 || s.Length > 12) return false;

            if (!char.IsLetter(s[0])) return false;

            return (!Regex.IsMatch(s, "[^a-z0-9_]", RegexOptions.IgnoreCase));
        }
        public static bool IsPhoneNumber(string s)
        {
            return (!Regex.IsMatch(s, @"^[0-9]{4}.[0-9]{3}.[0-9]{3}$"));
        }


        public static string GetRandString()
        {
            var rand = new System.Random();
            var iRan = rand.Next();
            var sKey = Security.Md5(iRan.ToString());
            return sKey.Substring(0, 2) + iRan.ToString()[0] + sKey.Substring(3, 2);
        }

        #endregion validate

        #region data
        public static string ShowDdlMenuByType(string type, int langId, int selectId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("[Web_Menu]", "ShowDdlMenuByType." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetList(WebMenuService.Instance, langId, type);
                for (var i = 1; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }


        public static string ShowDdlMenuByTypeCity(string type, int langId, int selectId)
        {
            var html = string.Empty;
            var keyCache = Cache.CreateKey("[Web_Menu]", "ShowDdlMenuByTypeCity." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

                if (list == null) return html;

                var parentId = list[0].ID;

                list = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentId)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();

                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";

                }
                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlMenuByTypeCity(string type, int langId, int parentID, int selectId)
        {
            if (parentID < 1) return "";
            var html = string.Empty;
            var keyCache = Cache.CreateKey("[Web_Menu]", "ShowDdlMenuByTypeCity." + type + "." + langId + "." + selectId + ".parent" + parentID);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
               


                var list2 = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentID)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();

                for (var i = 0; list2 != null && i < list2.Count; i++)
                {
                    html += "<option " + (list2[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list2[i].ID + "\">" + list2[i].Name + "</option>";

                }
                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlMenuByType(string type, int langId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("[Web_Menu]", "ShowDdlMenuByType." + type + "." + langId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetList(WebMenuService.Instance, langId, type);
                for (var i = 1; list != null && i < list.Count; i++)
                {
                    html += "<option value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }


        public static string ShowDdlByConfigkey(string configKey, int selectId)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowDdlByConfigkey." + configKey + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">" + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowRadioByConfigkey(string configKey, string name, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowCheckBoxByConfigkey." + configKey + "." + name + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<input name=\"" + name + "\"" + ((flag & Core.Global.Convert.ToInt(list[i].Value)) == Core.Global.Convert.ToInt(list[i].Value) ? "checked=\"checked\"" : string.Empty) + " value=\"" + list[i].Value + "\" type=\"radio\" />" + list[i].Name + " &nbsp;";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }


        public static string ShowCheckBoxByConfigkey(string configKey, string name, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowCheckBoxByConfigkey." + configKey + "." + name + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += @" <div class=""md-checkbox"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để chọn hoặc bỏ chọn"">
                                                 <input type=""checkbox"" id=""" + name + i + @""" name=""" + name + @""" value=""" + list[i].Value + @""" " + ((flag & Core.Global.Convert.ToInt(list[i].Value)) == Core.Global.Convert.ToInt(list[i].Value) ? "checked=\"checked\"" : string.Empty) + @" onclick=""isChecked(this.checked)"" class=""md-check border-checkbox check-select"" />
                                                <label for=""" + name + i + @""">
                                                    <span style=""left:0;""></span>
                                                    <span class=""check"" style=""left:0;""></span>
                                                    <span class=""box"" style=""left:0;""></span>" + list[i].Name + @"
                                                </label>
                                            </div>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowNameByConfigkey(string configKey, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowNameByConfigkey." + configKey + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    if ((flag & Core.Global.Convert.ToInt(list[i].Value)) != Core.Global.Convert.ToInt(list[i].Value)) continue;

                    html += list[i].Name + "<br />";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetNameByConfigkey(string configKey, int value)
        {
            var list = List.GetListByConfigkey(configKey);

            var item = list.Find(o => o.Value == value.ToString());

            return item == null ? string.Empty : item.Name;
        }

        //public static List<WebMenuEntity> GetListMenuByType(string type, int langId)
        //{
        //    var keyCache = Cache.CreateKey("Web_Menu", "GetListMenuByType." + type + "." + langId);
        //    var list = Cache.GetValue(keyCache) as List<WebMenuEntity>;
        //    if (list != null) return list;
        //    else
        //    {
        //        list = WebMenuService.Instance.CreateQuery()
        //                                    .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
        //                                    .OrderByAsc(o => o.Order)
        //                                    .ToList_Cache();

        //        if (list == null) return null;

        //        var parentId = list[0].ID;

        //        list = WebMenuService.Instance.CreateQuery()
        //                        .Where(o => o.ParentID == parentId)
        //                        .OrderByAsc(o => o.Order)
        //                        .ToList_Cache();

        //        Cache.SetValue(keyCache, list);
        //    }

        //    return list;
        //}

        //public static string GetListMenuByType(string type, int langId, int selectId)
        //{
        //    var html = string.Empty;
        //    var keyCache = Cache.CreateKey("Web_Menu", "GetListMenuByType." + type + "." + langId + "." + selectId);
        //    var obj = Cache.GetValue(keyCache);
        //    if (obj != null) html = obj.ToString();
        //    else
        //    {
        //        var list = WebMenuService.Instance.CreateQuery()
        //                                      .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
        //                                      .OrderByAsc(o => o.Order)
        //                                      .ToList_Cache();

        //        if (list == null) return null;

        //        var parentId = list[0].ID;

        //        list = WebMenuService.Instance.CreateQuery()
        //                        .Where(o => o.ParentID == parentId)
        //                        .OrderByAsc(o => o.Order)
        //                        .ToList_Cache();


        //        for (var i = 0; list != null && i < list.Count; i++)
        //        {
        //            html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";

        //        }
        //        Cache.SetValue(keyCache, html);
        //    }

        //    return html;
        //}

        #endregion data

        #region number to word

        private static string Read(string number)
        {
            var result = "";
            switch (number)
            {
                case "0":
                    result = "không";
                    break;

                case "1":
                    result = "một";
                    break;

                case "2":
                    result = "hai";
                    break;

                case "3":
                    result = "ba";
                    break;

                case "4":
                    result = "bốn";
                    break;

                case "5":
                    result = "năm";
                    break;

                case "6":
                    result = "sáu";
                    break;

                case "7":
                    result = "bảy";
                    break;

                case "8":
                    result = "tám";
                    break;

                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }

        private static string Unit(string number)
        {
            var result = "";

            if (number.Equals("1"))
                result = "";
            if (number.Equals("2"))
                result = "nghìn";
            if (number.Equals("3"))
                result = "triệu";
            if (number.Equals("4"))
                result = "tỷ";
            if (number.Equals("5"))
                result = "nghìn tỷ";
            if (number.Equals("6"))
                result = "triệu tỷ";
            if (number.Equals("7"))
                result = "tỷ tỷ";

            return result;
        }

        private static string Split(string possition)
        {
            var result = "";

            if (possition.Equals("000")) return result;

            if (possition.Length != 3) return result;

            var first = possition.Trim().Substring(0, 1).Trim();
            var middle = possition.Trim().Substring(1, 1).Trim();
            var last = possition.Trim().Substring(2, 1).Trim();

            if (first.Equals("0") && middle.Equals("0"))
                result = " không trăm lẻ " + Read(last.Trim()) + " ";

            if (!first.Equals("0") && middle.Equals("0") && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm ";

            if (!first.Equals("0") && middle.Equals("0") && !last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm lẻ " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && Core.Global.Convert.ToInt(middle) > 1 && Core.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && Core.Global.Convert.ToInt(middle) > 1 && last.Equals("0"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi ";

            if (first.Equals("0") && Core.Global.Convert.ToInt(middle) > 1 && last.Equals("5"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi lăm ";

            if (first.Equals("0") && middle.Equals("1") && Core.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = " không trăm mười " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && middle.Equals("1") && last.Equals("0"))
                result = " không trăm mười ";

            if (first.Equals("0") && middle.Equals("1") && last.Equals("5"))
                result = " không trăm mười lăm ";

            if (Core.Global.Convert.ToInt(first) > 0 && Core.Global.Convert.ToInt(middle) > 1 && Core.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi " + Read(last.Trim()).Trim() + " ";

            if (Core.Global.Convert.ToInt(first) > 0 && Core.Global.Convert.ToInt(middle) > 1 && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi ";

            if (Core.Global.Convert.ToInt(first) > 0 && Core.Global.Convert.ToInt(middle) > 1 && last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi lăm ";

            if (Core.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && Core.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm mười " + Read(last.Trim()).Trim() + " ";

            if (Core.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm mười ";

            if (Core.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm mười lăm ";

            return result;
        }

        public static string NumberToWord(string number)
        {
            if (string.IsNullOrEmpty(number)) return "Không đồng.";

            string result = string.Empty, firstPart = string.Empty, lastPart = string.Empty;

            var quotient = Core.Global.Convert.ToInt(number.Length / 3);
            var remainder = number.Length % 3;

            switch (remainder)
            {
                case 0:
                    firstPart = "000";
                    break;

                case 1:
                    firstPart = "00" + number.Trim().Substring(0, 1);
                    break;

                case 2:
                    firstPart = "0" + number.Trim().Substring(0, 2);
                    break;
            }

            if (number.Length > 2)
                lastPart = Core.Global.Convert.ToString(number.Trim().Substring(remainder, number.Length - remainder)).Trim();

            var im = quotient + 1;
            if (remainder > 0)
                result = Split(firstPart).Trim() + " " + Unit(im.ToString().Trim());

            var i = quotient;
            var j = quotient;
            var k = 1;

            while (i > 0)
            {
                var possition = lastPart.Trim().Substring(0, 3).Trim();
                result = result.Trim() + " " + Split(possition.Trim()).Trim();
                quotient = j + 1 - k;

                if (!possition.Equals("000"))
                    result = result.Trim() + " " + Unit(quotient.ToString().Trim()).Trim();

                lastPart = lastPart.Trim().Substring(3, lastPart.Trim().Length - 3);

                i--;
                k++;
            }

            if (result.Trim().Length <= 0) return result.Trim();

            if (result.Trim().Substring(0, 1).Equals("k"))
                result = result.Trim().Substring(10, result.Trim().Length - 10).Trim();

            if (result.Trim().Substring(0, 1).Equals("l"))
                result = result.Trim().Substring(2, result.Trim().Length - 2).Trim();

            result = result.Trim().Substring(0, 1).Trim().ToUpper() +
                     result.Trim().Substring(1, result.Trim().Length - 1).Trim() + " đồng.";

            return result.Trim();
        }

        public static string NumberToWordV2(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;

                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;

                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;

                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }

                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc + "đồng";
        }

        public static string NumberToWordV3(string input)
        {
            var result = "";

            var number = Core.Global.Convert.ToLong(input);
            if (number < 1) return "0 đồng";

            var billion = number / (long)Math.Pow(10, 9);
            var million = (number - billion * (long)Math.Pow(10, 9)) / (long)Math.Pow(10, 6);
            var thousand = (number - billion * (long)Math.Pow(10, 9) - million * (long)Math.Pow(10, 6)) / (long)Math.Pow(10, 3);

            if (billion > 0) result += billion + " tỷ ";
            if (million > 0) result += million + " triệu ";
            if (thousand > 0) result += thousand + " nghìn ";

            return result.Trim();
        }

        #endregion number to word

        #region media

        public static string ImageToBase64(byte[] _imagePath)
        {
            if (_imagePath == null) return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";
            string _base64String = null;
            var html = string.Empty;
            var keyCache = Cache.CreateKey("Employee", "GetBase64File." + _imagePath);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();

            else
            {

                using (MemoryStream _mStream = new MemoryStream(_imagePath))
                {
                    System.Drawing.Image _image = System.Drawing.Image.FromStream(_mStream);
                    byte[] _imageBytes = _mStream.ToArray();
                    _base64String = System.Convert.ToBase64String(_imageBytes);

                    html = "data:image/jpg;base64," + _base64String;
                }
            }
            return html;
        }
        public static string DefaultImage(string parth)
        {
            if (string.IsNullOrEmpty(parth)) return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";
            else
            {
                var keyCache = string.Concat("[Mod_Adv]", ".GetCodeAdv." + parth);
                var obj = Cache.GetValue(keyCache);
                if (obj != null) return obj.ToString();
                else
                {
                    Cache.SetValue(keyCache, parth.Replace("~/", "/"));
                    return parth.Replace("~/", "/");

                }
            }
        }

        public static string OptimalPng(string file)
        {

            if (string.IsNullOrEmpty(file)) return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";
            else
            {
                var keyCache = string.Concat("[Mod_Adv]", ".GetImgOptimal." + file);
                var obj = Cache.GetValue(keyCache);
                if (obj != null) return obj.ToString();
                else
                {
                    file = HttpUtility.UrlDecode(file);
                    var filePath = HttpContext.Current.Server.MapPath(file);
                    if (!System.IO.File.Exists(filePath)) return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";

                    var target = Path.GetDirectoryName(file.ToLower().Replace("~/data/upload/images", "").Replace("/data/upload/images", ""));
                    var extension = Path.GetExtension(file);
                    Directory.Create(HttpContext.Current.Server.MapPath("~/Data/Imgm/" + target));
                    target = "~/Data/Imgm/" + target + "/" + File.FormatFileName(Path.GetFileNameWithoutExtension(file)) + "-m30" + extension;
                    var targetPạth = HttpContext.Current.Server.MapPath(target);
                    if (System.IO.File.Exists(targetPạth))
                    {
                        Cache.SetValue(keyCache, HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/"));
                        return HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/");
                    }
                    if (extension == ".png")
                    {

                        try
                        {
                            var quantizer = new OptimalQuantizer();
                            using (var bitmap = new Bitmap(filePath))
                            {
                                using (var quantized = quantizer.OptimalPng(bitmap))
                                {
                                    quantized.Save(targetPạth, ImageFormat.Png);
                                }
                            }
                        }
                        catch
                        {
                            Image.CompressionImageFile(45, filePath, targetPạth);
                        }
                    }
                    else
                    {
                        Image.CompressionImageFile(45, filePath, targetPạth);
                    }
                    Cache.SetValue(keyCache, HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/"));
                    return HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/");

                }

            }
        }


        public static string GetResizeFile(string file, int type, int width, int height)
        {
            if (string.IsNullOrEmpty(file))
                return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";

            if (file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                return file;

            var applicationPath = HttpRequest.ApplicationPath;

            file = HttpUtility.UrlDecode(file);
            var filePath = HttpContext.Current.Server.MapPath(file);

            var target = Path.GetDirectoryName(file.ToLower().Replace("~/data/upload/", "").Replace("/data/upload/", ""));
            Directory.Create(HttpContext.Current.Server.MapPath("~/Data/RImage/" + target));

            var extension = Path.GetExtension(file);
            target = "~/Data/RImage/" + target + "/" + File.FormatFileName(Path.GetFileNameWithoutExtension(file)) + "x" + type + extension;
            var targetPạth = HttpContext.Current.Server.MapPath(target);

            if (System.IO.File.Exists(targetPạth))
                return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");

            if (!System.IO.File.Exists(filePath))
                return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAz1BMVEVHcEz6iyn7YB/6hif6iSj7Yh/7ayH6gyf7aSH5kir6eiX6eST4py/7ZyD6iij4py/5nS38VBz6iij4qTD5kyr5lSv7aSH8VBz8WB38UBv4rzH7ZCD7ciP8WB34rTD6gCb8Vxz8VBz5mCv7cyP4rTD7ZyD6fib6dyT6gSb6eiX6cSP7Yh/6bCL5kir5hCf8Uxz5lSv5hyj4mSz5iin7WB35jSn7Xh74pS/7Wx76dCT4qjD7aiH6biL4nC37YB/3sTH4oi75jyr8Thr4ny35jiqJj8isAAAAJXRSTlMAoHh4AZ8nBBIN2cd22rLUxtvFshnbx3Dwxsey8IHjR19T7jeR+TZfpwAABoVJREFUeNrtm+la6jwUhasGW1AqUgCBg4AeUY/zfByreO7/mr6kgE2nlWrH5/n28o8/Nrre7N0kTTaaRiKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUj/M+0PB5VxNV5sdVwZDPfL5b9ycHBy8rYzjhM72n47OTk4qLAS+R88OwD391117OTvvQPw3CkPweB0CfC3q/a/AOAEemn8uwC3CoLJrQtwWhKCwYsMgAkmtzLASymqaPDHC3ABCCYXXoA/G3oJ/PsBogkmF36A4gnMxyBAFAH3HwB43Ci2iszDMIBZKMFkFgZwWGgOBofhALOQFW00CwcoksA8jgKYtQL7h1kUwHFhVWQeRwMM/cHdaIDjgnJgTgFAL1BtAGBaCIE5RQDb/vAeAjgqoIrMo+m3MlBBANOjpp67fwwwDk6iECBvAvMcA2wHS6KHAc6beVaReYUBdtaDnzG2McB5jjkwrzBAmH9BgAGuciMwbzBAuP85AQK4yamKzDsMEOXfIYAAN7nkwLzDANH+NWZsY4B/ORCYHxgA+HdyAAHuPjKvIvMMA2D/ggADnGWcA/MMA6j8OwQQ4KyRJYF5iQHU/gUBBjhrsAz9Y4A4/jlBHQJcXmaWA+saA/j9h/tgzKhjgOuMCKxrDOD611dM02yuVCNn0zoGuM6kiqxXDOD6r26JwI8RWA/qGOA1gxxYnxjgl+yfB+7h9aCOAdInsD4hgOuf6VsicE+1HtQxwGfKVWS9YwB5/EXgnno9qGOAz1RzYL1jAI9/Hqj2L54DDPCQIoH1gAFk/yJwL+Z6AAHeH1KrIusBA8j+H3hgPP+CAAM89NPJgfWEATz+OUBc/w4BBEiHwHrCAPL8IyK/k3ijhgGe+iwN/xBAHn8n0tpc8akifjzadwkwwFPiHFg2BpD9206kE3jNV+K7mxsexoOODx8f+VQlthtvb/ydh7/bf2WNE0AAOyGBZWMAj38J4NIFmAYAPO/9Rg0D2ImqiPtHANMv//q6bX8HYDYbSQQAQBDoSf1HArjjr41+b0pa21zzaHVt1afKWHqSYQZs2/qp/5Ftowwcuf4Z09lc4vfv5pwToAzY9maCBIAM/JLn+0TTnVGDGbD7P/y7uzADtVZ6ey29CTOwmwQgMgMb6Z7WowzYSUoo+hmoGaklYAM/A/0kDzGYhdIi0DfwLPTTh5gxy8brgEQwqfgnSj51eudSPrm6+r0p+8ez0E8TwJjex+vAsUuwP79/cc4Nxevu6XynzK0JZ3f/uDHH1/tyWPdd/3gltreqP08u6yu2El8EbH594QK8AIAne7cl+ccASfzzP99XbOZcgvWd+ACyf8VmLpl/QYABHt0qWvdumZ1NtG9jvWY5AF/+GfePAbYSTxSsr3ihkZ5k1SaCMUsAyOOveKFJOv6LHOBXynrsQTLawpbkv6N4pUzDvyDAAI9xCVpt8Qx4/GOArZQWGtbApxLxcsCMtvAl+1ccq6Qz/s6/aigOtuIQtNrCl+wfH2y9p+ffIcBHi2oC7p/7kuafjuJoccvQUhRrYIBnFYHRFr7a0vgrDnfTHP95DhTH65ig1eYLmdc/BkjbvyBQXHAggn1nJfb6hwDp1s+yihRXTNEEbKVpmuaK5B9fMb22q1oG0huKS75IAuZdp7l/DJCNf0GAAe7jrQd6R3HN2ja0jMQaiovu7Rj/Wu/hi+7LrMZ/ngMM8KYm0HuKVoMs/YvzD0Wzh4qA+8cA2dXPooqainYbTKD3FO022Y7/PAeKhidEwP1jgOz9OwS45SySgOk9RctZ1vWzrCJF018UAfePAfIY/3kOFG2X4QTCPwI4z8u/Q4AbX8MI9J6i8bVmaLmJNRWtx73gRwa49fiqVtVylN78bvP3SNH8na9/hwACDPwfULTf51k/i5LYSPELENO8x39xqJzaV1CK8D8/lo0EqPijVwFA/vWzrKJogPXAqVA0QDHjvzhajgAIfpWPjaMAivPvEIQCDMOCu+EARdXPsorCAIbhwd0wgCLHf56DIMAwKrgbBCjav0PgAxhGB3f9AMXWz7KKvABDFNz1AhQ//otjZglgiIO7MkC9FP4dgi+AoSq4+wVwWje0koh1lgBDdXB3CVCW8Z9v9h2At26c2PGOAzAokX+x3a/UO8N4DaOstdqpVyYaKxUAa41HcXvbmDYar2slFNNIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCRSmfUfNGc0RraH9IEAAAAASUVORK5CYII=";

            try
            {
                Image.ResizeImageFile(type, filePath, targetPạth, extension, width, height, null);
            }
            catch
            {
                // ignored
            }

            if (System.IO.File.Exists(targetPạth))
                return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");

            return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");
        }

        public static string GetResizeFile(string file, int type, int width, int height, string cssClass, string alt)
        {
            var html = string.Empty;
            var keyCache = Cache.CreateKey("Mod_Adv", "GetResizeFile." + file + "." + type + "." + width + "." + height + "." + cssClass + "." + alt);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                if (!string.IsNullOrEmpty(cssClass))
                    html += " class=\"" + cssClass + "\" ";

                if (type != 4 && type != 5)
                {
                    if (width > 0)
                        html += " width=\"" + width + "\" ";

                    if (height > 0)
                        html += " height=\"" + height + "\" ";
                }

                if (!string.IsNullOrEmpty(alt))
                    html += " alt=\"" + alt + "\" ";

                html = @"<img src=""" + GetResizeFile(file, type, width, height) + @""" " + html + @" />";

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetResizeFile(string file, int type, int width, int height, string alt)
        {
            return GetResizeFile(file, type, width, height, string.Empty, alt);
        }

        public static string GetCropFile(string file, int width, int height)
        {
            return GetResizeFile(file, 5, width, height);
        }

        public static string GetMedia(int typeResize, string file, int width, int height, string cssClass, string alt, string addInTag, bool compression)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("Mod_Adv", "GetMedia." + typeResize + "." + file + "." + width + "." + height + "." + cssClass + "." + alt + "." + addInTag + "." + compression);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                if (string.IsNullOrEmpty(file))
                    return string.Empty;

                var extension = Path.GetExtension(file).ToLower();
                if (extension == ".swf")
                {
                    file = file.Replace("~/", "/");

                    if (!file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                        file = HttpRequest.ApplicationPath + HttpContext.Current.Server.UrlPathEncode(file);

                    html = @"   <object width=""" + (width > 0 ? width.ToString() : "100%") + @""" height=""" + (height > 0 ? height.ToString() : "100%") + @" border=""0"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"" classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"">
                                    <param value=""" + file + @""" name=""movie"">
                                    <param value=""always"" name=""AllowScriptAccess"">
                                    <param value=""high"" name=""quality"">
                                    <param name=""scale"" value=""exactfit"">
                                    <param value=""transparent"" name=""wmode"">
                                    <embed width=""" + (width > 0 ? width.ToString() : "100%") + @""" height=""" + (height > 0 ? height.ToString() : "100%") + @" type=""application/x-shockwave-flash"" scale=""exactfit"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" allowscriptaccess=""always"" wmode=""transparent"" quality=""high"" src=""" + file + @""">
                                </object>";
                }
                else
                {
                    if (!string.IsNullOrEmpty(cssClass))
                        html += " class=\"" + cssClass + "\" ";

                    if (!string.IsNullOrEmpty(alt))
                        html += " alt=\"" + alt + "\" ";

                    if (!compression)
                    {
                        if (!file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                            file = HttpRequest.ApplicationPath + HttpContext.Current.Server.UrlPathEncode(file);

                        if (width > 0)
                            html += " width=\"" + width + "\" ";

                        if (height > 0)
                            html += " height=\"" + height + "\" ";
                    }
                    else
                    {
                        file = GetResizeFile(file, typeResize, width, height);

                        if (typeResize != 4)
                        {
                            if (width > 0)
                                html += " width=\"" + width + "\" ";

                            if (height > 0)
                                html += " height=\"" + height + "\" ";
                        }
                    }

                    if (!file.StartsWith("/") && !file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                        file = "/" + file;

                    html = @"<img src=""" + file + @""" " + html + addInTag + @" />";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetMedia(string file, int width, int height, string cssClass, string alt, string addInTag, bool compression)
        {
            return GetMedia(2, file, width, height, cssClass, alt, addInTag, compression);
        }

        public static string GetMedia(string file, int width, int height, string cssClass, string alt, string addInTag)
        {
            return GetMedia(2, file, width, height, cssClass, alt, addInTag, true);
        }

        public static string GetMedia(string file, int width, int height, string addInTag)
        {
            return GetMedia(2, file, width, height, string.Empty, string.Empty, addInTag, true);
        }

        public static string GetMedia(string file, int width, int height)
        {
            return GetMedia(2, file, width, height, string.Empty, string.Empty, string.Empty, true);
        }

        public static string GetMedia(string file, int width)
        {
            return GetMedia(2, file, width, 0, string.Empty, string.Empty, string.Empty, true);
        }

        public static string GetMedia(string file)
        {
            return GetMedia(2, file, 0, 0, string.Empty, string.Empty, string.Empty, true);
        }

        #endregion media

        #region fomart 
        public static string FormatPhoneNumber(string phone)
        {
            if (string.IsNullOrEmpty(phone) || phone.Length != 10) return phone;
            else return Regex.Replace(phone, @"(\d{4})(\d{3})(\d{3})", "$1.$2.$3");
        }
        //hàm covert kiểu decimal thành kiểu float 123,456,789.00
        public static string FormatFloat(decimal money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu int thành kiểu float 123,456,789.00
        public static string FormatFloat(int money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu double thành kiểu float 123,456,789.00
        public static string FormatFloat(double money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu decimal thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(decimal money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu int thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(int money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu double thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(double money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu double thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(long money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm cồng chuỗi 
        public static string GetText(string[] arrString)
        {
            string @string = "";

            for (int i = 0; arrString != null && i < arrString.Length; i++)
                if (!string.IsNullOrEmpty(arrString[i])) @string += (!string.IsNullOrEmpty(@string) ? "," : "") + arrString[i];

            return @string;
        }
        //hàm lấy dữ liệu theo tag xml add vào mảng dữ liệu trả vê
        //public static DataTable Convert_xml_string_to_array(string file_xml, string tag)
        //{
        //    DataTable list = new DataTable();
        //    XmlDataDocument xmldoc = new XmlDataDocument();
        //    XmlNodeList xmlnode;
        //    xmldoc.Load(file_xml);
        //    xmlnode = xmldoc.GetElementsByTagName(tag);
        //    bool flag = false;
        //    for (int i = 0; i <= xmlnode.Count - 1; i++)
        //    {
        //        DataRow dr = list.NewRow();
        //        foreach (System.Xml.XmlNode child in xmlnode[i].ChildNodes)
        //        {
        //            flag = false;
        //            if (list.Columns.Count == 0)
        //            {
        //                flag = true;
        //            }
        //            else
        //            {
        //                for (int k = 0; k < list.Columns.Count; k++)
        //                {
        //                    if (list.Columns[k].ColumnName.ToLower() == child.Name.ToLower())
        //                    {
        //                        flag = false;
        //                        break;
        //                    }
        //                    if (k == list.Columns.Count - 1)
        //                    {
        //                        flag = true;
        //                    }
        //                }
        //            }

        //            if (flag)
        //            {
        //                list.Columns.Add(child.Name);
        //            }
        //            dr[child.Name] = child.InnerText.Trim();
        //        }
        //        list.Rows.Add(dr);
        //    }

        //    return list;
        //}
        //public static string get_value_tag_xml(string file_xml, string tag)
        //{
        //    XmlDataDocument xmldoc = new XmlDataDocument();
        //    XmlNodeList xmlnode;
        //    string value = "";
        //    try
        //    {
        //        xmldoc.Load(file_xml);
        //        xmlnode = xmldoc.GetElementsByTagName(tag);

        //        for (int i = 0; i <= xmlnode.Count - 1; i++)
        //        {
        //            value = xmlnode[i].InnerText;
        //            break;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Global.Error.Write(e.Message);
        //    }
        //    return value;
        //}
        //hàm covert kiểu DateTime thành kiểu hiển thị ngày dd-MM-yyyy
        public static string FormatDate(DateTime datetime)
        {
            CultureInfo us = new CultureInfo("vi-VN");
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format(us,"{0:dddd, dd/MM/yyyy}", datetime);
        }
        public static string FormatDate2(DateTime datetime)
        {
           
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:dd/MM/yyyy}", datetime);
        }


        public static string FomartTime(DateTime datetime)
        {
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:hh:mm}", datetime);
        }


        //hàm covert kiểu DateTime thành kiểu hiển thị ngày dd-MM-yyyy
        public static string FormatDateTime(DateTime datetime)
        {
            CultureInfo us = new CultureInfo("vi-VN");
            if (datetime == DateTime.MinValue)
            {
                return "";
            }
            return string.Format(us,"{0:HH:mm - dddd, dd/MM/yyyy}", datetime);
        }
        public static string FormatDateTime2(DateTime datetime)
        {
            
            if (datetime == DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:HH:mm - dd/MM/yyyy}", datetime);
        }

        #endregion
    }
}