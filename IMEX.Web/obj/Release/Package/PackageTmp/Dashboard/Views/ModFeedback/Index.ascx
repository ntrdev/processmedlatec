﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModFeedbackModel;
    var listItem = ViewBag.Data as List<ModFeedbackEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý sản phẩm</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phản hồi</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetTinyConfigCommand() %>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Danh sách phản hồi</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">STT</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting"><%= GetSortLink("Họ và tên", "Name")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Điện thoại", "Phone")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Nội dung", "Content")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("IP", "IP")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày gửi", "Created")%></th>
                                        <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td>
                                            <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].Phone%></td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].Content%></td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].IP%></td>
                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].Created) %></td>
                                        <td class="text-center">
                                            <div class="control-button">
                                                <button data-toggle="tooltip" data-original-title="Xem & Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <script type="text/javascript">

        var REDDEVILController = "ModFeedback";

        var REDDEVILArrVar = [
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

</form>
