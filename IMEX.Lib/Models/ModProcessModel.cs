﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModProcessEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int ProcessID { get; set; }

        [DataInfo]
        public int MaLichHen { get; set; }//mã lịch hẹn

        [DataInfo]
        public int ScheduleID { get; set; }

        [DataInfo]
        public string SID { get; set; }

        [DataInfo]
        public string HoVaTen { get; set; }

        [DataInfo]
        public string DiaChi { get; set; }

        [DataInfo]
        public DateTime NgayDatLich { get; set; }

        [DataInfo]
        public string KhungGio { get; set; }

        [DataInfo]
        public DateTime TGXacNhan { get; set; }

        [DataInfo]
        public DateTime TGVaoSo { get; set; }

        [DataInfo]
        public string UserNhanLich { get; set; }

        [DataInfo]
        public bool DaNhanLich { get; set; }

        [DataInfo]
        public bool FullResult { get; set; }

        [DataInfo]
        public DateTime FullResultTime { get; set; }

        [DataInfo]
        public DateTime TGNhanLich { get; set; }

        [DataInfo]
        public DateTime TGDenDiaChi { get; set; }

        [DataInfo]
        public DateTime TGTraKQ { get; set; }

        [DataInfo]
        public bool TraKQ { get; set; }

        [DataInfo]
        public DateTime TGHenTaiKham { get; set; }

        [DataInfo]
        public DateTime TGNhanMau { get; set; }

        [DataInfo]
        public DateTime ValidTime { get; set; }

        [DataInfo]
        public bool TuVan { get; set; }

        [DataInfo]
        public string UserTuVan { get; set; }

        [DataInfo]
        public bool ThucHienDichVu { get; set; }

        [DataInfo]
        public double DiemDanhGia { get; set; }

        [DataInfo]
        public string NoiDungDanhGia { get; set; }

        #endregion Autogen by RDV


        public string token { get; set; }
    }

    public class ModProcessService : ServiceBase<ModProcessEntity>
    {
        #region Autogen by RDV

        private ModProcessService() : base("[Process]", "ProcessID")
        {
            DBConfigKey = "DBConnectMEDO";

        }

        private static ModProcessService _instance;
        public static ModProcessService Instance => _instance ?? (_instance = new ModProcessService());

        #endregion Autogen by RDV


    }
}