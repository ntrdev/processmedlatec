﻿using IMEX.Lib.Global;
using IMEX.Lib.MVC;

namespace IMEX.Lib.CPControllers
{
    public class HomeController : CPController
    {
        public void ActionIndex()
        {
        }

        public void ActionLogout()
        {
            CPViewPage.SetLog("Thoát khỏi hệ thống.");

            CPLogin.Logout();

            CPViewPage.CPRedirect("Login.aspx");
        }
    }
}