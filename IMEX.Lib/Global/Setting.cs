﻿using IMEX.Core.Global;

namespace IMEX.Lib.Global
{
    public class Setting : Core.Web.Setting
    {

        //facebook
        public static string FacebookClientId = Config.GetValue("Facebook.ClientID").ToString();

        public static string FacebookClientSecret = Config.GetValue("Facebook.ClientSecret").ToString();
        public static string FacebookRedirectUri = Core.Web.HttpRequest.Domain + "/login-fb/Facebook";  //Config.GetValue("Facebook.RedirectUri").ToString();

        //google
        public static string GoogleClientId = Config.GetValue("Google.ClientID").ToString();

        public static string GoogleClientSecret = Config.GetValue("Google.ClientSecret").ToString();
        public static string GoogleRedirectUri = Core.Web.HttpRequest.Domain + "/login-gg/Google";//Config.GetValue("Google.RedirectUri").ToString();

        public static string GoogleAPI = Core.Web.HttpRequest.IsLocal ? Config.GetValue("Google.LocalAPI").ToString() : Config.GetValue("Google.ServerAPI").ToString();

        public static string FireBaseAppID = Config.GetValue("Google.FireBaseAppID").ToString();
        public static string FireBaseSenderID = Config.GetValue("Google.FireBaseSenderID").ToString();

        public static string GoogleReCaptchaSecret = Config.GetValue("Google.GoogleReCaptchaSecret").ToString();
        public static string GoogleReCaptchaSiteKey = Config.GetValue("Google.GoogleReCaptchaSiteKey").ToString();

        public static string Zalo_ClientID = Core.Global.Config.GetValue("Zalo.ClientID").ToString();
        public static string Zalo_ClientSecret = Core.Global.Config.GetValue("Zalo.ClientSecret").ToString();
        public static string Zalo_RedirectUri = Core.Global.Config.GetValue("Zalo.RedirectUri").ToString();
    }
}