﻿using System;
using IMEX.Lib.Global;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;

namespace IMEX.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Static", Code = "CStatic", IsControl = true, Order = 4)]
    public class CStaticController : Controller
    {
        

        [Core.MVC.PropertyInfo("Văn bản / Html")]
        public string ServiceText;
        public override void OnLoad()
        {
            ViewBag.Content =!string.IsNullOrEmpty(ServiceText) ? Data.Base64Decode(ServiceText):"";
        }

        
      
        public void ActionChangeState()
        {
            if (ViewPage.MobileDevice)
                Global.Cookies.SetValue("CustomDevice", "desktop", true);
            else
                Global.Cookies.SetValue("CustomDevice", "mobile", true);

            ViewPage.RefreshPage();
        }
    }
    public class CStaticModel
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public string ValidCode { get; set; }
        public string Password2 { get; set; }
        public string LoginName { get; set; }
        public int uid { get; set; }


        public string returnpath { get; set; }
    }
}