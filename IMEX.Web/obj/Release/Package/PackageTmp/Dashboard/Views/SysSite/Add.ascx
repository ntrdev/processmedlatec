﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var listLang = SysLangService.Instance.CreateQuery().ToList();
    var model = ViewBag.Model as SysSiteModel;
    var item = ViewBag.Data as SysSiteEntity;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật ngôn ngữ" : "Thêm mới ngôn ngữ"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Ngôn ngữ</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin ngôn ngữ</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên:</label>
                                        <span class="help-block">Tên của ngôn ngữ</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Code" name="Code" value="<%=item.Code %>" placeholder="nhập vào đây">
                                        <label for="form_control_1">Mã ngôn ngữ:</label>
                                        <span class="help-block">Ngôn ngữ Việt Nam có mã: vn</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input " id="list_page">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" onchange="lang_change(this.value)" name="LangID" id="LangID">
                                            <%for (var i = 0; listLang != null && i < listLang.Count; i++)
                                                { %>
                                            <option <%if (item.LangID == listLang[i].ID)
                                                {%>selected<%} %>
                                                value="<%= listLang[i].ID%>">&nbsp; <%= listLang[i].Name%></option>
                                            <%} %>
                                        </select>
                                        <label for="form_control_1">Chọn ngôn ngữ:</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" name="Summary" placeholder="Mã thiết kế"><%=item.Custom %></textarea>
                                        <label for="form_control_1">Mã thiết kế:</label>
                                        <span class="help-block">Có thể hệ thống sẽ tự sinh</span>
                                    </div>
                                </div>
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh Quốc kỳ</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse show" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh Quốc kỳ</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        var ddl_lang = document.getElementById("LangID");

        function lang_change(langID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "LangID=" + langID + "&PageID=<%=item.PageID %>&rnd=" + ranNum;
            $.ajax({
                url: "/{CPPath}/Api/SiteGetPage.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Params;

                    content = "<select class=\"form-control chosen\" name=\"PageID\" id=\"PageID\"><option value=\"0\">Chọn trang</option>" + content + "</select><label for=\"form_control_1\">Trang mặc định:</label><span class=\"help-block\"> Cài đặt trang mặc định cho toàn bộ website</span>";
                    $("#list_page").html(content);
                    $('.chosen').chosen({
                        search_contains: true,
                        no_results_text: 'Không tìm thấy kết quả phù hợp'
                    });
                },
                error: function (status) { }
            });
        }

        if (<%=item.LangID%> > 0) lang_change(<%=item.LangID %>);
        else if ($("#LangID").val() > 0) lang_change($("#LangID").val());
    </script>
</form>
