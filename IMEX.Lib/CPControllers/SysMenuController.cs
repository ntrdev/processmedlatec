﻿using System;
using System.Collections.Generic;
using IMEX.Lib.Global;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;

namespace IMEX.Lib.CPControllers
{
    [CPModuleInfo(Name = "Danh Mục",
      Description = "Quản lý - Danh Mục",
      Code = "SysMenu",
      Access = 31,
      Order = 1,
      ShowInMenu = false,
      CssClass = "icon-16-article", Partitioning = 0)]
    public class SysMenuController : CPController
    {
        public SysMenuController()
        {
            //khoi tao Service
            DataService = WebMenuService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(SysMenuModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort, "[Order]");

            //tao danh sach
            var dbQuery = WebMenuService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                    .Where(o => o.ParentID == model.ParentID && o.LangID == model.LangID)
                                    .Take(model.PageSize)
                                    .Skip(model.PageIndex * model.PageSize)
                                    .OrderBy(orderBy);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(SysMenuModel model)
        {
            if (model.RecordID > 0)
            {
                _item = WebMenuService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new WebMenuEntity
                {
                    LangID = model.LangID,
                    ParentID = model.ParentID,
                    Type = model.ParentID > 0 ? WebMenuService.Instance.GetByID(model.ParentID).Type : "News",
                    Order = GetMaxOrder(model),
                    Activity = true

                };
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(SysMenuModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(SysMenuModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(SysMenuModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }
            var list = new List<int>();
            GetMenuIDChildForDelete(ref list, arrID);

            if (list != null && list.Count > 0)
            {
                //var sWhere = "[MenuID] IN (" + Core.Global.Array.ToString(list.ToArray()) + ")";


                ////xoa news
                //if (ModNewsService.Instance.Exists(sWhere))
                //{
                //    CPViewPage.Alert("Danh mục bạn xóa còn chứa Bài viết. Hãy xóa hết Bài viết trong chuyên mục muốn xóa trước.");
                //    return;
                //}

                //xoa menu
                string sWhere = "[MenuID] IN (" + Core.Global.Array.ToString(list.ToArray()) + ")";

                WebMenuService.Instance.DeleteAsync(sWhere);
            }

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        public void ActionUpload(SysMenuModel model)
        {
            CPViewPage.Script("Redirect", "REDDEVILRedirect('Import')");
        }

        public void ActionImport(SysMenuModel model)
        {
            ViewBag.Model = model;
        }

        public override void ActionCancel()
        {
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx")
                .Replace("Import.aspx", "Index.aspx"));
        }

        #region private func

        private WebMenuEntity _item;

        private bool ValidSave(SysMenuModel model)
        {
            if (!string.IsNullOrEmpty(model.Value))
            {
                var parent = WebMenuService.Instance.GetByID(model.ParentID);
                if (parent == null) return false;

                var ArrItem = model.Value.Split('\n');

                foreach (var t in ArrItem)
                {
                    if (string.IsNullOrEmpty(t.Trim()) || t.StartsWith("//"))
                        continue;

                    _item = new WebMenuEntity { Name = t.Trim(), Url = Data.GetCode(t.Trim()) };

                    var exists = WebMenuService.Instance.CreateQuery()
                                        .Where(o => o.Url == _item.Url && o.Type == parent.Type && o.LangID == parent.LangID)
                                        .Count()
                                        .ToValue()
                                        .ToBool();

                    if (exists) continue;

                    _item.Type = parent.Type;

                    _item.ParentID = model.ParentID;
                    _item.LangID = parent.LangID;
                    if (!exists) WebMenuService.Instance.Save(_item);
                    _item.Levels = _item.Parent.Levels + "-" + _item.ID;

                    _item.Order = GetMaxOrder(model);
                    _item.Activity = true;
                    

                    WebMenuService.Instance.Save(_item);
                }

                return true;
            }
            else
            {
                TryUpdateModel(_item);

                _item.ID = model.RecordID;

                ViewBag.Data = _item;
                ViewBag.Model = model;

                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

                //kiem tra ten
                if (_item.Name.Trim() == string.Empty)
                    CPViewPage.Message.ListMessage.Add("Nhập tên chuyên mục.");

                if (CPViewPage.Message.ListMessage.Count != 0) return false;

                //neu code khong duoc nhap -> tu dong tao ra khi them moi
                if (_item.Url == string.Empty)
                    _item.Url = Data.GetCode(_item.Name);


                try
                {
                    //neu di chuyen thi cap nhat lai Type va Order
                    if (model.RecordID > 0 && _item.ParentID != model.ParentID)
                    {
                        //cap nhat Type
                        if (_item.ParentID != 0)
                            _item.Type = WebMenuService.Instance.GetByID(_item.ParentID).Type;

                        //cap nhat Order
                        _item.Order = GetMaxOrder(model);

                    }
                    if (_item.ParentID > 0 && _item.ID > 0)
                        _item.Levels = !string.IsNullOrEmpty(_item.Parent.Levels) ? _item.Parent.Levels + "-" + _item.ID : _item.ID.ToString();
                    else if (_item.ID < 1)
                    {
                        WebMenuService.Instance.Save(_item);
                        _item.Levels = !string.IsNullOrEmpty(_item.Parent.Levels) ? _item.Parent.Levels + "-" + _item.ID : _item.ID.ToString();
                    }
                    //save
                    WebMenuService.Instance.Save(_item);

                    //neu di chuyen thi cap nhat lai Type cua chuyen muc con
                    if (model.RecordID > 0 && _item.ParentID != model.ParentID && _item.ParentID != 0)
                    {
                        //lay danh sach chuyen muc con
                        var list = new List<int>();
                        GetMenuIDChild(ref list, model.RecordID);

                        //cap nhat Type cho danh sach chuyen muc con
                        if (list.Count > 1)
                            WebMenuService.Instance.Update("[ID] IN (" + Core.Global.Array.ToString(list.ToArray()) + ")",
                                "@Type", WebMenuService.Instance.GetByID(_item.ParentID).Type);
                    }
                }
                catch (Exception ex)
                {
                    Error.Write(ex);
                    CPViewPage.Message.ListMessage.Add(ex.Message);
                    return false;
                }

                return true;
            }
        }

        private static int GetMaxOrder(SysMenuModel model)
        {
            return WebMenuService.Instance.CreateQuery()
                    .Where(o => o.LangID == model.LangID && o.ParentID == model.ParentID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private void GetMenuIDChildForDelete(ref List<int> list, int[] arrID)
        {
            for (var i = 0; arrID != null && i < arrID.Length; i++)
            {
                GetMenuIDChild(ref list, arrID[i]);
            }
        }

        private static void GetMenuIDChild(ref List<int> list, int menuID)
        {
            list.Add(menuID);

            var listMenu = WebMenuService.Instance.CreateQuery()
                                                .Where(o => o.ParentID == menuID)
                                                .ToList();

            for (var i = 0; listMenu != null && i < listMenu.Count; i++)
            {
                GetMenuIDChild(ref list, listMenu[i].ID);
            }
        }

        #endregion private func
    }

    public class SysMenuModel : DefaultModel
    {
        public int ParentID { get; set; }

        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

        public int State { get; set; }
        public int[] ArrState { get; set; }

        public string Value { get; set; }
    }
}