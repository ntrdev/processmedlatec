﻿using System;
using IMEX.Core.Models;

namespace IMEX.Lib.Models
{
    public class ModScheduleEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int ScheduleID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Gender { get; set; }

        [DataInfo]
        public int BirthYear { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string Address { get; set; }

        [DataInfo]
        public int ServiceID { get; set; }

        [DataInfo]
        public int ScheduleTimeID { get; set; }

        [DataInfo]
        public int ScheduleTypeID { get; set; }

        [DataInfo]
        public int OrganizeID { get; set; }

        [DataInfo]
        public int DoctorID { get; set; }

        [DataInfo]
        public DateTime DateSchedule { get; set; }

        [DataInfo]
        public DateTime DateCreate { get; set; }

        [DataInfo]
        public string Note { get; set; }

        [DataInfo]
        public bool Active { get; set; }

        [DataInfo]
        public bool IsCancel { get; set; }

        [DataInfo]
        public int SpecialistID { get; set; }

        [DataInfo]
        public string Counpon { get; set; }

        [DataInfo]
        public string CounponType { get; set; }

        [DataInfo]
        public string TestCode { get; set; }

        [DataInfo]
        public string DoctorIDCode { get; set; }

        [DataInfo]
        public DateTime Birthday { get; set; }

        [DataInfo]
        public string DateExamination { get; set; }

        [DataInfo]
        public string TimeExamination { get; set; }

        [DataInfo]
        public string ReasonExamination { get; set; }

        [DataInfo]
        public string AutoCode { get; set; }

        [DataInfo]
        public DateTime DateUsed { get; set; }

        [DataInfo]
        public DateTime CancelDate { get; set; }

        [DataInfo]
        public string OrganizeCode { get; set; }


        [DataInfo]
        public string SpecialistCode { get; set; }

        [DataInfo]
        public bool Confirms { get; set; }

        [DataInfo]
        public int MedicalProfileID { get; set; }

        [DataInfo]
        public bool OldCustomer { get; set; }

        [DataInfo]
        public string Status { get; set; }

        #endregion
    }

    public class ModScheduleService : ServiceBase<ModScheduleEntity>
    {
        #region Autogen by RDV

        private ModScheduleService()
            : base("[Schedule]", "ScheduleID")
        {
            DBConfigKey = "DBConnectMEDO";
        }

        private static ModScheduleService _instance;
        public static ModScheduleService Instance
        {
            get { return _instance ?? (_instance = new ModScheduleService()); }
        }

        #endregion

        public ModScheduleEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ScheduleID == id)
               .ToSingle();
        }
        public ModScheduleEntity GetByID_Cache(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ScheduleID == id)
               .ToSingle_Cache();
        }

    }
}