﻿using IMEX.Core.Models;

namespace IMEX.Lib.Models
{
    public class CPUserRoleEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int RoleID { get; set; }

        #endregion Autogen by RDV
    }

    public class CPUserRoleService : ServiceBase<CPUserRoleEntity>
    {
        #region Autogen by RDV

        public CPUserRoleService() : base("[CP_UserRole]")
        {
        }

        private static CPUserRoleService _instance;

        public static CPUserRoleService Instance => _instance ?? (_instance = new CPUserRoleService());

        #endregion Autogen by RDV
    }
}