﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.ViewControl" %>


<%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.Content) %>


<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="pricingTable">
            <div class="pricing-content">
                <ul class="pricing-content">
                    <li><b>Lịch hẹn Lấy mẫu xét nghiệm tại nhà</b></li>
                    <li>
                        <ul class="info-nv">
                            <li><b><i class="fa fa-user"></i>&nbsp;Vũ Đoàn</b><b class="mr-info"><i class="fa fa-phone"></i>&nbsp;0912587504</b> </li>
                            <li><b><i class="fa fa-clock-o"></i>&nbsp;NV sẽ đến lúc 8:30 - 9:00 ngày 25/05/2020</b></li>
                            <li><b><i class="fa fa-map-marker"></i>&nbsp;63 Cửa Bắc, Ba Đình, Hà Nội</b></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="pricingTable blue">

            <div class="pricing-content">
                <ul class="pricing-content">
                    <li><b>Thông tin CB lấy mẫu tại nhà</b></li>
                    <li>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <div class="profile-userpic">
                                    <img src="/interface/img/favicon.png" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10">
                                <ul class="info-nv">
                                    <li><b>Đặng Giang Nam</b><b class="mr-info"><i class="fa fa-star"></i>&nbsp;4.5</b></li>
                                    <li><b>Mã NV: 6783 | Ngày sinh: 02/02/1989</b></li>
                                    <li><b><i class="fa fa-phone"></i>&nbsp; 0972548204</b></li>
                                </ul>

                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 subtitle text-center" style="color: orangered; margin-top: 3%">* Quý khách lưu ý chỉ đồng ý sử dụng dịch vụ khi nhận diện đúng NV lấy mẫu tại nhà qua các thông tin phía trên để đề phòng kẻ gian lợi dụng.</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 ">
        <div class="col-lg-12 col-md-12 subtitle text-center" style="color: orangered; margin-top: 3%">* Quý khách vui lòng click vào các bước bên dưới để xem thêm thông tin.</div>
        <div class="listGroupResuilt">
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>1. Lịch hẹn đã được xác nhận</span>
                </div>

            </div>
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>2. Hoàn thành khám/lấy mẫu</span>
                </div>

            </div>
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>3. Trạng thái mẫu bệnh phẩm</span>
                </div>

            </div>
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>4. Kết quả khám/xét nghiệm</span>
                </div>

            </div>
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>5. Tư vấn kết quả</span>
                </div>

            </div>
            <div class="block_list_item">
                <div class="item_heading" data-check="0">
                    <span>6. Lịch hẹn tái khám</span>
                </div>

            </div>
        </div>
    </div>

</div>

