﻿namespace IMEX.Lib.Global
{
    public class Pager
    {
        private int _totalRecord = -1;
        private int _pageSize = 10;
        private int _pageMax = 10;

        public int TotalRecord
        {
            get => _totalRecord;
            set
            {
                if (value > 0)
                    _totalRecord = value;
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                if (value > 0)
                    _pageSize = value;
            }
        }

        public int PageIndex { get; set; }

        public int PageMax
        {
            get => _pageMax;
            set
            {
                if (value > 0)
                    _pageMax = value;
            }
        }

        public int Skip => PageIndex * PageSize;

        public int TotalBegin { get; private set; }

        public int TotalEnd { get; private set; }

        public int TotalPage
        {
            get
            {
                if (PageSize == 0)
                    return 0;

                return (TotalRecord % PageSize == 0 ? 0 : 1) + (TotalRecord / PageSize);
            }
        }

        public string Url { get; set; } = string.Empty;

        public string ParamName { get; set; } = "page";

        public string ActionName { get; set; } = "";

        public string CssClass { get; set; } = string.Empty;

        public string BackText { get; set; } = "<i class=\"fa fa-chevron-left\"></i>";

        public string NextText { get; set; } = "<i class=\"fa fa-chevron-right\"></i>";

        public string BackEndText { get; set; } = "<i class=\"fa fa-angle-double-left\"></i>";

        public string NextEndText { get; set; } = "<i class=\"fa fa-angle-double-right\"></i>";

        public bool IsCpLayout { get; set; }

        public bool DisableMode { get; set; }

        public string Html { get; private set; } = string.Empty;

        public void Update()
        {
            var pageIndex = PageIndex;
            var minPage = pageIndex / _pageMax * _pageMax;
            var maxPage = minPage + _pageMax;

            var maxPageIndex = _totalRecord / ((double)_pageSize);
            TotalBegin = pageIndex * _pageSize;
            TotalEnd = TotalBegin + _pageSize;

            if (maxPageIndex - pageIndex < 1)
                TotalEnd = _totalRecord;

            var url = Url;

            string[] allKey = Core.Web.HttpQueryString.AllKeys;
            for (var i = 0; i < allKey.Length; i++)
            {
                var key = allKey[i].Trim();
                if (string.IsNullOrEmpty(key) || key.Equals("page", System.StringComparison.OrdinalIgnoreCase)) continue;

                var value = Core.Web.HttpQueryString.GetValue(key).ToString().Trim();

                if (!url.Contains("?"))
                    url += "?" + key + "=" + System.Web.HttpContext.Current.Server.UrlEncode(value);
                else
                    url += "&" + key + "=" + System.Web.HttpContext.Current.Server.UrlEncode(value);
            }

            if (url.EndsWith("/"))
                url = ParamName;
            else if (url.Contains("?"))
                url += "&" + ParamName;
            else
                url += "?" + ParamName;

            if (IsCpLayout)
            {
                #region CP

                if (!(maxPageIndex > 1)) return;

                if (maxPage > _pageMax)
                {
                    Html += @"<li class=""page-item""><a href=""javascript:REDDEVILRedirect('" + ActionName + @"', " + 1 + @", '" + ParamName + @"')"" class=""page-link"" title=""Quay lại trang đầu"">" + BackEndText + @"</a></li>";
                    Html += @"<li class=""page-item""><a href=""javascript:REDDEVILRedirect('" + ActionName + @"', " + minPage + @", '" + ParamName + @"')"" class=""page-link"" title=""Trang trước"">" + BackText + @"</a></li>";
                }
                else
                {
                    Html += @"<li class=""page-item disabled""><a href=""#"" class=""page-link"" title=""Quay lại trang đầu""> " + BackEndText + @"</a></li>";
                    Html += @"<li class=""page-item disabled""><a href=""#"" class=""page-link"" title=""Trang trước"">" + BackText + @"</a></li>";
                }

                for (var i = minPage; i < maxPage; i++)
                {
                    if (i != pageIndex)
                    {
                        if (i < maxPageIndex)
                            Html += @"<li class=""page-item""><a href=""javascript:REDDEVILRedirect('" + ActionName + @"', " + (i + 1) + @", '" + ParamName + @"')"" class=""page-link"" title=""Trang tiếp theo"">" + (i + 1) + @"</a></li>";
                    }
                    else
                    {
                        if (i < maxPageIndex)
                            Html += @"<li class=""page-item""><a href= ""#"" class=""page-link disabled"">" + (i + 1) + @"</a></li>";
                    }
                }

                if (maxPage < maxPageIndex)
                {
                    Html += @"<li class=""page-item""><a href=""javascript:REDDEVILRedirect('" + ActionName + @"', " + (maxPage + 1) + @", '" + ParamName + @"')"" class=""page-link"" title=""Trang " + (maxPage + 1) + @""">" + NextText + @"</a></li>";
                    Html += @"<li class=""page-item""><a href=""javascript:REDDEVILRedirect('" + ActionName + @"', " + (maxPageIndex > (int)maxPageIndex ? (int)maxPageIndex + 1 : maxPageIndex) + @", '" + ParamName + @"')"" class=""page-link"" title=""Trang cuối cùng"">" + NextEndText + @"</a></li>";
                }
                else
                {
                    Html += @"<li class=""page-item disabled""><a href=""#"" class=""page-link"" title=""Trang tiếp"">" + NextText + @"</a></li>";
                    Html += @"<li class=""page-item disabled""><a href=""#"" class=""page-link"" title=""Trang cuối"">" + NextEndText + @"</a></li>";
                }

                #endregion CP
            }
            else
            {
                #region Web

                //Html = string.Empty;
                //if (!(maxPageIndex > 1)) return;

                //if (maxPage > _pageMax)
                //{
                //    Html += @"<li class=""page-item""><a href=""" + url + (url.Contains("?") ? "=" : "/") + 1 + @""" class=""page-link"" title=""Quay lại trang đầu"">" + BackEndText + @"</a></li>";
                //    Html += @"<li class=""page-item""><a href=""" + url + (url.Contains("?") ? "=" : "/") + minPage + @""" class=""page-link"" title=""Trang trước"">" + BackText + @"</a></li>";
                //}
                //else if (DisableMode)
                //{
                //    Html += @"<li class=""page-item disabled""><a href=""javascript:void(0)"" class=""page-link"" title=""Quay lại trang đầu"">" + BackEndText + @"</a></li>";
                //    Html += @"<li class=""page-item disabled""><a href=""javascript:void(0)"" class=""page-link"" title=""Trang trước"">" + BackText + @"</a></li>";
                //}

                //for (var i = minPage; i < maxPage; i++)
                //{
                //    if (i != pageIndex)
                //    {
                //        if (i < maxPageIndex)
                //        {
                //            Html += @"<li class=""page-item""><a href=""" + url + (url.Contains("?") ? "=" : "/") + (i + 1) + @""" class=""page-link"">" + (i + 1) + @"</a></li>";
                //        }
                //    }
                //    else
                //    {
                //        if (i < maxPageIndex)
                //            Html += @"<li class=""page-item active""><a href=""javascript:void(0)"" class=""page-link"">" + (i + 1) + @"</a></li>";
                //    }
                //}

                //if (maxPage < maxPageIndex)
                //{
                //    Html += @"<li class=""page-item""><a href=""" + url + (url.Contains("?") ? "=" : "/") + (maxPage + 1) + @""" class=""page-link"" title=""Trang tiếp"">" + NextText + @"</a></li>";
                //    Html += @"<li class=""page-item""><a href=""" + url + (url.Contains("?") ? "=" : "/") + (maxPageIndex > (int)maxPageIndex ? (int)maxPageIndex + 1 : maxPageIndex) + @""" class=""page-link"" title=""Trang cuối"">" + NextEndText + @"</a></li>";
                //}
                //else if (DisableMode)
                //{
                //    Html += @"<li class=""page-item disabled""><a href=""javascript:void(0)"" class=""page-link"" title=""Trang tiếp"">" + NextText + @"</a></li>";
                //    Html += @"<li class=""page-item disabled""><a href=""javascript:void(0)"" class=""page-link"" title=""Trang cuối"">" + NextEndText + @"</a></li>";
                //}
                Html = string.Empty;
                if (!(maxPageIndex > 1)) return;

                if (maxPage > _pageMax)
                {
                    Html += @"<a class=""page-numbers prev"" href=""" + url + (url.Contains("?") ? "=" : "/") + 1 + @"""  title=""Quay lại trang đầu""><svg class=""btn-prev""><use xlink:href=""#arrow-left""></use></svg></a>";
                   // Html += @"<a class=""page-numbers prev"" href=""" + url + (url.Contains("?") ? "=" : "/") + minPage + @""" title=""Trang trước""><svg class=""btn-prev""><use xlink:href=""#arrow-left""></use></svg></a>";
                }
                else if (DisableMode)
                {
                    Html += @"<a class=""page-numbers prev"" href=""javascript:void(0)""  title=""Quay lại trang đầu""><svg class=""btn-prev""><use xlink:href=""#arrow-left""></use></svg></a>";
                   // Html += @"<a class=""page-numbers prev"" href=""javascript:void(0)""  title=""Trang trước""><svg class=""btn-prev""><use xlink:href=""#arrow-left""></use></svg></a>";
                }

                for (var i = minPage; i < maxPage; i++)
                {
                    if (i != pageIndex)
                    {
                        if (i < maxPageIndex)
                        {
                            Html += @"<a class=""page-numbers bg-border-color"" href=""" + url + (url.Contains("?") ? "=" : "/") + (i + 1) + @""">" + (i + 1) + @"</a>";
                        }
                    }
                    else
                    {
                        if (i < maxPageIndex)
                            Html += @"<a class=""page-numbers bg-border-color current"" href=""javascript:void(0)"">" + (i + 1) + @"</a>";
                    }
                }

                if (maxPage < maxPageIndex)
                {
                    Html += @"<a class=""page-numbers next"" href=""" + url + (url.Contains("?") ? "=" : "/") + (maxPage + 1) + @""" title=""Trang tiếp""><svg class=""btn-next""><use xlink:href=""#arrow-right""></use></svg></a>";
                   // Html += @"<a class=""page-numbers next"" href=""" + url + (url.Contains("?") ? "=" : "/") + (maxPageIndex > (int)maxPageIndex ? (int)maxPageIndex + 1 : maxPageIndex) + @""" title=""Trang cuối""><svg class=""btn-next""><use xlink:href=""#arrow-right""></use></svg></a>";
                }
                else if (DisableMode)
                {
                    Html += @"<a class=""page-numbers next""  href=""javascript:void(0)""  title=""Trang tiếp""><svg class=""btn-next""><use xlink:href=""#arrow-right""></use></svg></a>";
                   // Html += @"<a class=""page-numbers next""  href=""javascript:void(0)""  title=""Trang cuối""><svg class=""btn-next""><use xlink:href=""#arrow-right""></use></svg></a>";
                }
                #endregion Web


            }
        }
    }
}