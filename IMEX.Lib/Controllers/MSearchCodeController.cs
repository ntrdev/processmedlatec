﻿using IMEX.Lib.Models;
using IMEX.Lib.MVC;
using System.Web;
using Controller = IMEX.Lib.MVC.Controller;

namespace IMEX.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Mã lịch hẹn", Code = "MSearchCode", Order = 1, Crawl = false)]
    public class MSearchCodeController : Controller
    {
        public void ActionIndex(MSearchCodeModel model)
        {

            if (string.IsNullOrEmpty(model.token))
            {
                ViewPage.AlertThenRedirect("Token không hợp lệ."); return;
            }

            int _id = Core.Global.Convert.ToInt(Core.Global.Security.Decrypt(model.token).Replace("MrReddevil_", string.Empty));
            if (_id > 0)
            {
                var _process = ModProcessService.Instance.CreateQuery().Where(o => o.ProcessID == _id).ToSingle();
                if (_process != null)
                {
                    var _nhanvien = ModNhanVienService.Instance.CreateQuery().Select(o => new { o.maTN, o.soDT })
                                            .Where(o => o.maNV == _process.UserNhanLich).ToSingle_Cache() ?? new ModNhanVienEntity();
                    var _sysUser = ModSysNhanVienService.Instance.CreateQuery().Select(o => o.UserCode)
                                            .Where(o => o.UserID == _nhanvien.maTN).ToSingle_Cache() ?? new ModSysNhanVienEntity();

                    var _employee = ModKPIEmployeeService.Instance.CreateQuery().Select(o => new { o.NgaySinh, o.Hoten, o.Manhanvien, o.Anh })
                                            .Where(o => o.Manhanvien == _sysUser.UserCode && o.DataType == "Employee")
                                            .ToSingle_Cache() ?? new ModKPIEmployeeEntity();


                    ViewBag.Customer = ModScheduleService.Instance.CreateQuery().Select(o => new { o.Name, o.UserID, o.Phone }).Where(o => o.ScheduleID == _process.ScheduleID).ToSingle();
                    _process.token = HttpUtility.UrlEncode(model.token);
                    ViewBag.Employee = _employee;
                    ViewBag.NhanVien = _nhanvien;
                    ViewBag.Process = _process;

                }
                else ViewPage.AlertThenRedirect("Dữ liệu không tồn tại trên hệ thống.");

                //SEO
                ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 600, 315);
            }
            else ViewPage.AlertThenRedirect("Token không hợp lệ.");
        }
    }
    public class MSearchCodeModel
    {
        public string token { get; set; }
    }
}