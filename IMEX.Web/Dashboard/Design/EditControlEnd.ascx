﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<script runat="server">

    private string _cphName = string.Empty;
    public string CphName
    {
        get { return _cphName; }
        set { _cphName = value; }
    }

    List<IMEX.Lib.MVC.ModuleInfo> _listModule;
    protected void Page_Load(object sender, EventArgs e)
    {
        _listModule = IMEX.Lib.Web.Application.Modules.Where(o => o.IsControl == true).OrderBy(o => o.Order).ToList();
    }
</script>

<div id="to_cph_<%= CphName%>" ondragenter="return dragEnter(event)" ondrop="return dragDrop(event)" ondragover="return dragOver(event)"></div>
<li class="col-sm-12 col-xs-12 item">
    <div class="border-control">
        <a href="javascript:void(0)" onclick="do_display('tbl_<%= CphName%>')">Thêm mới điều khiển</a>
    </div>
    <div class="border-control" id="tbl_<%= CphName%>" style="display: none;">
        <div class="form-body mt10">
            <div class="form-group form-md-line-input has-warning col-sm-6">
                <input type="text" class="form-control" id="rdv_cph_<%= CphName %>_RDVID" name="rdv_cph_<%= CphName %>_RDVID" value="" placeholder=" Ví dụ: MenuTop...">
                <label for="form_control_1">Tên điều khiển:</label>
                <span class="help-block">Ký tự tối đa 20</span>
            </div>
            <div class="form-group form-md-line-input has-warning col-sm-6">
                <select class="form-control chosen-layout" name="rdv_cph_<%= CphName %>_control_code" id="rdv_cph_<%= CphName %>_control_code">
                    <%for (var i = 0; _listModule != null && i < _listModule.Count; i++)
                        { %>
                    <option value="<%= _listModule[i].Code%>"><%= _listModule[i].Name%></option>
                    <%} %>
                    <option value="RDVMODULE">RDVMODULE</option>
                </select>
                <label for="form_control_1">Chọn điều khiển</label>
            </div>

            <div class="cmd-control justify-content-end">
                <button data-toggle="tooltip" data-original-title="Thêm điều khiển" onclick="cp_update('rdv_cph_<%= CphName %>|add')" type="button" data-placement="bottom" class="btn waves-effect waves-light green  btn-icon"><i class="icon-plus"></i></button>
            </div>
        </div>
    </div>
</li>
<div style="padding-top: 10px"></div>
</div>