﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.ViewControl" %>


<%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.Content) %>


<div class="turn-up--container">
    <div class="turn-up--form">

        <h1 class="title text-center">QUẢN LÝ TIẾN TRÌNH LẤY MẪU TẠI NHÀ</h1>
        <div class="subtitle text-center">Quý khách vui lòng nhập mã lịch hẹn để xem tiến trình dịch vụ.</div>


        <div class="container">

            <div class="turn-up--wrapper ">


                <div class="turn-up--panel">
                    <div class="form">
                        <div id="tab-customer" class="tab-customer turn-up--content">
                            <div class="form-group">
                                <label for="dateCustomer">Mã lịch hẹn <i>(*)</i></label>
                                <input id="code-medical" class="form-control tab-customer--date" name="code" placeholder="Nhập mã lịch hẹn" type="text" value="" />
                            </div>
                            <div class="subtitle text-center" style="color: orangered">* Mã lịch hẹn là mã được MEDLATEC gửi đến tin nhắn điện thoại của bạn khi lịch hẹn được xác nhận đặt thành công.</div>
                            <div class="form-group form-group--btn clearfix">
                                <a href="javascrpit:void(0)" id="process" class="btn-form--submit disabled">Xem tiến trình</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
