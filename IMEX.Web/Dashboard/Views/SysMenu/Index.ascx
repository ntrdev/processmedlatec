﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<script runat="server">
    private List<EntityBase> AutoGetMap(SysMenuModel model)
    {
        List<EntityBase> list = new List<EntityBase>();

        int _id = model.ParentID;
        while (_id > 0)
        {
            var _item = WebMenuService.Instance.GetByID(_id);

            if (_item == null)
                break;

            _id = _item.ParentID;

            list.Insert(0, _item);
        }

        return list;
    }
</script>

<% 
    var model = ViewBag.Model as SysMenuModel;
    var listItem = ViewBag.Data as List<WebMenuEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />
    <div class="page-content">

        <div class="breadcrumbs">
            <h1>Quản lý Danh mục</h1>
            <ol class="breadcrumb">
                <%= ShowMap(AutoGetMap(model))%>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultNoCopyListCommand()%>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Danh mục</span>
                        </div>
                        <div class="tools col-md-9">

                            <div class="dataTables_search col-md-4 col-xs-12">
                                <input type="text" class="form-control input-inline input-sm" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <th>
                                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="text-left"><%= GetSortLink("Tên danh mục", "Name")%></th>
                                        <%if (model.ParentID == 0)
                                            { %>
                                        <th class="sorting text-left hidden-sm hidden-col"><%= GetSortLink("Loại danh mục", "Type")%></th>
                                        <%} %>
                                        <th class="text-center">Trạng thái</th>
                                        <th style="width: 110px">Sắp xếp vị trí</th>
                                        <th class="text-center">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        {

                                    %>
                                    <tr>

                                        <td class="text-center"><%=i+1 %></td>
                                        <td><%= GetCheckbox(listItem[i].ID, i)%></td>
                                        <td class="text-left"><a href="javascript:REDDEVILRedirect('Index', <%= listItem[i].ID %>, 'ParentID')">&nbsp;<%= listItem[i].Name%></a></td>
                                        <%if (model.ParentID == 0)
                                            {%>
                                        <td class="text-left hidden-sm hidden-col">
                                            <%= listItem[i].Type %>
                                        </td>
                                        <%} %>

                                        <td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Activity)%></td>
                                        <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                        <td class="text-center">
                                            <div class="control-button ">
                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        window.document.title = 'Quản trị Danh mục for MEDO | Power by IMEXsoft®';

        var REDDEVILController = 'SysMenu';

        var REDDEVILArrVar = [
            'filter_lang', 'LangID',
            'limit', 'PageSize'
        ];

        var REDDEVILArrQT = [
            '<%= model.PageIndex + 1 %>', 'PageIndex',
            '<%= model.ParentID %>', 'ParentID',
            '<%= model.Sort %>', 'Sort'
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrDefault = [
            '1', 'PageIndex',
            '1', 'LangID',
            '20', 'PageSize'
        ];
    </script>

</form>
