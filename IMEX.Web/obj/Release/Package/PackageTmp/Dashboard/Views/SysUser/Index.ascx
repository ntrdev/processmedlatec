﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysUserModel;
    var listItem = ViewBag.Data as List<CPUserEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý người dùng</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Người dùng</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetSoftDefautCommand() %>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Danh sách người dùng</span>
                        </div>
                        <div class="tools col-md-9">

                            <div class="dataTables_search col-md-3 col-xs-12">
                                <input type="text" class="form-control" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">STT</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting"><%= GetSortLink("Tên đăng nhập", "LoginName")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Avatar", "File")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Họ và tên", "Name")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Địa chỉ", "Address")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Điện thoại", "Phone")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Email", "Email")%></th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td>
                                            <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].LoginName%></a>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Name %></td>
                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Address %></td>
                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Phone %></td>
                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Email %></td>
                                        <td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Active)%></td>
                                        <td class="text-center">
                                            <div class="control-button">
                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        window.document.title = 'Quản trị tài khoản quản trị for MEDO | Power by IMEXsoft®';

        var REDDEVILController = "SysUser";

        var REDDEVILArrVar = [
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

</form>
