﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CPLogin.IsLogin() && CPLogin.CurrentUser.IsAdministrator) return;

        Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        var listProduct = ModProductService.Instance.CreateQuery().Select(o => new { o.Content, o.Specifications }).ToList_Cache();
        for (int i = 0; listProduct != null && i < listProduct.Count; i++)
        {
            if (!string.IsNullOrEmpty(listProduct[i].Content) && listProduct[i].Content.Contains("bepvuson.vn"))
            {
                listProduct[i].Content = listProduct[i].Content.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");

                ModProductService.Instance.Save(listProduct[i], o => new { o.Content });
            }

            if (!string.IsNullOrEmpty(listProduct[i].Specifications) && listProduct[i].Specifications.Contains("bepvuson.vn"))
            {
                listProduct[i].Content = listProduct[i].Content.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");
                listProduct[i].Specifications = listProduct[i].Specifications.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");

                ModProductService.Instance.Save(listProduct[i], o => new { o.Specifications });
            }
        }

        var listNews = ModNewsService.Instance.CreateQuery().Select(o => new { o.Content }).ToList_Cache();
        for (int i = 0; listNews != null && i < listNews.Count; i++)
        {
            if (!string.IsNullOrEmpty(listNews[i].Content) && listNews[i].Content.Contains("bepvuson.vn"))
            {
                listNews[i].Content = listNews[i].Content.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");
                ModNewsService.Instance.Save(listNews[i], o => o.Content);
            }
        }

        var listPage = SysPageService.Instance.CreateQuery().Select(o => new { o.Content, o.TopContent }).ToList_Cache();
        for (int i = 0; listPage != null && i < listPage.Count; i++)
        {
            if (!string.IsNullOrEmpty(listPage[i].TopContent) && listPage[i].TopContent.Contains("bepvuson.vn"))
            {
                listPage[i].TopContent = listPage[i].TopContent.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");
                SysPageService.Instance.Save(listPage[i], o => new { o.TopContent });
            }

            if (!string.IsNullOrEmpty(listPage[i].Content) && listPage[i].Content.Contains("bepvuson.vn"))
            {
                listPage[i].Content = listPage[i].Content.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");
                SysPageService.Instance.Save(listPage[i], o => new { o.Content });
            }
        }

        var listAdv = ModAdvService.Instance.CreateQuery().Select(o => new { o.URL }).ToList_Cache();
        for (int i = 0; listAdv != null && i < listAdv.Count; i++)
        {
            if (!string.IsNullOrEmpty(listAdv[i].URL) && listAdv[i].URL.Contains("bepvuson.vn"))
            {
                listAdv[i].URL = listAdv[i].URL.Replace("bepvuson.vn", "<%=IMEX.Core.Web.HttpRequest.Host%>");
                ModAdvService.Instance.Save(listAdv[i], o => o.URL);
            }
        }
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SQL</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center;" >
                <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Run" Width="111px" />
            </div>
        </div>
    </form>
</body>
</html>