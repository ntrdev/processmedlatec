﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysMenuModel;
    var item = ViewBag.Data as WebMenuEntity;

    var listParent = IMEX.Lib.Global.ListItem.List.GetList(WebMenuService.Instance, model.LangID);

    if (model.RecordID > 0)
    {
        //loai bo danh muc con cua danh muc hien tai
        listParent = IMEX.Lib.Global.ListItem.List.GetListForEdit(listParent, model.RecordID);
    }

%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật " +item.Name: "Thêm mới Danh mục"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Danh mục</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="<%=item.ParentID>0?"col-sm-9":"col-sm-12" %>">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin Danh mục</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" autocomplete="off" placeholder="Nhập Danh mục vào đây ...">
                                        <label for="form_control_1">Tên danh mục:</label>
                                        <span class="help-block">Ký tự tối đa 100</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" name="Url" value="<%=item.Url %>" autocomplete="off" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Url danh mục:</label>
                                        <span class="help-block">ví dụ: Tin-covid</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="1" name="Summary" placeholder="Mô tả tóm tắt về danh mục"><%=item.Summary %></textarea>
                                        <label for="form_control_1">Mô tả tóm tắt:</label>
                                        <span class="help-block">Mô tả ngắn về danh mục</span>
                                    </div>
                                </div>

                                <%if (item.ParentID == 0)
                                    { %>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Type" name="Type" value="<%=item.Type %>" autocomplete="off" placeholder="">
                                        <label for="form_control_1">Loại danh mục:</label>
                                        <span class="help-block">ví dụ: danh mục tin tức= News</span>
                                    </div>
                                </div>
                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%if (item.ParentID > 0)
                { %>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-Bannerance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh đại diện</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-Bannerance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh/banner</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính cho danh mục</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <select class="form-control chosen" name="ParentID">
                                        <option value="0">Chọn danh mục cha</option>
                                        <%for (var i = 0; listParent != null && i < listParent.Count; i++)
                                            { %>
                                        <option <%if (item.ParentID.ToString() == listParent[i].Value)
                                            {%>selected<%} %>
                                            value="<%= listParent[i].Value%>"><%= listParent[i].Name%></option>
                                        <%} %>
                                    </select>
                                    <label for="form_control_1">danh mục cha:</label>
                                </div>
                            </div>

                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Active" value="1" class="md-radiobtn" <%=item.Activity ? "checked" : "" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Active" value="0" class="md-radiobtn" <%=!item.Activity ? "checked" : "" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>

            </div>
            <%} %>
        </div>
    </div>

</form>

<link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
<script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>

<script type="text/javascript">
    window.document.title = 'Quản trị Danh mục for MEDO | Power by IMEXsoft®';
    $('.chosen').chosen({
        search_contains: true,
        no_results_text: 'Không tìm thấy kết quả phù hợp'
    });</script>
