﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysResourceModel;
    var listItem = ViewBag.Data as List<WebResourceEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý tài nguyên</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Tài Nguyên</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultListCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="portlet-body">
                    <div class="dataTables_wrapper">
                        <div class="row hidden-sm hidden-col">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="dataTables_search">
                                    <input type="text" class="form-control input-inline input-sm" id="filter_search" value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                    <button type="button" class="btn blue" onclick="REDDEVILRedirect();return false;">Tìm kiếm</button>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-right pull-right">
                                <div class="table-group-actions d-inline-block">
                                    <%= ShowDDLLang(model.LangID)%>
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">#</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn tất cả">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting"><%= GetSortLink("Tên tài nguyên", "Name")%></th>
                                        <th class="sorting text-center"><%= GetSortLink("Nội dung", "Value")%></th>
                                        <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td>
                                            <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Code%></a>
                                        </td>
                                        <td class="text-center"><%= listItem[i].Value%></td>
                                        <td class="text-center"><%= listItem[i].ID%></td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex  center">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var REDDEVILController = "SysResource";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>

</form>
