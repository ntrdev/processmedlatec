﻿var html = ' ';
html += ' <div class="placeload">';
html += '   <div class="placeholder-footer ">';
html += '      <div class="footer-block">';
html += '           <div class="content-shape loads"></div>';
html += '            <div class="content-shape loads"></div>';
html += '       </div>';
html += '   </div>';
html += ' </div>';


$('.item_heading').on('click', function () {
    $(this).addClass('heading-load');
    if ($(this).data('check') == 0) {

        $(this).parent().find('.item_body').append(html);
        $(this).parent().find('.item_body').show();
        loadprocess($('#token').val(), $(this).data('step'), $(this));

    }
    $(this).data('check', 1);


});

$('#code-medical').on("keyup", function () {

    if ($(this).val() == '') {
        $('#process').prop("disabled", true);
        $('#process').addClass("disabled");
    }
    else {
        $('#process').prop("disabled", false);
        $('#process').removeClass("disabled");
    }
})


$('#process').on('click', function () {
    var _sub = $('#code-medical').val();
    if (_sub == undefined || _sub == '') {
        swee_alert('Thông báo', 'Nhập mã lịch hẹn');
        return;
    }
    $.ajax({
        url: '/api/GetProcess/',
        data: { email: _sub },
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var mess = data.Message;
            var content = data.Html;
            if (mess != '') alert_swee('Thông báo', mess);
            if (content != '') {
                location.href = '/tien-trinh-lay-mau/?token=' + content;
            }
        },
        error: function () { }
    });


});

function loadprocess(token, step, thistag) {

    if (token == '' || token == undefined) alert_swee('Thông báo', 'Tiến trình không tồn tại');
    else {

        $.ajax({
            url: '/api/StepProcess/',
            data: { token: token, step: step },
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var mess = data.Message;
                var content = data.Html;
                if (mess != '') alert_swee('Thông báo', mess);
                if (content != '') {
                    $(thistag).parent().find('.item_body').fadeOut(500, function () {


                        $(thistag).parent().find('.item_body').html(content).fadeIn(500);

                    });


                }
            },
            error: function () { }
        });
    }
}



function postRate() {
    var _maprate = $('.start-rate').map(function () {
        return this;
    }).get();
    var _comment = $('#comment').val();
    var _alert = '';
    var _diem = 0;
    for (var i = 0; i < _maprate.length; i++) {
        if (_maprate[i].value < 1) {
            var _parent = _maprate[i];
            _alert += _parent.parentNode.children.item(0).innerText + '<br/>';
        }
        else {
            _diem += parseInt(_maprate[i].value);
        }
    }
    if (_comment == '' || _comment == undefined) _alert += 'Quý khách vui lòng nhập thêm góp ý cho MEDLATEC.';
    if (_alert != '') { swee_alert('Thông báo', _alert); }
    else {
        var token = $('#token').val();


        $.ajax({
            url: '/api/Rate/',
            data: { token: token, step: _diem, content: _comment },
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var mess = data.Message;
                if (mess != '') {
                    $('.close-model').click(); $('.rating-process').html(data.Html); alert_swee('Thông báo', mess);
                    $('.rating-process').removeAttr('data-toggle');
                }
            },
            error: function () { }
        });

    }
}


function loadprocess2(token, thistag) {

    if (token == '' || token == undefined) alert_swee('Thông báo', 'Tiến trình không tồn tại');
    else {

        $.ajax({
            url: '/api/StepProcess/',
            data: { token: token },
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var mess = data.Message;
                var content = data.Html;
                if (mess != '') alert_swee('Thông báo', mess);
                if (content != '') {
                    $(thistag).fadeOut(500, function () {
                        $(thistag).html(content).fadeIn(500);
                    });
                }
            },
            error: function () { }
        });
    }
}


function postRate_service() {

    var _comment = $('#comment_bacsi').val();
    var _pointSevice = $('#PointService').val();
    var _doctorLevel = $('#DoctorLevel').val();
    var _doctorPoint = $('#DoctorPoint').val();
    var _timeAdvisoryPoint = $('#TimeAdvisoryPoint').val();
    var _alert = '';

    if (_timeAdvisoryPoint < 1) _alert += 'Quý khách vui lòng đánh giá về thời gian tư vấn của bác sĩ.<br/>';
    if (_doctorPoint < 1) _alert += 'Quý khách vui lòng đánh giá về thái độ của bác sĩ.<br/>';
    if (_doctorLevel < 1) _alert += 'Quý khách vui lòng đánh giá về trình độ của bác sĩ.<br/>';
    if (_pointSevice < 1) _alert += 'Quý khách vui lòng cho chúng tôi biết mức độ hài lòng của quý khách.<br/>';
    if (_comment == '' || _comment == undefined) _alert += 'Quý khách vui lòng nhập thêm góp ý giúp nâng cao chất lượng dịch vụ của MEDLATEC.';


    if (_alert != '') { swee_alert('Thông báo', _alert); }
    else {
        var token = $('#token').val();
        $.ajax({
            url: '/api/RateAdvisory/',
            data: {
                token: token,
                doctocLevel: _doctorLevel,
                doctorPoint: _doctorPoint,
                timeAdvisoryPoint: _timeAdvisoryPoint,
                pointSevice: _pointSevice,
                content: _comment
            },
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var mess = data.Message;
                if (mess != '') {
                    $('.close-model').click();
                    $('.alert-process').find('.tuvan-star').html(data.Html);
                    $('.alert-process').find('.tuvan-star').removeAttr('data-toggle');
                    alert_swee('Thông báo', mess);
                }
            },
            error: function () { }
        });

    }
}