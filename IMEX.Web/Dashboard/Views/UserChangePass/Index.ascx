﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Thay đổi mật khẩu</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Đổi mật khẩu</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin mật khẩu</span>
                            <span class="caption-helper">thay đổi mật khẩu</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="password" class="form-control" id="LoginName" name="CurrentPassword" value="" placeholder="nhập mật khẩu hiện tại">
                                        <label for="form_control_1">Mật khẩu hiện tại:</label>
                                        <span class="help-block">Nhập mật khẩu đang dùng</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="password" class="form-control" id="NewPassword" name="NewPassword" value="" />
                                        <label for="form_control_1">Nhập mật khẩu mới:</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" value="" />
                                        <label for="form_control_1">Nhập lại mật khẩu mới:</label>
                                        <span class="help-block">Xác nhận lại mật khẩu mới tạo</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        window.document.title = 'Cập nhật mật khẩu tài khoản quản trị for MEDO | Power by IMEXsoft®';

    </script>
</form>
