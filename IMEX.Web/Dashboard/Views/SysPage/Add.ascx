﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysPageModel;
    var item = ViewBag.Data as SysPageEntity;

    var listTemplate = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 0)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listTemplateMobile = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 1)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listTemplateTablet = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 2)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listModule = IMEX.Lib.Web.Application.Modules.Where(o => o.IsControl == false && o.HideOrShow == false).OrderBy(o => o.Order).ToList();

    var listParent = IMEX.Lib.Global.ListItem.List.GetList(SysPageService.Instance, model.LangID);

    if (model.RecordID > 0)
    {
        //loai bo danh muc con cua danh muc hien tai
        listParent = IMEX.Lib.Global.ListItem.List.GetListForEdit(listParent, model.RecordID);
    }


%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">
    <input type="hidden" id="_imt_action" name="_imt_action" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật trang" : "Thêm mới trang"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Trang</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="<%=item.ParentID>0?"col-sm-9":"col-sm-12" %>">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin trang</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" autocomplete="off" placeholder="Nhập tên trang vào đây ...">
                                        <label for="form_control_1">Tên trang:</label>
                                        <span class="help-block">Ký tự tối đa 100</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" value="<%=item.Url %>" name="Url" autocomplete="off" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">URL trình duyệt:</label>
                                        <span class="help-block">ví dụ: san-pham-tai-nghe-cao-cap</span>
                                    </div>
                                </div>

                                <%if (model.ParentID > 0)
                                    {%>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Faicon" name="Faicon" value="<%=item.Faicon %>" autocomplete="off" placeholder="Mã html font awesome">
                                        <label for="form_control_1">Mã icon:</label>
                                        <span class="help-block">Ví dụ: fa-info-circle</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <select class="form-control" name="TemplateID">
                                            <option value="0"></option>
                                            <%for (var i = 0; listTemplate != null && i < listTemplate.Count; i++)
                                                { %>
                                            <option <%if (item.TemplateID == listTemplate[i].ID)
                                                {%>selected<%} %>
                                                value="<%= listTemplate[i].ID%>">&nbsp; <%= listTemplate[i].Name%></option>
                                            <%} %>
                                        </select>
                                        <label for="form_control_1">Mẫu giao diện PC:</label>
                                        <span class="help-block">Giao diện hiển thị trên PC</span>
                                    </div>
                                </div>

                                <%if (Setting.Sys_Mobile == true)
                                    { %>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <select class="form-control" name="TemplateMobileID">
                                            <option value="0"></option>
                                            <%for (var i = 0; listTemplateMobile != null && i < listTemplateMobile.Count; i++)
                                                { %>
                                            <option <%if (item.TemplateMobileID == listTemplateMobile[i].ID)
                                                {%>selected<%} %>
                                                value="<%= listTemplateMobile[i].ID%>">&nbsp; <%= listTemplateMobile[i].Name%></option>
                                            <%} %>
                                        </select>
                                        <label for="form_control_1">Mẫu giao diện Mobile:</label>
                                        <span class="help-block">Giao diện hiển thị trên Mobile</span>
                                    </div>
                                </div>
                                <%} %>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <select class="form-control" name="ModuleCode" id="ModuleCode" onchange="page_control_change(this.value)">
                                            <option value="0"></option>
                                            <%for (var i = 0; i < listModule.Count; i++)
                                                { %>
                                            <option <%if (item.ModuleCode == listModule[i].Code)
                                                {%>selected<%} %>
                                                value="<%= listModule[i].Code%>">&nbsp; <%= listModule[i].Name%></option>
                                            <%} %>
                                        </select>
                                        <label for="form_control_1">Chức năng:</label>
                                        <span class="help-block">VD: Chức năng hiển thị sản phẩm, tin tức...</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input " id="list_menu"></div>
                                </div>

                                <div class="col-md-6">
                                    <div id="control_param"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input ">
                                        <label for="multiple" class="control-label">Trang cha:</label>
                                        <select class="form-control chosen" name="ParentID">
                                            <option value="0">Chọn trang cha</option>
                                            <%for (var i = 0; listParent != null && i < listParent.Count; i++)
                                                { %>
                                            <option <%if (item.ParentID.ToString() == listParent[i].Value)
                                                {%>selected<%} %>
                                                value="<%= listParent[i].Value%>">&nbsp; <%= listParent[i].Name%></option>
                                            <%} %>
                                        </select>
                                    </div>
                                </div>
                                <%} %>



                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt về trang"><%=item.Summary %></textarea>
                                        <label for="form_control_1">Mô tả tóm tắt:</label>
                                        <span class="help-block">Mô tả ngắn về trang</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" id="Custom" name="Custom"><%=item.Custom %></textarea>
                                        <label for="form_control_1">Mã thiết kế:</label>
                                        <span class="help-block">Mã thiết kế tự động sinh</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control ckeditor" rows="3" id="Content" name="Content"><%=item.Content %></textarea>
                                        <label for="form_control_1">Nội dung:</label>
                                        <span class="help-block">Mô tả nội dung trang</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%if (item.ParentID > 0)
                { %>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh cho trang</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính cho trang</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-checkboxes">
                                    <label>Vị trí </label>
                                    <div class="md-checkbox-inline">
                                        <%= Utils.ShowCheckBoxByConfigkey("Mod.PageState", "ArrState", item.State)%>
                                    </div>
                                </div>
                            </div>
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Activity" value="1" class="md-radiobtn" <%=item.Activity ? "checked" : "" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Activity" value="0" class="md-radiobtn" <%=!item.Activity ? "checked" : "" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">SEO Customer</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" autocomplete="off" id="PageTitle" name="PageTitle" value="<%=item.PageTitle %>" placeholder="nhập vào đây ...">
                                    <label for="form_control_1">PageTitle:</label>
                                    <span class="help-block">Ký tự tối đa: 200</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" autocomplete="off" id="PageHeading" name="PageHeading" value="<%=item.PageHeading %>" placeholder="Tiêu đề trang thẻ H1 ...">
                                    <label for="form_control_1">PageHeading (H1):</label>
                                    <span class="help-block">Ký tự tối đa: 200</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input">
                                    <textarea class="form-control" rows="3" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 300 ký tự"><%=item.PageDescription%></textarea>
                                    <label for="form_control_1">Description:</label>
                                    <span class="help-block">Ký tự tối đa: 300</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" autocomplete="off" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                    <label for="form_control_1">Keywords:</label>
                                    <span class="help-block">Ví dụ: Sản phẩm axx, giá rẻ...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>



    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        function menu_change(name) {
            var txtPageName = document.getElementById('Name');
            if (txtPageName.value === '') {
                var i = name.indexOf('---- ');
                if (i > -1)
                    txtPageName.value = name.substr(i + 5);
            }
        }

        function page_control_change(controlID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = 'LangID=<%= model.LangID%>&PageID=<%=item.ID %>&ModuleCode=' + controlID + '&rnd=' + ranNum;
            $.ajax({
                url: '/{CPPath}/Api/PageGetControl.aspx',
                type: 'get',
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    var listMenu = data.Params;
                    var listBrand = data.Js;

                    listMenu = '<select class="form-control chosen" name="MenuID" onchange="menu_change(this.options[this.selectedIndex].text)"><option value="0">Chuyên mục</option>' + listMenu + '</select>';
                    listBrand = '<select class="form-control" name="BrandID"><option value="0">Root</option>' + listBrand + '</select>';
                    listMenu += '<label for= "form_control_1" > Liên kết Chuyên mục:</label><span class="help-block">Chuyên mục chứa dữ liệu hiển thị trên trang</span>';
                    $('#control_param').html(content);
                    $('#list_menu').html(listMenu);
                    $('#list_brand').html(listBrand);
                    $('.chosen').chosen({
                        search_contains: true,
                        no_results_text: 'Không tìm thấy kết quả phù hợp'
                    });

                    if (controlID != 'MProduct')
                        $('.brand-block').hide();
                    else
                        $('.brand-block').show();

                    //window.setTimeout('CKEditorInstance()', 100);
                },
                error: function () { }
            });
        }

        if ('<%=item.ModuleCode%>' !== '') page_control_change('<%= item.ModuleCode %>');
        else if ($('#ModuleCode').val() !== '') page_control_change($('#ModuleCode').val());
    </script>

</form>
