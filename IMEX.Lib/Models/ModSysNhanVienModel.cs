﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModSysNhanVienEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public string UserID { get; set; }

        [DataInfo]
        public string UserName { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string UserCode { get; set; }

        [DataInfo]
        public string UserFull { get; set; }


        #endregion Autogen by RDV


    }

    public class ModSysNhanVienService : ServiceBase<ModSysNhanVienEntity>
    {
        #region Autogen by RDV

        private ModSysNhanVienService() : base("[Sys_User]", "UserID")
        {
            DBConfigKey = "DBConnectLIS";

        }

        private static ModSysNhanVienService _instance;
        public static ModSysNhanVienService Instance => _instance ?? (_instance = new ModSysNhanVienService());

        #endregion Autogen by RDV


    }
}