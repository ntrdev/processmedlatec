﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Aspose.Cells;
using IMT.Lib.Models;
using System.IO;

namespace IMT.Lib.Global
{
    public class Excel
    {
        public static void Export(List<List<object>> list, int start_row, string sourceFile, string exportFile)
        {
            if (list == null) return;

            Workbook workbook = new Workbook();
            workbook.Open(sourceFile);
            Cells cells = workbook.Worksheets[0].Cells;

            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    cells[start_row + i, j].PutValue(list[i][j]);
                }
            }

            workbook.Save(exportFile);
        }

        //public static int Import(string file, int menuID)
        //{
        //    if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return 0;

        //    var fstream = new FileStream(file, FileMode.Open);
        //    var workbook = new Workbook();
        //    workbook.Open(fstream);
        //    var worksheet = workbook.Worksheets[0];

        //    if (worksheet == null || worksheet.Cells.MaxRow < 1) return 0;

        //    int success = 0;

        //    for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
        //    {
        //        string name = worksheet.Cells[i, 0].StringValue.Trim();
        //        string model = worksheet.Cells[i, 1].StringValue.Trim();
        //        int price = CoreMr.Reddevil.Global.Convert.ToInt(worksheet.Cells[i, 2].StringValue.Trim().Replace(".", "").Replace(",", ""));
        //        string summary = worksheet.Cells[i, 3].StringValue.Trim();
        //        string content = worksheet.Cells[i, 4].StringValue.Trim();

        //        string code = Data.GetCode(name);

        //        try
        //        {
        //            var item = new ModProductEntity()
        //            {
        //                ID = 0,
        //                MenuID = menuID,
        //                Name = name,
        //                Url = code,
        //                Sku = model,
        //                Price = price,
        //                Content = content,
        //                Order = GetMaxOrder(menuID),
        //                Published = DateTime.Now,
        //                Updated = DateTime.Now,
        //                Activity = true
        //            };

        //            ModProductService.Instance.Save(item);

        //            //update url
        //            ModCleanURLService.Instance.InsertOrUpdate(item.Url, "Product", item.ID, item.MenuID, item.GetMenu().LangID);

        //            success++;
        //        }
        //        catch
        //        {
        //            continue;
        //        }
        //    }

        //    fstream.Close();
        //    fstream.Dispose();

        //    return success;
        //}

        //public static void ImportCity(string file, int langID)
        //{
        //    if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

        //    FileStream fstream = new FileStream(file, FileMode.Open);
        //    Workbook workbook = new Workbook();
        //    workbook.Open(fstream);
        //    Worksheet worksheet = workbook.Worksheets[0];

        //    if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

        //    for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
        //    {
        //        string _Name = worksheet.Cells[i, 0].StringValue.Trim();
        //        string _Code = Data.GetCode(_Name);

        //        var _item = WebMenuService.Instance.CreateQuery()
        //                                        .Where(o => o.Activity == true && o.Type == "City" && o.Url == _Code)
        //                                        .ToSingle_Cache();
        //        if (_item == null)
        //        {
        //            _item = new WebMenuEntity();

        //            _item.Name = _Name;
        //            _item.Url = _Code;
        //            _item.Type = "City";
        //            _item.CityCode = worksheet.Cells[i, 1].StringValue.Trim();

        //            _item.ParentID = 1;
        //            _item.LangID = langID;

        //            _item.Order = GetMaxOrder(langID, 1);
        //            _item.Activity = true;

        //            WebMenuService.Instance.Save(_item);

        //            _item.Levels = 1 + "-" + _item.ID;
        //            WebMenuService.Instance.Save(_item, o => new { o.Levels });
        //        }
        //    }

        //    fstream.Close();
        //    fstream.Dispose();
        //}

        //public static void ImportDistrict(string file, int langID)
        //{
        //    if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

        //    FileStream fstream = new FileStream(file, FileMode.Open);
        //    Workbook workbook = new Workbook();
        //    workbook.Open(fstream);
        //    Worksheet worksheet = workbook.Worksheets[0];

        //    if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

        //    for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
        //    {
        //        string _Name = worksheet.Cells[i, 2].StringValue.Trim();
        //        string _Code = Data.GetCode(_Name);
        //        string _CityCode = worksheet.Cells[i, 1].StringValue.Trim();

        //        var _item = WebMenuService.Instance.CreateQuery()
        //                            .Where(o => o.Activity == true && o.Type == "City" && o.Url == _Code)
        //                            .ToSingle_Cache();

        //        var parent = WebMenuService.Instance.CreateQuery()
        //                            .Where(o => o.Activity == true && o.Type == "City" && o.CityCode == _CityCode)
        //                            .ToSingle_Cache();

        //        if (_item == null)
        //        {
        //            _item = new WebMenuEntity();

        //            _item.Name = _Name;
        //            _item.Url = _Code;
        //            _item.Type = "City";

        //            _item.ParentID = parent != null ? parent.ID : 5;
        //            _item.LangID = langID;

        //            _item.Order = GetMaxOrder(langID, _item.ParentID);
        //            _item.Activity = true;

        //            WebMenuService.Instance.Save(_item);

        //            _item.Levels = parent.Levels + "-" + _item.ID;
        //            WebMenuService.Instance.Save(_item, o => new { o.Levels });
        //        }
        //    }

        //    fstream.Close();
        //    fstream.Dispose();
        //}

        //public static void ImportWard(string file, int langID)
        //{
        //    if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

        //    FileStream fstream = new FileStream(file, FileMode.Open);
        //    Workbook workbook = new Workbook();
        //    workbook.Open(fstream);
        //    Worksheet worksheet = workbook.Worksheets[0];

        //    if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

        //    for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
        //    {
        //        string _Name = worksheet.Cells[i, 2].StringValue.Trim();
        //        string _Code = Data.GetCode(_Name);
        //        string _CityCode = worksheet.Cells[i, 1].StringValue.Trim();

        //        string _NameItem = worksheet.Cells[i, 4].StringValue.Trim();
        //        string _CodeItem = Data.GetCode(_NameItem);

        //        var parent = WebMenuService.Instance.CreateQuery()
        //                          .Where(o => o.Activity == true && o.Type == "City" && o.CityCode == _CityCode)
        //                          .ToSingle_Cache();

        //        var _item = WebMenuService.Instance.CreateQuery()
        //                            .Where(o => o.Activity == true && o.Type == "City" && o.ParentID == parent.ID && o.Url==_Code)
        //                            .ToSingle_Cache();

        //        if (_item != null)
        //        {
        //            var _itemName = WebMenuService.Instance.CreateQuery()
        //                               .Where(o => o.Activity == true && o.Type == "City" && o.Url == _CodeItem && o.ParentID == _item.ID)
        //                               .ToSingle_Cache();





        //            if (_itemName == null)
        //            {
        //                _itemName = new WebMenuEntity();

        //                _itemName.Name = _NameItem;
        //                _itemName.Url = _CodeItem;
        //                _itemName.Type = "City";

        //                _itemName.ParentID = _item != null ? _item.ID : 1;
        //                _itemName.LangID = langID;

        //                _itemName.Order = GetMaxOrder(langID, _itemName.ParentID);
        //                _itemName.Activity = true;

        //                WebMenuService.Instance.Save(_itemName);

        //                _itemName.Levels = _item.Levels + "-" + _itemName.ID;
        //                WebMenuService.Instance.Save(_itemName, o => new { o.Levels });
        //            }
        //        }
        //    }

        //    fstream.Close();
        //    fstream.Dispose();
        //}

        //private static int GetMaxOrder(int langID, int parentID)
        //{
        //    return WebMenuService.Instance.CreateQuery()
        //                    .Where(o => o.LangID == langID && o.ParentID == parentID)
        //                    .Max(o => o.Order)
        //                    .ToValue().ToInt(0) + 1;
        //}

        private static int GetMaxOrder()
        {
            return ModMenuService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private static int GetMaxOrder2()
        {
            return ModMaterialService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        public static bool ImportExcelProduct(string fileInput)
        {
            if (!fileInput.EndsWith(".xls")) return false;

            FileStream fstream = new FileStream(fileInput, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return false;

            string _name = ""; ModMenuEntity _menu = null; //int _idmenu = 0;
            for (int i = worksheet.Cells.MinColumn + 1; i <= worksheet.Cells.MaxRow; i++)
            {

                if (worksheet.Cells[i, 0].IsMerged && !string.IsNullOrEmpty(worksheet.Cells[i, 0].StringValue))
                {
                    _name = worksheet.Cells[i, 0].StringValue;
                    string code = Data.VietHoa(_name).ToLower() + "-" + Data.GetCode(_name);
                    string ranid = Data.VietHoa(_name) + "-" + Global.Random.couponCode(3) + "-" + GetMaxOrder();
                    _menu = ModMenuService.Instance.CreateQuery().Where(o => o.Name == _name && o.Code == code).ToSingle();
                    if (_menu == null)
                    {
                        _menu = new ModMenuEntity();
                        _menu.Name = _name;
                        _menu.Code = code;
                        _menu.RanID = ranid;
                        _menu.Order = GetMaxOrder();
                        _menu.Updated = DateTime.Now;
                        _menu.Created = DateTime.Now;
                        _menu.UserCreated = CPLogin.UserID;
                        _menu.UserUpdated = CPLogin.UserID;

                        ModMenuService.Instance.Save(_menu);


                    }
                }
                else if (!worksheet.Cells[i, 0].IsMerged && !string.IsNullOrEmpty(worksheet.Cells[i, 0].StringValue))
                {
                    _name = worksheet.Cells[i, 0].StringValue;
                    string code = Data.VietHoa(_name).ToLower() + "-" + Data.GetCode(_name);
                    string ranid = Data.VietHoa(_name) + "-" + Global.Random.couponCode(3) + "-" + GetMaxOrder();
                    _menu = ModMenuService.Instance.CreateQuery().Where(o => o.Name == _name && o.Code == code).ToSingle();
                    if (_menu == null)
                    {
                        _menu = new ModMenuEntity();
                        _menu.Name = _name;
                        _menu.Code = code;
                        _menu.RanID = ranid;
                        _menu.Order = GetMaxOrder();
                        _menu.Updated = DateTime.Now;
                        _menu.Created = DateTime.Now;
                        _menu.UserCreated = CPLogin.UserID;
                        _menu.UserUpdated = CPLogin.UserID;

                        ModMenuService.Instance.Save(_menu);

                    }

                }

                if (!string.IsNullOrEmpty(_name) && _menu != null)
                {
                    string _material = worksheet.Cells[i, 1].StringValue;
                    string code1 = Data.VietHoa(_material).ToLower() + "-" + Data.GetCode(_material);
                    var Material = ModMaterialService.Instance.CreateQuery()
                                               .Where(o => o.Name == _material && o.Code == code1).ToSingle();
                    if (Material != null)
                    {
                        var _check = ModMenuDetailService.Instance.CreateQuery().Where(o => o.MenuID == _menu.ID && o.MaterialID == Material.ID).ToSingle();
                        if (_check == null)
                        {
                            _check = new ModMenuDetailEntity();
                            _check.MenuID = _menu.ID;
                            _check.MaterialID = Material.ID;
                            _check.Name = Material.Name;
                            _check.PriceMaterial = Material.Price;
                            _check.UnitMaterial = Material.Unit;
                            _check.Type = 2;
                            ModMenuDetailService.Instance.Save(_check);
                        }
                    }
                    else
                    {
                        int price = worksheet.Cells[i, 2].IntValue;
                        string unit = worksheet.Cells[i, 3].StringValue;

                        Material = new ModMaterialEntity();
                        Material.Name = _material;
                        Material.UserUpdated = CPLogin.CurrentUser.ID;
                        Material.Updated = DateTime.Now;
                        Material.Created = DateTime.Now;
                        Material.UserCreated = CPLogin.CurrentUser.ID;
                        Material.Order = GetMaxOrder2();
                        Material.RanID = Data.VietHoa(Material.Name) + "-" + Global.Random.couponCode(3) + "-" + Material.Order;
                        Material.Code = Data.VietHoa(_material).ToLower() + "-" + Data.GetCode(_material);
                        Material.Price = price;
                        Material.Unit = unit;
                        ModMaterialService.Instance.Save(Material);


                        var _check = ModMenuDetailService.Instance.CreateQuery().Where(o => o.MenuID == _menu.ID && o.MaterialID == Material.ID).ToSingle();
                        if (_check == null)
                        {
                            _check = new ModMenuDetailEntity();
                            _check.MenuID = _menu.ID;
                            _check.MaterialID = Material.ID;
                            _check.Name = Material.Name;
                            _check.PriceMaterial = Material.Price;
                            _check.UnitMaterial = Material.Unit;
                            _check.Type = 2;
                            ModMenuDetailService.Instance.Save(_check);
                        }
                    }

                }



                //var _material = new ModMaterialEntity();
                //_material.Name = _Name;
                //_material.UserUpdated = CPLogin.CurrentUser.ID;
                //_material.Updated = DateTime.Now;
                //_material.Created = DateTime.Now;
                //_material.UserCreated = CPLogin.CurrentUser.ID;
                //_material.Order = GetMaxOrder();
                //_material.RanID = Data.VietHoa(_material.Name) + "-" + Global.Random.couponCode(3) + "-" + _material.Order;
                //_material.Code = Data.VietHoa(_Name).ToLower() + "-" + Data.GetCode(_Name);
                //_material.Price = _Price;
                //_material.Unit = _unit;

                //ModMaterialService.Instance.Save(_material);

            }
            fstream.Close();
            fstream.Dispose();

            return true;
        }

    }
}
