﻿using IMEX.Core.Interface;
using IMEX.Core.Models;

namespace IMEX.Lib.Models
{
    public class SysLangEntity : EntityBase, ILangInterface
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        #endregion Autogen by RDV
    }

    public class SysLangService : ServiceBase<SysLangEntity>, ILangServiceInterface
    {
        #region Autogen by RDV

        public SysLangService() : base("[Sys_Lang]")
        {
        }

        private static SysLangService _instance;

        public static SysLangService Instance => _instance ?? (_instance = new SysLangService());

        #endregion Autogen by RDV

        public SysLangEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        #region ILangServiceInterface Members

        public ILangInterface CoreMr_Reddevil_GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        #endregion ILangServiceInterface Members
    }
}