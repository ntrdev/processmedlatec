
CKEDITOR.editorConfig = function (config) {
	//config.skin = 'n1theme';
	config.language = 'vi';
	config.height = 500;
	config.toolbarCanCollapse = true;
	config.removeButtons = 'Flash,About';
	config.allowedContent = true;
	config.fullPage = true;
	config.removePlugins = 'htmldataprocessor';
};
