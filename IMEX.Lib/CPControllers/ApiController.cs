﻿using System;
using System.Collections.Generic;
using IMEX.Core.Global;
using IMEX.Core.MVC;
using Newtonsoft.Json;
using IMEX.Lib.Global;
using IMEX.Lib.Global.ListItem;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;
using Error = IMEX.Lib.Global.Error;
using Setting = IMEX.Core.Web.Setting;
using System.Linq;
using Aspose.Cells;
using System.Threading.Tasks;

namespace IMEX.Lib.CPControllers
{
    public class ApiController : CPController
    {

        private CPUserEntity _oUser = null;
        private CPUserEntity User
        {
            get
            {
                if (_oUser == null && CPLogin.IsLogin())
                    _oUser = CPLogin.CurrentUser;

                return _oUser;
            }
        }

        public void ActionIndex() { }
        public void ActionGetChild(GetChildModel model)
        {
            var json = new Json();

            var listItem = WebMenuService.Instance.GetByParentID_Cache(model.ParentID);

            json.Instance.Html = @"<option value=""0"" selected=""selected"">- chọn -</option>";
            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<option value=""" + listItem[i].ID + @""" " + (listItem[i].ID == model.SelectedID ? @"selected=""selected""" : @"") + @">" + listItem[i].Name + @"</option>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionSiteGetPage(SiteGetPageModel model)
        {


            var json = new Json();

            var listPage = List.GetList(SysPageService.Instance, model.LangID);

            for (var i = 0; listPage != null && i < listPage.Count; i++)
            {
                json.Instance.Params += "<option " + (model.PageID.ToString() == listPage[i].Value ? "selected" : "") + " value='" + listPage[i].Value + "'>&nbsp; " + listPage[i].Name + "</option>";
            }

            json.Create();
        }

        public void ActionTemplateGetCustom(int templateID)
        {
            var json = new Json();

            json.Instance.Html = SysTemplateService.Instance.GetByID(templateID).Custom;

            json.Create();
        }

        public void ActionPageGetCustom(int pageID)
        {
            var json = new Json();

            json.Instance.Html = SysPageService.Instance.GetByID(pageID).Custom;

            json.Create();
        }

        public void ActionPageGetControl(PageGetControlModel model)
        {
            var json = new Json();
            try
            {
                if (!string.IsNullOrEmpty(model.ModuleCode))
                {
                    SysPageEntity currentPage = null;
                    var currentModule = SysModuleService.Instance.CoreMr_Reddevil_GetByCode(model.ModuleCode);

                    if (model.PageID > 0)
                        currentPage = SysPageService.Instance.GetByID(model.PageID);

                    if (currentModule != null)
                    {
                        var currentObject = new Class(currentModule.ModuleType);

                        var filePath = (System.IO.File.Exists(CPViewPage.Server.MapPath("~/Views/Design/" + currentModule.Code + ".ascx")) ?
                            "~/Views/Design/" + currentModule.Code + ".ascx" : "~/" + Setting.Sys_AdminDir + "/Design/EditModule.ascx");

                        var sHtml = Core.Web.Utils.GetHtmlControl(CPViewPage, filePath,
                                            "CurrentObject", currentObject,
                                            "CurrentPage", currentPage,
                                            "CurrentModule", currentModule,
                                            "LangID", model.LangID);

                        if (currentObject.ExistsField("MenuID"))
                        {
                            var fieldInfo = currentObject.GetFieldInfo("MenuID");
                            var attributes = fieldInfo.GetCustomAttributes(typeof(PropertyInfo), true);
                            if (attributes.GetLength(0) > 0)
                            {
                                var propertyInfo = (PropertyInfo)attributes[0];
                                var listMenu = List.GetListByText(propertyInfo.Value.ToString());

                                var menuType = List.FindByName(listMenu, "Type").Value;

                                listMenu = List.GetList(WebMenuService.Instance, model.LangID, menuType);
                                listMenu.Insert(0, new Item(string.Empty, string.Empty));

                                for (var j = 1; j < listMenu.Count; j++)
                                {
                                    if (string.IsNullOrEmpty(listMenu[j].Name)) continue;

                                    json.Instance.Params += "<option " + (currentPage != null && currentPage.MenuID.ToString() == listMenu[j].Value ? "selected" : "") + " value='" + listMenu[j].Value + "'>&nbsp; " + listMenu[j].Name + "</option>";
                                }
                            }
                        }


                        json.Instance.Html = sHtml.Replace("{CPPath}", CPViewPage.CPPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
            }

            json.Create();
        }


    }

    public class GetChildModel
    {
        public int ParentID { get; set; }
        public int SelectedID { get; set; }
    }

    public class PageGetControlModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
        public string ModuleCode { get; set; }
    }

    public class SiteGetPageModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
    }
}

