﻿using System.Reflection;
using System.Runtime.InteropServices;


[assembly: AssemblyTitle("Library IMEXsoft")]
[assembly: AssemblyDescription("Code by vantruong2412@live.com || Version: 2.8")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("IMEXsoft., JSC || imexsoft.net")]
[assembly: AssemblyProduct("IMEX.Lib")]
[assembly: AssemblyCopyright("Copyright © 2020 vantruong2412@live.com")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("8906822c-656d-2221-8ae9-892630611268")]
[assembly: AssemblyVersion("2.8.0.0")]
[assembly: AssemblyFileVersion("2.8.0.0")]
