﻿using IMEX.Core.Models;
using System;

namespace IMEX.Lib.Models
{
    public class ModKPIEmployeeEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int EmployeeID { get; set; }

        [DataInfo]
        public string Manhanvien { get; set; }

        [DataInfo]
        public string Hoten { get; set; }

        [DataInfo]
        public DateTime NgaySinh { get; set; }

        [DataInfo]
        public string DataType { get; set; }

        [DataInfo]
        public byte[] Anh { get; set; }
        #endregion Autogen by RDV


    }

    public class ModKPIEmployeeService : ServiceBase<ModKPIEmployeeEntity>
    {
        #region Autogen by RDV

        private ModKPIEmployeeService() : base("[KPI_Employee]", "EmployeeID")
        {
            DBConfigKey = "DBConnectFinger";

        }

        private static ModKPIEmployeeService _instance;
        public static ModKPIEmployeeService Instance => _instance ?? (_instance = new ModKPIEmployeeService());

        #endregion Autogen by RDV


    }
}