﻿using System;
using System.Collections.Generic;
using IMEX.Lib.Global;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;

namespace IMEX.Lib.CPControllers
{
    [CPModuleInfo(Name = "Trang (Menu)",
       Description = "Quản lý - Trang (Menu)",
       Code = "SysPage",
       Access = 31,
       Order = 1,
       ShowInMenu = true,
       CssClass = "icon-16-article", Partitioning = 3)]
    public class SysPageController : CPController
    {

        public SysPageController()
        {
            //khoi tao Service
            DataService = SysPageService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(SysPageModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort, "[Order]");

            //tao danh sach
            var dbQuery = SysPageService.Instance.CreateQuery()
                                    .Where(o => o.ParentID == model.ParentID && o.LangID == model.LangID)
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(SysPageModel model)
        {
            if (model.RecordID > 0)
            {
                _item = SysPageService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                _item = new SysPageEntity
                {
                    ParentID = model.ParentID,
                    LangID = model.LangID,
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(model),
                    Activity = true
                };

                //khoi tao gia tri mac dinh khi insert
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(SysPageModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(SysPageModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(SysPageModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public void ActionShowMenuTop(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Approve)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }

            var product = SysPageService.Instance.GetByID(arrID[0]);
            if (product == null) return;

            product.ShowMenuTop = Core.Global.Convert.ToBool(arrID[1]);
            SysPageService.Instance.Save(product, o => o.ShowMenuTop);

            //thong bao
            CPViewPage.SetMessage(arrID[1] == 0 ? "Đã bỏ hiển thị MenuTop." : "Đã hiển thị Menu Top");
            CPViewPage.RefreshPage();
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }

            var list = new List<int>();
            GetPageIDChildForDelete(ref list, arrID);
            if (list.Count > 1)
            {
                CPViewPage.Alert("Menu bạn xóa còn chứa Dữ liệu. Hãy kiểm tra kĩ trước khi xóa.");
                return;
            }


            foreach (var pageId in list)
            {
                //xoa cleanurl
                var id = pageId;
                var _page = SysPageService.Instance.CreateQuery().Select(o => o.MenuID).Where(o => o.ID == id).ToSingle();
                var sWhere = "[MenuID]=" +(_page != null ? _page.MenuID : 0) + ""; 
                var _Site = SysSiteService.Instance.CreateQuery().Select(o => o.ID).Where(o => o.PageID == id).Count().ToValue().ToBool();
                if (_Site)
                {
                    CPViewPage.Alert("Đây là trang mặc định của website, không thể xóa. Xóa sẽ gây lỗi website");
                    return;
                }
                if (!string.IsNullOrEmpty(_page.Content))
                {
                    CPViewPage.Alert("Danh mục bạn xóa còn chứa Bài viết. Hãy xóa hết Bài viết trong chuyên mục muốn xóa trước.");
                    return;
                }
               
                ModCleanURLService.Instance.Delete(o => o.Type == "Page" && o.Value == id);
                //xoa Page
                SysPageService.Instance.Delete(pageId);
            }

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        public void ActionUpload(SysPageModel model)
        {
            CPViewPage.Script("Redirect", "REDDEVILRedirect('Import')");
        }

        public void ActionImport(SysPageModel model)
        {
            ViewBag.Model = model;
        }

        public override void ActionCancel()
        {
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx")
                .Replace("Import.aspx", "Index.aspx"));
        }

        #region private func

        private SysPageEntity _item;

        private bool ValidSave(SysPageModel model)
        {
            if (!string.IsNullOrEmpty(model.Value))
            {
                foreach (var t in model.Value.Split('\n'))
                {
                    if (string.IsNullOrEmpty(t.Trim()) || t.StartsWith("//"))
                        continue;

                    _item = new SysPageEntity { Name = t.Trim(), Url = Data.GetCode(t.Trim()) };

                    //khoi tao gia tri mac dinh khi insert

                    var menu = WebMenuService.Instance.CreateQuery()
                                        .Where(o => o.Url == _item.Url)
                                        .ToSingle();

                    if (menu != null)
                    {
                        _item.MenuID = menu.ID;
                        _item.ModuleCode = "M" + (string.IsNullOrEmpty(model.Type) ? menu.Type : model.Type);

                        var template = SysTemplateService.Instance.CreateQuery()
                                                    .Where(o => o.Name == menu.Type && o.LangID == menu.LangID)
                                                    .ToSingle() ?? SysTemplateService.Instance.CreateQuery().Where(o => o.LangID == menu.LangID).Take(1).ToSingle();

                        if (template != null) _item.TemplateID = template.ID;
                    }

                    _item.ParentID = model.ParentID;
                    _item.LangID = model.LangID;
                    _item.TemplateID = model.TeamplateID;
                    _item.Created = DateTime.Now;
                    _item.Updated = DateTime.Now;
                    _item.Order = GetMaxOrder(model);
                    _item.Activity = true;
                    _item.Levels = _item.Parent.Levels + "-" + _item.ID;

                    SysPageService.Instance.Save(_item);

                    //update url
                    ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "Page", _item.ID, _item.MenuID, model.LangID);
                }

                return true;
            }

            TryUpdateModel(_item);

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tên trang.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "Page", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //neu code khong duoc nhap -> tu dong tao ra khi them moi
            if (_item.Url == string.Empty)
            {
                _item.Url = Data.GetCode(_item.Name);
            }

            //neu di chuyen thi cap nhat lai Order
            if (model.RecordID > 0 && _item.ParentID != model.ParentID)
                _item.Order = GetMaxOrder(model);

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                if (_item.ParentID > 0 && _item.ID > 0)
                    _item.Levels = !string.IsNullOrEmpty(_item.Parent.Levels) ? _item.Parent.Levels + "-" + _item.ID : _item.ID.ToString();
                else if (_item.ID < 1)
                {
                    SysPageService.Instance.Save(_item);
                    _item.Levels = !string.IsNullOrEmpty(_item.Parent.Levels) ? _item.Parent.Levels + "-" + _item.ID : _item.ID.ToString();
                }
                //save
                SysPageService.Instance.Save(_item);

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "Page", _item.ID, _item.MenuID, model.LangID);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder(SysPageModel model)
        {
            return SysPageService.Instance.CreateQuery()
                    .Where(o => o.LangID == model.LangID && o.ParentID == model.ParentID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private static void GetPageIDChildForDelete(ref List<int> list, int[] arrId)
        {
            for (var i = 0; arrId != null && i < arrId.Length; i++)
            {
                GetPageIDChild(ref list, arrId[i]);
            }
        }

        private static void GetPageIDChild(ref List<int> list, int pageId)
        {
            list.Add(pageId);

            var listPage = SysPageService.Instance.CreateQuery()
                                                .Where(o => o.ParentID == pageId)
                                                .ToList();

            for (var i = 0; listPage != null && i < listPage.Count; i++)
            {
                GetPageIDChild(ref list, listPage[i].ID);
            }
        }

        #endregion private func
    }

    public class SysPageModel : DefaultModel
    {
        public int ParentID { get; set; }
        public int TeamplateID { get; set; }

        public int LangID { get; set; } = 1;

        public int State { get; set; }
        public List<int> ArrState { get; set; }

        public string Type { get; set; }
        public string Value { get; set; }
    }
}