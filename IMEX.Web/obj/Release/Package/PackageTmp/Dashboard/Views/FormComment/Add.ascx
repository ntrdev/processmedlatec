﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as FormCommentModel;
    var item = ViewBag.Data as ModNewsCommentEntity;




%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">
    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />
    <input type="hidden" id="ModuleCode" value="<%=CPViewPage.CurrentModule.Code %>" />


    <div class="page-content" >
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật comment" : "Thêm mới Comment"%></h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin bình luận</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="NameUser" name="NameUser" value="<%=item.NameUser %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên Khách hàng:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled value="<%=Utils.FormatDateTime2(item.DateEdit) %>">
                                        <label for="form_control_1">Cập nhật nội dung gần nhất:</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled value="<%=Utils.FormatDateTime2(item.DateCreate) %>">
                                        <label for="form_control_1">Ngày bình luận:</label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" name="Comment" placeholder="Mô tả tóm tắt"><%=item.Comment %></textarea>
                                        <label for="form_control_1">Nội dung bình luận:</label>
                                        <span class="help-block">Mô tả của khách hàng</span>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính Khách hàng</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">

                           
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                          
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Active" value="1" class="md-radiobtn" <%=item.Active?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Active" value="0" class="md-radiobtn" <%=!item.Active?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">

        window.document.title = 'Quản trị bình luận của Khách hàng MEDO | Power by IMEXsoft®';




    </script>

</form>
