﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<script runat="server">

    List<CPRoleEntity> listRole;
    SysUserModel model;
    CPUserEntity item;
    protected void Page_Load(object sender, EventArgs e)
    {
        model = ViewBag.Model as SysUserModel;
        item = ViewBag.Data as CPUserEntity;

        listRole = CPRoleService.Instance.CreateQuery().ToList();
    }

    bool HasRole(int roleID)
    {
        if (item.ID == 0)
            return false;

        return item.GetRole().Find(o => o.ID == roleID) != null;
    }
</script>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="ModuleCode" value="<%=CPViewPage.CurrentModule.Code %>" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật tài khoản người dùng" : "Thêm mới"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Người dùng</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin sản phẩm</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="LoginName" <%=item.ID>0?"disabled=\"disabled\"":"" %> name="LoginName" value="<%=item.LoginName%>" placeholder="Tên đăng nhập phải không dấu, và không có ký tự đặc biệt (&,^,*,#)">
                                        <label for="form_control_1">Tên đăng nhập:</label>
                                        <span class="help-block">Ví dụ: trangdt</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="password" class="form-control" id="Password" name="NewPassword" value="" />
                                        <label for="form_control_1">Mật khẩu:</label>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Họ và tên:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="EmployeeCode" name="EmployeeCode" value="<%=item.EmployeeCode %>" placeholder="Mã Nhân Viên">
                                        <label for="form_control_1">Mã Nhân Viên:</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Phone" name="Phone" value="<%=item.Phone %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Số điện thoại:</label>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Email" name="Email" value="<%=item.Email %>" placeholder="Nhập email">
                                        <label for="form_control_1">Email:</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Address" name="Address" value="<%=item.Address %>" placeholder="Địa chỉ">
                                        <label for="form_control_1">Địa chỉ:</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Ảnh đại diện</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage("/data/upload/images"+item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-checkboxes">
                                    <label>Thuộc nhóm admin?</label>
                                    <%for (var i = 0; listRole != null && i < listRole.Count; i++)
                                        {%>

                                    <div class="md-radio-inline">

                                        <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Chọn Quyền <%=listRole[i].Name %>">
                                            <input type="checkbox" id="listRole<%=listRole[i].ID %>" name="ArrRole" <%if (HasRole(listRole[i].ID))
                                                {%>
                                                checked="checked" <%} %> value="<%= listRole[i].ID%>" class="md-check">
                                            <label for="listRole<%=listRole[i].ID %>">
                                                <%= listRole[i].Name%>
                                                <span style="left: -15px;"></span>
                                                <span style="left: 5px;" class="check"></span>
                                                <span style="left: 0;" class="box"></span>

                                            </label>
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Active" value="1" class="md-radiobtn" <%=item.Active?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Active" value="0" class="md-radiobtn" <%=!item.Active?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        window.document.title = 'Quản trị tài khoản quản trị for MEDO | Power by IMEXsoft®';
    </script>



</form>
