﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysTemplateModel;
    var listItem = ViewBag.Data as List<SysTemplateEntity>;
%>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý mẫu giao diện</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Mẫu giao diện</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetSortListCommand()%>
                    </div>
                    <div class="row " style="margin-top:10px">
                        <div class="col-md-1 col-sm-12">
                            <div class="md-checkbox" data-toggle="tooltip" data-placement="bottom" data-original-title="Click để chọn tất cả">
                                <input type="checkbox" data-toggle="tooltip" data-original-title="Click để chọn tất cả"  id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                <label for="checks">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-12">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-3 text-right pull-right">
                                <div class="table-group-actions d-inline-block">
                                    <%= ShowDDLLang(model.LangID)%>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row manager_page">
                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                        { %>
                    <div class="col-md-3">
                        <button type="button" class="btn blue-hoki btn-outline sbold uppercase btn-block btn-lg">
                            <%= GetCheckbox(listItem[i].ID, i)%> <br />
                            Giao diện  <%=listItem[i].Name %>
                            <span><b>Thiết bị hiển thị: </b><%= listItem[i].DeviceText %></span>
                            <span><b>File Hiển Thị: </b><%= listItem[i].File %></span><br />
                            <p data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" class="btn waves-effect waves-light green  btn-icon"><i class="icon-pencil"></i></p>
                            <p class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Xóa" type="button" onclick="setcheck_delete('<%=i %>');"><i class="fa fa-trash"></i></p>
                        </button>

                    </div>
                    <%} %>
                </div>
                <div class="row center">
                    <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                        <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        window.document.title = 'Quản lý Mẫu giao diện' + " | trình quản trị IMEXsoft.net ";


        var REDDEVILController = "SysTemplate";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>

</form>
