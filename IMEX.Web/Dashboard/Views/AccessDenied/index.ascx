﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<link href="/{CPPath}/interface/css/login-4.min.css" rel="stylesheet" type="text/css" />

<div class="content">
    <div class="page" style="min-height: 630px; padding-top: 68px">
        <div class="auth-box card">
            <div class="card-block">
                
                <div class="copyright  text-center" style="color:#fff">
                    <h1><i class="icon-bell"></i>Oops!</h1>
                    <h2>Access denied!</h2>
                    <p class=" text-center p-b-5">Xin lỗi, bạn không có quyền truy cập chức năng này. Liên hệ Administrator để biết thêm chi tiết.</p>
                </div>
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="text-center">
                            <img src="/{CPPath}/interface/images/logo.png" alt="IMEXsoft" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/{CPPath}/interface/plugins/jquery.validate.min.js" type="text/javascript"></script>
<script src="/{CPPath}/interface/plugins/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="/{CPPath}/interface/plugins/login-4.min.js" type="text/javascript"></script>
