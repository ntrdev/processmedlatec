﻿using System;
using System.Collections.Generic;
using IMEX.Core.Interface;
using IMEX.Core.Models;
using IMEX.Core.MVC;
using IMEX.Core.Web;

namespace IMEX.Lib.Models
{
    public class SysPageEntity : EntityBase, IPageInterface
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int TemplateID { get; set; }

        [DataInfo]
        public int TemplateMobileID { get; set; }

        [DataInfo]
        public int TemplateTabletID { get; set; }

        [DataInfo]
        public string ModuleCode { get; set; }

        [DataInfo]
        public string Levels { get; set; }

        [DataInfo]
        public int LangID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }


        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string Faicon { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string TopContent { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string LinkTitle { get; set; }

        [DataInfo]
        public string PageHeading { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public string PageCanonical { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public bool ShowMenuTop { get; set; }

        #endregion Autogen by RDV

        #region SEO

        public bool PageState { get; set; } = true;

        public string PageURL { get; set; } = string.Empty;

        private string _pageFile = string.Empty;

        public string PageFile
        {
            get => _pageFile.IndexOf("base64", StringComparison.OrdinalIgnoreCase) > -1 ? string.Empty : _pageFile;
            set => _pageFile = value;
        }



        #endregion SEO

        public bool HasEnd { get; set; }

        public bool Root => ParentID == 0;

        public bool End => Items.GetValue("End").ToBool();

        private string _oBrand = string.Empty;
        public string Brand
        {
            get
            {
                if (string.IsNullOrEmpty(_oBrand))
                {
                    var ArrName = Name.Split(' ');
                    _oBrand = ArrName[ArrName.Length - 1];
                }

                return _oBrand;
            }
        }


        private SysPageEntity _oParent;
        public SysPageEntity Parent
        {
            get
            {
                if (_oParent == null)
                    _oParent = SysPageService.Instance.GetByID_Cache(ParentID);

                return _oParent ?? (_oParent = new SysPageEntity());
            }
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity Menu
        {
            get
            {
                if (_oMenu == null && MenuID > 0)
                    _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

                return _oMenu ?? (_oMenu = new WebMenuEntity());
            }
        }


    }

    public class SysPageService : ServiceBase<SysPageEntity>, IPageServiceInterface
    {
        #region Autogen by RDV

        public SysPageService() : base("[Sys_Page]")
        {
        }

        private static SysPageService _instance;

        public static SysPageService Instance => _instance ?? (_instance = new SysPageService());

        #endregion Autogen by RDV

        public SysPageEntity GetByID(int id)
        {
            return CreateQuery().Where(o => o.ID == id).ToSingle();
        }

        public SysPageEntity GetByCode(string code)
        {
            return CreateQuery().Where(o => o.Url == code).ToSingle();
        }

        public SysPageEntity GetByID_Cache(int id)
        {
            if (GetAll_Cache() == null) return null;

            return GetAll_Cache().Find(o => o.ID == id);
        }

        public List<SysPageEntity> GetByParent_Cache(int parentID)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID);

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public List<SysPageEntity> GetByParent_Cache(int parentID, bool show)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID && o.ShowMenuTop == show);

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public List<SysPageEntity> GetByGrandParent_Cache(int parentID)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID);

            var listGrand = new List<SysPageEntity>();
            foreach (var t in list)
            {
                var temp = GetAll_Cache().FindAll(o => o.ParentID == t.ID);
                listGrand.AddRange(temp);
            }

            if (listGrand.Count > 0)
                listGrand.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return listGrand;
        }

        public List<SysPageEntity> GetByParent_Cache(int parentID, string module)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID && o.ModuleCode == module);

            if (list.Count == 0) return null;

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public string GetMapCode_Cache(SysPageEntity page)
        {
            var keyCache = string.Concat("[Sys_Page]", ".GetMapCode_Cache." + page.ID);

            string mapCode;
            var obj = Cache.GetValue(keyCache);
            if (obj != null)
            {
                mapCode = obj.ToString();
            }
            else
            {
                var tempPage = page;

                mapCode = tempPage.Url;
                while (tempPage.ParentID > 0)
                {
                    var parentId = tempPage.ParentID;

                    tempPage = CreateQuery()
                           .Where(o => o.ID == parentId)
                           .ToSingle_Cache();

                    if (tempPage == null || tempPage.Root)
                        break;

                    mapCode = tempPage.Url + "/" + mapCode;
                }

                Cache.SetValue(keyCache, mapCode);
            }

            return mapCode;
        }

        private List<SysPageEntity> GetAll_Cache()
        {
            return CreateQuery().Where(o => o.Activity == true).ToList_Cache();
        }

        #region IPageServiceInterface Members

        public IPageInterface CoreMr_Reddevil_GetByID(int id)
        {
            return GetAll_Cache().Find(o => o.ID == id);
        }

        public IPageInterface CoreMr_Reddevil_CurrentPage(ViewPage viewPage)
        {
            var code = viewPage.CurrentVQS.GetString(0);

            if (code == string.Empty || code.Length > 260) return null;

            if (string.Equals(code.ToLower(), "api", StringComparison.OrdinalIgnoreCase) || code.StartsWith("sitemap", StringComparison.OrdinalIgnoreCase) || string.Equals(code, "rss", StringComparison.OrdinalIgnoreCase))
            {
                if (code.ToLower().StartsWith("sitemap") && viewPage.CurrentVQS.Count > 1)
                    HttpRequest.Redirect301(viewPage.GetURL(code));

                var template = SysTemplateService.Instance.CreateQuery()
                                            .Select(o => o.ID)
                                            .ToSingle_Cache();

                if (template == null) return null;

                viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, 1);

                if (code.StartsWith("sitemap", StringComparison.OrdinalIgnoreCase))
                {
                    return new SysPageEntity
                    {
                        TemplateID = template.ID,
                        TemplateMobileID = template.ID,
                        Name = "Sitemap",
                        Url = "sitemap",
                        ModuleCode = "MSitemap",
                        Activity = true
                    };
                }

                switch (code.ToLower())
                {
                    case "api":
                        return new SysPageEntity
                        {
                            TemplateID = template.ID,
                            TemplateMobileID = template.ID,
                            Name = "",
                            Url = "api",
                            ModuleCode = "MApi",
                            Activity = true
                        };

                    case "rss":
                        return new SysPageEntity
                        {
                            TemplateID = template.ID,
                            TemplateMobileID = template.ID,
                            Name = "Đọc tin RSS",
                            Url = "rss",
                            ModuleCode = "MRss",
                            Activity = true
                        };

                }
            }
          

            var cleanUrl = ModCleanURLService.Instance.GetByUrl(code, viewPage.CurrentLang.ID);
            if (cleanUrl == null)
            {
                cleanUrl = ModCleanURLService.Instance.CreateQuery().Where(o => o.Url.StartsWith(code) && o.LangID == viewPage.CurrentLang.ID).ToSingle();
                if (cleanUrl != null)
                    HttpRequest.Redirect301(viewPage.GetURL(cleanUrl.Url));

                return null;
            }

            if (viewPage.CurrentVQS.Count > 1 && cleanUrl.MenuID > 0)
                HttpRequest.Redirect301(viewPage.GetURL(viewPage.CurrentVQS.EndCode));

            viewPage.ViewBag.CleanURL = cleanUrl;

            if (cleanUrl.Type == "Page")
            {
                viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, 1);
                return GetByID_Cache(cleanUrl.Value);
            }


            viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, viewPage.CurrentVQS.Count - 1);

            SysPageEntity page = null;
            var menuId = cleanUrl.MenuID;
            while (menuId > 0)
            {
                var id = menuId;
                page = CreateQuery().Where(o => o.MenuID == id && o.Activity == true).ToSingle_Cache();

                if (page != null) break;

                var menu = WebMenuService.Instance.GetByID_Cache(menuId);

                if (menu == null || menu.ParentID == 0) break;

                menuId = menu.ParentID;
            }

            return page;
        }


        public void CoreMr_Reddevil_CPSave(IPageInterface item)
        {
            Save(item as SysPageEntity);
        }

        #endregion IPageServiceInterface Members
    }
}