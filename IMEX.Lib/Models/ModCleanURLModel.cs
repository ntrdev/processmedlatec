﻿using IMEX.Core.Models;

namespace IMEX.Lib.Models
{
    public class ModCleanURLEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string Type { get; set; }

        [DataInfo]
        public int Value { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int LangID { get; set; }

        #endregion Autogen by RDV

        private WebMenuEntity _oMenu;

        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0) _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
    }

    public class ModCleanURLService : ServiceBase<ModCleanURLEntity>
    {
        #region Autogen by RDV

        private ModCleanURLService() : base("[Mod_CleanURL]")
        {
        }

        private static ModCleanURLService _instance;
        public static ModCleanURLService Instance => _instance ?? (_instance = new ModCleanURLService());

        #endregion Autogen by RDV

        public ModCleanURLEntity GetByUrl(string Url, int langId)
        {
            return CreateQuery().Where(o => o.Url == Url && o.LangID == langId).ToSingle();
        }

        public bool CheckUrl(string Url, string type, int value, int langId)
        {
            if (string.IsNullOrEmpty(Url)) return false;

            Url = Url.Trim();

            return CreateQuery()
                        .Where(o => o.Url == Url && o.LangID != langId)
                        .Where(type != string.Empty, o => o.Type != type)
                        .Where(value > 0, o => o.Value != value)
                        .Count()
                        .ToValue().ToBool();
        }

        public ModCleanURLEntity CURLEntity(string Url, string type, int value, int langId)
        {

            Url = Url.Trim();

            return CreateQuery()
                        .Where(o => o.Url == Url && o.LangID == langId)
                        .Where(type != string.Empty, o => o.Type == type)
                        .Where(value > 0, o => o.Value == value)
                        .ToSingle();
        }

        public void InsertOrUpdate(string Url, string type, int value, int menuId, int langId)
        {
            if (Url == null) return;

            Url = Url.Trim();

            if (string.IsNullOrEmpty(Url) || Url == "-") return;

            var clearUrl = CreateQuery().Where(o => o.Type == type && o.Value == value && o.LangID == langId).ToSingle();
            if (clearUrl != null)
            {
                clearUrl.Url = Url;
                clearUrl.MenuID = menuId;
                clearUrl.LangID = langId;

                Save(clearUrl, o => new { o.Url, o.MenuID });
            }
            else
            {
                clearUrl = new ModCleanURLEntity()
                {
                    Url = Url,
                    Type = type,
                    Value = value,
                    MenuID = menuId,
                    LangID = langId
                };

                Save(clearUrl);
            }
        }
    }
}