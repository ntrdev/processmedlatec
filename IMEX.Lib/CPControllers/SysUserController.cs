﻿using System;
using IMEX.Lib.Global;
using IMEX.Lib.Models;
using IMEX.Lib.MVC;

namespace IMEX.Lib.CPControllers
{
    [CPModuleInfo(Name = "Tài khoản quản trị",
         Description = "Quản lý - Tài khoản quản trị",
         Code = "SysUser",
         Access = 31,
         Order = 1,
         ShowInMenu = true,
         CssClass = "icon-16-article", Partitioning = 3)]
    public class SysUserController : CPController
    {

        public SysUserController()
        {
            //khoi tao Service
            DataService = CPUserService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(SysUserModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = CPUserService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => o.LoginName.Contains(model.SearchText) || o.Name.Contains(model.SearchText))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(SysUserModel model)
        {

            _item = model.RecordID > 0 ? CPUserService.Instance.GetByID(model.RecordID) : new CPUserEntity();

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(SysUserModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(SysUserModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(SysUserModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }

            for (int i = 0; arrID.Length > 0 && i < arrID.Length; i++)
            {
                if (arrID[i] == CPLogin.CurrentUser.ID)
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                    CPViewPage.SetMessage("Bạn không thể xóa tài khoản của mình.");
                }
                else
                {
                    var _id = arrID[i];
                    var _checkIsRoleAdmin = CPRoleService.Instance.CreateQuery()
                                                        .SelectJoin(false, o => new { o.ID, o.Lock })
                                                        .Join.LeftJoin(true, CPUserRoleService.Instance.CreateQuery().Where(x => x.UserID == _id), o => o.ID, x => x.RoleID)
                                                        .WhereJoin(o => o.Lock == true || o.Name == "Administrator")
                                                        .ToSingle_Cache();

                    if (_checkIsRoleAdmin != null && !_checkIsRoleAdmin.Lock)
                        DataService.Delete("[ID]=" + arrID[i] + "");
                }
            }


            CPUserLogService.Instance.Save(new CPUserLogEntity
            {
                Module = "User",
                Note = "Đã xóa",
                UserID = CPLogin.UserID,
                Created = DateTime.Now,
                IP = Core.Web.HttpRequest.IP,
                TypeAction = "Đã xóa"
            });
            //thong bao
            //  CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }


        #region private func

        private CPUserEntity _item;

        private bool ValidSave(SysUserModel model)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Add)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return false;
            }

            TryUpdateModel(_item);

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra ten dang nhap
            if (_item.LoginName.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tài khoản người dùng.");
            else if (CPUserService.Instance.CheckLoginNanme(_item.LoginName) && _item.ID < 1)
                CPViewPage.Message.ListMessage.Add("Tài khoản đã có người sử dụng, vui lòng dùng tài khoản khác.");
            else if (!string.IsNullOrEmpty(model.NewPassword) && !Utils.IsLoginName(model.NewPassword))
                CPViewPage.Message.ListMessage.Add("Tên tài khoản sai định dạng (tên đăng nhập không bao gồm các ký tự đặc biệt (* @ # $ % & .) và không có dấu cách.");

            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tên người sử dụng.");

            if (!string.IsNullOrEmpty(_item.Email))
            {
                if (!Utils.IsEmailAddress(_item.Email)) CPViewPage.Message.ListMessage.Add("Nhập đúng định dạng Email.");
            }
            if (model.NewPassword != string.Empty)
            {
                if (!Utils.IsLoginName(model.NewPassword)) CPViewPage.Message.ListMessage.Add("Mật khẩu phải từ 6 đến 12 ký tự và không có ký tự đặc biệt.");
                else _item.Password = Security.Md5(model.NewPassword);
            }

            if (CPViewPage.Message.ListMessage.Count != 0) return false;




            try
            {
                //save
                CPUserService.Instance.Save(_item);

                //xoa
                CPUserRoleService.Instance.Delete(o => o.UserID == _item.ID);

                //them
                for (var i = 0; model.ArrRole != null && i < model.ArrRole.Length; i++)
                {
                    CPUserRoleService.Instance.Save(new CPUserRoleEntity
                    {
                        UserID = _item.ID,
                        RoleID = model.ArrRole[i]
                    });
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        #endregion private func
    }

    public class SysUserModel : DefaultModel
    {
        public int[] ArrRole { get; set; }
        public string NewPassword { get; set; }
        public string SearchText { get; set; }
    }
}