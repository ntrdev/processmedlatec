﻿using IMEX.Lib.MVC;

namespace IMEX.Lib.CPControllers
{
    [CPModuleInfo(Name = "File tải lên",
        Description = "Quản lý - File tải lên",
        Code = "ModFile",
        Access = 15,
        Order = 9999,
        ShowInMenu = false,
        CssClass = "files-o", Partitioning = 1)]
    public class ModFileController : CPController
    {
        public void ActionIndex()
        {
        }
    }
}