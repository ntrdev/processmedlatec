using System.Globalization;
using System.Web;

namespace IMEX.Lib.Global
{
    public static class CPResource
    {
        private static string CurrentCode => CultureInfo.CurrentCulture.Name;

        public static string GetValue(string code)
        {
            return GetValue(code, string.Empty);
        }

        public static string GetValue(string code, string defalt)
        {
            var resourceService = new IniResourceService(HttpContext.Current.Server.MapPath("~/" + Core.Web.Setting.Sys_AdminDir + "/Views/Lang/" + CurrentCode + ".ini"));
            return resourceService.CoreMr_Reddevil_GetByCode(code, defalt);
        }
    }
}