﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="IMEX.Lib.MVC.CPViewControl" %>

<script runat="server">

    List<CPAccessEntity> listUserAccess = null;
    SysRoleModel model;
    CPRoleEntity item;
    protected void Page_Load(object sender, EventArgs e)
    {
        model = ViewBag.Model as SysRoleModel;
        item = ViewBag.Data as CPRoleEntity;

        if (model.RecordID > 0)
        {
            listUserAccess = CPAccessService.Instance.CreateQuery()
                                 .Where(o => o.Type == "CP.MODULE" && o.RoleID == model.RecordID)
                                 .ToList();
        }
    }

    int GetAccess(string refCode)
    {
        if (listUserAccess == null)
            return 0;

        var obj = listUserAccess.Find(o => o.RefCode == refCode);

        return obj == null ? 0 : obj.Value;
    }
</script>

<form id="IMEXsoftForm" name="IMEXsoftForm" method="post">

    <input type="hidden" id="_imt_action" name="_imt_action" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật nhóm phân quyền" : "Thêm mới"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phân quyền</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">

                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên nhóm:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Name" value="<%=item.Name %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Quyền:</label>
                                                    <div class="col-md-9">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-hover table-bordered dataTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="sorting_disabled w1p">#</th>
                                                                        <th class="sorting_disabled w10p text-center">
                                                                            <p>Duyệt</p>
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" id="ArrApprove" name="toggle" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrApprove', this.checked)" class="md-check">
                                                                                <label for="ArrApprove">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="sorting_disabled w10p text-center">
                                                                            <p>Xóa</p>
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" id="ArrDelete" name="toggle" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrDelete', this.checked)" class="md-check">
                                                                                <label for="ArrDelete">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="sorting_disabled w10p text-center">
                                                                            <p>Sửa</p>
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" id="ArrEdit" name="toggle" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrEdit', this.checked)" class="md-check">
                                                                                <label for="ArrEdit">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="sorting_disabled w10p text-center">
                                                                            <p>Thêm</p>
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" id="ArrAdd" name="toggle" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrAdd', this.checked)" class="md-check">
                                                                                <label for="ArrAdd">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="sorting_disabled w10p text-center">
                                                                            <p>Truy cập</p>
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" id="ArrView" name="toggle" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrView', this.checked)" class="md-check">
                                                                                <label for="ArrView">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="sorting_disabled">Tên Chức năng  </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" name="ArrApprove" id="ArrApprove_1" value="SysAdministrator" disabled="disabled" class="md-check">
                                                                                <label for="ArrApprove_1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" name="ArrDelete" id="ArrDelete_1" value="SysAdministrator" disabled="disabled" class="md-check">
                                                                                <label for="ArrDelete_1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" name="ArrEdit" id="ArrEdit_1" value="SysAdministrator" disabled="disabled" class="md-check">
                                                                                <label for="ArrEdit_1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">

                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" name="ArrAdd" id="ArrAdd_1" disabled="disabled" class="md-check">
                                                                                <label for="ArrAdd_1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" name="ArrView" id="ArrView_1" value="SysAdministrator" <%if ((GetAccess("SysAdministrator") & 1) == 1)
                                                                                    { %>checked="checked"
                                                                                    <%} %> class="md-check">
                                                                                <label for="ArrView_1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>Quản trị hệ thống</td>
                                                                    </tr>

                                                                    <%for (var i = 0; i < IMEX.Lib.Web.Application.CPModules.OrderBy(o => o.Order).ToList().Count; i++)
                                                                        { %>
                                                                    <tr>
                                                                        <td><%=i + 1 %></td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" class="md-check" id="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Approve"%>" name="ArrApprove" value="<%= IMEX.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(IMEX.Lib.Web.Application.CPModules[i].Code) & 16) == 16)
                                                                                    { %>checked="checked"
                                                                                    <%} %> <%if ((IMEX.Lib.Web.Application.CPModules[i].Access & 16) != 16)
                                                                                    { %>disabled="disabled"
                                                                                    <%} %> />
                                                                                <label for="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Approve"%>">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" class="md-check" id="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Delete"%>" name="ArrDelete" value="<%= IMEX.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(IMEX.Lib.Web.Application.CPModules[i].Code) & 8) == 8)
                                                                                    { %>checked="checked"
                                                                                    <%} %> <%if ((IMEX.Lib.Web.Application.CPModules[i].Access & 8) != 8)
                                                                                    { %>disabled="disabled"
                                                                                    <%} %> />
                                                                                <label for="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Delete"%>">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" class="md-check" id="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Edit"%>" name="ArrEdit" value="<%= IMEX.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(IMEX.Lib.Web.Application.CPModules[i].Code) & 4) == 4)
                                                                                    { %>checked="checked"
                                                                                    <%} %> <%if ((IMEX.Lib.Web.Application.CPModules[i].Access & 4) != 4)
                                                                                    { %>disabled="disabled"
                                                                                    <%} %> />
                                                                                <label for="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Edit"%>">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" class="md-check" id="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Add"%>" name="ArrAdd" value="<%= IMEX.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(IMEX.Lib.Web.Application.CPModules[i].Code) & 2) == 2)
                                                                                    { %>checked="checked"
                                                                                    <%} %> <%if ((IMEX.Lib.Web.Application.CPModules[i].Access & 2) != 2)
                                                                                    { %>disabled="disabled"
                                                                                    <%} %> />
                                                                                <label for="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_Add"%>">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="md-checkbox">
                                                                                <input type="checkbox" class="md-check" id="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_View"%>" name="ArrView" value="<%= IMEX.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(IMEX.Lib.Web.Application.CPModules[i].Code) & 1) == 1)
                                                                                    { %>checked="checked"
                                                                                    <%} %> <%if ((IMEX.Lib.Web.Application.CPModules[i].Access & 1) != 1)
                                                                                    { %>disabled="disabled"
                                                                                    <%} %> />
                                                                                <label for="<%= IMEX.Lib.Web.Application.CPModules[i].Code +"_View"%>">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td><%= IMEX.Lib.Web.Application.CPModules[i].Description%></td>
                                                                    </tr>
                                                                    <%} %>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
