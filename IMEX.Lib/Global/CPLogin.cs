﻿using IMEX.Core.Global;
using IMEX.Lib.Models;

namespace IMEX.Lib.Global
{
    public static class CPLogin
    {
        public static void Logout()
        {
            // Session.Remove("IMEXsoft.Dashboard");
            Cookies.Remove("IMEXsoft.Dashboard");
        }

        public static bool CheckLogin(string loginName, string password)
        {
            var user = CPUserService.Instance.GetLogin(loginName, password);

            if (user == null) return false;
            SetLogin(user.ID);

            return true;
        }

        private static void SetLogin(int userId)
        {
            // Session.SetValue("IMEXsoft.Dashboard", userId);
            Cookies.SetValue("IMEXsoft.Dashboard", userId.ToString(), Core.Web.Setting.Mod_AdminTimeout, true);
        }

        public static bool IsLogin()
        {
            return (UserID > 0);
            //return (CurrentUser != null);
        }

        public static int UserID
        {
            get
            {
                var userId = Convert.ToInt(Cookies.GetValue("IMEXsoft.Dashboard", true));

                if (userId > 0)
                    SetLogin(userId);

                return userId;
            }
        }

        public static CPUserEntity CurrentUser => UserID > 0 ? CPUserService.Instance.GetLogin(UserID) : null;
    }
}