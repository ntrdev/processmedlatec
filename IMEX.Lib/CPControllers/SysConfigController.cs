﻿using System;
using IMT.Lib.Global;
using IMT.Lib.Models;
using IMT.Lib.MVC;

namespace IMT.Lib.CPControllers
{

    public class SysConfigController : CPController
    {

        public SysConfigController()
        {
            //khoi tao Service
            DataService = WebConfigService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(SysConfigModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort, "[ID]");

            //tao danh sach
            var dbQuery = WebConfigService.Instance.CreateQuery()
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionUpload(SysConfigModel model)
        {
            CPViewPage.Script("Redirect", "REDDEVILRedirect('Import')");
        }

        public void ActionImport(SysConfigModel model)
        {
            ViewBag.Model = model;
        }

        public void ActionAdd(SysConfigModel model)
        {
            _item = model.RecordID > 0 ? WebConfigService.Instance.GetByID(model.RecordID) : new WebConfigEntity();

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(SysConfigModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(SysConfigModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(SysConfigModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionCancel()
        {
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx")
                .Replace("Import.aspx", "Index.aspx"));
        }

        #region private func

        private WebConfigEntity _item;

        private bool ValidSave(SysConfigModel model)
        {

            TryUpdateModel(_item);

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            try
            {
                //save
                WebConfigService.Instance.Save(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;

        }

        #endregion private func
    }

    public class SysConfigModel : DefaultModel
    {
        private int _langID = 1;

        public int LangID
        {
            get => _langID;
            set => _langID = value;
        }

        public string SearchText { get; set; }
        public string Config { get; set; }
    }
}