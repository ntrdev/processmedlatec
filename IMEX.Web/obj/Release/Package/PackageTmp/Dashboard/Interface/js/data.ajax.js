﻿function ShowFile(type) {
    var finder = new CKFinder();
    finder.basePath = '../';
    finder.selectActionFunction = refreshFile;
    finder.popup();

    return false;
}
function ShowFileLanding() {
    var finder = new CKFinder();
    finder.basePath = '../';
    finder.selectActionFunction = refreshFileLanding;
    finder.popup();

    return false;
}
function refreshFileLanding(arg) {
    var langID = $("#LandID").val();
    addFileLanding(langID, arg);
}

function ShowProductForm(sValue) {
    window.open('/' + window.CPPath + '/FormProduct/Index.aspx?Value=' + sValue, '', 'width=1024, height=800, top=80, left=200,scrollbars=yes');
    return false;
}

function refreshFile(arg) {
    addFile(arg);
}

function refreshGift(arg) {
    addProduct(arg)
}

function CloseGift(arg) {
    if (window.opener)
        window.opener.refreshGift(arg);
    else
        window.parent.refreshGift(arg);

    window.close();
}


//file
function addFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/AddFile.aspx',
        data: {
            Name: $('#Name').val(),
            MenuID: $('#CategoryID').val(),
            ProductID: $('#RecordID').val(),
            File: file,
        },
        dataType: 'json',
        success: function (data) {
            var js = data.Js;
            var params = data.Params;
            var content = data.Html;

            if (params != '') {
                alert_wee('Thông báo !', params);
                return;
            }

            if (js != '') {
                location.href = '/' + window.CPPath + '/' + ($('#dynamic_id').val() == 1 ? "ModProduct" : "ModNews") + '/Add.aspx/RecordID/' + js;
                return;
            }

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function deleteFile(file, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/DeleteFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function upFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/UpFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function downFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/DownFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

$('.publish-comment').on('click', function () {
    var type = $(this).data('type');
    publish($(this).data('id'), type);
    type = (type == 'True' ? 'False' : 'True');

    $(this).data('type', type);
});


function publish(id, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/PublishComment.aspx',
        data: {
            ObjID: id,
            type: type
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Message;

            if (content != '')
                alert_wee('Thông báo', content);
        },
        error: function (status) { }
    });
}


$('.publish-like').on('click', function () {
    var type = $(this).data('type');
    like($(this).data('id'), type, $(this).data('newsid'));
    type = (type == 'True' ? 'False' : 'True');

    $(this).data('type', type);
});


function like(id, type, newsid) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Api/PublishLike.aspx',
        data: {
            ObjID: id,
            type: type,
            PageIndex: newsid
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Message;

            if (content != '')
                alert_wee('Thông báo', content);
        },
        error: function (status) { }
    });
}
